#!/usr/bin/bash

STARTING_DIR="$1"
TARGET_DIR="$2"
TCL_SCRIPT="$3"
RESULTS="$4"

removeUselessFiles(){
	find . -not -name simulation.do -not -name vcd_design.do \
	-not -name vcd_synthesis.do -not -name "$RESULTS" \
	-not -name synop_simulation.do -not -name innov_simulation.do -delete
}

# Move to the directory containing the script
# to run with Modelsim
cd $TARGET_DIR

removeUselessFiles

# Set up the environment to properly run Modelsim
source /software/scripts/init_msim6.2g

# Execute the desired set of Modelsim commands
vsim -c -do $TCL_SCRIPT

removeUselessFiles

# Return to the starting directory
cd $STARTING_DIR
