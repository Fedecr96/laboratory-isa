#!/usr/bin/python3

import os
import random

n_samples = 200

rand_file_name = "samples.txt"
file_path = "./" + rand_file_name

rand_file = open(rand_file_name, 'w+')

for i in range(n_samples):
	rand_file.write(str(random.randint(-256, 255)) + "\n")

rand_file.close()
