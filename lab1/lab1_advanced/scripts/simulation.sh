#!/usr/bin/bash

# Path of the directory in which simulation.sh is
START_DIR=`pwd`

# Random input generation script
RAND="random_input_gen.py"


# ---------------------------------- C model ----------------------------------------

# Makefile location: in order to be correctly executed, the make command needs
# to be executed in the directory in which the Makefile is located
MAKEDIR="../C"

EXECUTABLE_FILE="filtering"

C_RESULTS="../C/C_results.txt"

# Remove the result from previous simulations
if [[ -f $C_RESULTS ]]
then
	rm $C_RESULTS
fi

# Generate random inputs
./$RAND

# Move to the folder in which the makefile is contained
cd $MAKEDIR

# Create the executable starting from the various source files
make

# Add the execution permission to the generated executable only for the user
chmod 700 $EXECUTABLE_FILE

# Run the executable
./$EXECUTABLE_FILE

# Remove the object files
make clean

# Remove the executable
make cleanexe

# Return in the starting directory
cd $START_DIR




# -------------------------------- vhdl architecture --------------------------------

# simulation.do location
SIMULATION_DO="../sim"

VHDL_RESULTS="../sim/results_synopsys_sim.txt"

cd $SIMULATION_DO

# Initialize the environment to be able to use vsim
source /software/scripts/init_msim6.2g

# Remove previously existing files and directories:
# 1) Work directory
if [[ -d work ]]
then
	rm -r work
fi
# 2) simulation file
if [[ -f vsim.wlf ]]
then
	rm vsim.wlf
fi
# 3) Transcript
if [[ -f transcript ]]
then
	rm transcript
fi
# 4) Output results
if [[ -f $VHDL_RESULTS ]]
then
	rm $VHDL_RESULTS
fi

# Run compilation and simulation
vsim -c -do synop_simulation.do

# Remove the new generate files useless for the test of the component
rm -rf work
rm -f vsim.wlf
rm -f transcript

# Return in the starting directory
cd $START_DIR



# ------------------------------- compare results ----------------------------------

RED="\033[0;31m"
GREEN="\033[0;32m"
RM_ATTRIBUTES="\033[0m"

# Compare the two results files
diff $C_RESULTS $VHDL_RESULTS 1>/dev/null 2>error.txt

DIFF_OUT="$?"

# Check if there is any difference
if [[ $DIFF_OUT == 0 ]]
then
	echo -e "$GREEN\nCORRECT\n$RM_ATTRIBUTES"
elif [[ $DIFF_OUT == 1  ]]
then
	echo -e "$RED\nWRONG\n$RM_ATTRIBUTES"
else [[ $DIFF_OUT == 2 ]]
	echo -en "\n"
	cat error.txt
	echo -en "\n"
fi

rm error.txt
