#!/usr/bin/bash

STARTING_DIR="$1"
TARGET_DIR="$2"
TCL_SCRIPT="$3"
DESIGN_FILE="$4"

removeUselessFiles(){
	find . -not -name "$DESIGN_FILE.enc" -not -name "$DESIGN_FILE.enc.dat" \
	-not -name design.globals -not -name mmm_design.tcl \
	-not -name design_power.cmd -not -name place_and_route.cmd \
	-not -name report_power.txt -not -name report_power_tot.txt \
	-not -name timingReports -not -name $DESIGN_FILE.v \
	-not -name $DESIGN_FILE.sdf -delete
}

# Move to the directory containing the script
# to run with Modelsim
cd $TARGET_DIR

removeUselessFiles

# Set up the environment to properly run Modelsim
source /software/scripts/init_innovus17.11

# Execute the desired set of Modelsim commands
innovus -no_gui -batch -files $TCL_SCRIPT

removeUselessFiles

# Return to the starting directory
cd $STARTING_DIR
