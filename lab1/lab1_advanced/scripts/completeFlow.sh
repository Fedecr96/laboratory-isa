#!/usr/bin/bash

DUT="iir_o1_n9_optimized"

# Project directories
CURRENT_DIR="`pwd`"
SIM_DIR="../sim"
SYN_DIR="../syn"
INNOVUS_DIR="../innovus"
C_DIR="../C"
TB_DIR="../tb"
VCD_DIR="../vcd"
SAIF_DIR="../saif"
NETLIST_DIR="../netlist"

# Modelsim tcl scripts
SIMULATION="simulation.do"
SYN_SIMULATION="synop_simulation.do"
INN_SIMULATION="innov_simulation.do"
VCD_SYNTHESIS="vcd_synthesis.do"
VCD_DESIGN="vcd_design.do"


# Synopsys tcl scripts
SYN_POWER="synthesis_power.tcl"
SYNTH="synth.tcl"

# Innovus tcl scripts
DESIGN_POWER="design_power.cmd"
PLACE_AND_ROUTE="place_and_route.cmd"

# Bash scripts
MODELSIM_SCRIPT="modelsim.sh"
SYNOPSYS_SCRIPT="synopsys.sh"
INNOVUS_SCRIPT="innovus.sh"
C="C.sh"
RAND="random_input_gen.py"

# C executable file
C_EXE="filtering"

# Results files
C_RESULTS="C_results.txt"
VHDL_RESULTS="results.txt"
SYN_RESULTS="results.txt"
INN_RESULTS="results.txt"
VCD_SYNOPSYS="synthesis.vcd"
VCD_INNOVUS="design.vcd"
SAIF_SYNOPSYS="synthesis.saif"

# Message color	
RED="\033[0;31m"
GREEN="\033[0;32m"
RM_ATTRIBUTES="\033[0m"


checkIfCorrect(){

	C_RESULTS="$1"
	VHDL_RESULTS="$2"

	# Compare the two results files
	diff $C_RESULTS $VHDL_RESULTS 1>/dev/null 2>error.txt

	DIFF_OUT="$?"

	# Check if there is any difference
	if [[ $DIFF_OUT == 0 ]]
	then
		echo -e "$GREEN\nCORRECT\n$RM_ATTRIBUTES"
		rm error.txt
	elif [[ $DIFF_OUT == 1  ]]
	then
		echo -e "$RED\nWRONG\n$RM_ATTRIBUTES"
		rm error.txt
		exit -1
	else [[ $DIFF_OUT == 2 ]]
		echo -en "\n"
		cat error.txt
		echo -en "\n"
		rm error.txt
		exit -1
	fi
}



# --------------------------------- SIMULATION  ---------------------------------- 
echo -e "$GREEN\n--------------------------------- STARTING SIMULATION \
---------------------------------\n$RM_ATTRIBUTES"

# Create the file sample.txt containing random numbers
./$RAND

# Run the C simulation
./$C $CURRENT_DIR $C_DIR $C_EXE $C_RESULTS

# Run the vhdl developed simulation
./$MODELSIM_SCRIPT $CURRENT_DIR $SIM_DIR $SIMULATION $VHDL_RESULTS

# Check the results
checkIfCorrect $C_DIR/$C_RESULTS $SIM_DIR/$VHDL_RESULTS





# --------------------------------- SYNTHESIS ----------------------------------
echo -e "$GREEN\n--------------------------------- STARTING SYNTHESIS \
---------------------------------\n$RM_ATTRIBUTES"

# Synthetize the design
./$SYNOPSYS_SCRIPT $CURRENT_DIR $SYN_DIR $SYNTH

# Simulate the generated netlist
./$MODELSIM_SCRIPT $CURRENT_DIR $SIM_DIR $SYN_SIMULATION $SYN_RESULTS

# Check the results
checkIfCorrect $C_DIR/$C_RESULTS $SIM_DIR/$SYN_RESULTS




# --------------------------------- POWER CONSUMPTION ----------------------------------
echo -e "$GREEN\n--------------------------------- STARTING SYNTHESIS POWER ESTIMATION \
---------------------------------\n$RM_ATTRIBUTES"

rm -rf $SAIF_DIR
mkdir $SAIF_DIR

# Extract the vcd file
./$MODELSIM_SCRIPT $CURRENT_DIR $SIM_DIR $VCD_SYNTHESIS $SYN_RESULTS

# Convert the vcd to saif
source /software/scripts/init_synopsys_64.18
vcd2saif -input $VCD_DIR/$VCD_SYNOPSYS -output $SAIF_DIR/$SAIF_SYNOPSYS

# Compute the power consumption
./$SYNOPSYS_SCRIPT $CURRENT_DIR $SYN_DIR $SYN_POWER



# --------------------------------- PLACE AND ROUTE ----------------------------------
echo -e "$GREEN\n--------------------------------- STARTING PLACE AND ROUTE \
---------------------------------\n$RM_ATTRIBUTES"

# Place and route
./$INNOVUS_SCRIPT $CURRENT_DIR $INNOVUS_DIR $PLACE_AND_ROUTE $DUT

# Simulate the generated netlist
./$MODELSIM_SCRIPT $CURRENT_DIR $SIM_DIR $INN_SIMULATION $INN_RESULTS

# Check the results
checkIfCorrect $C_DIR/$C_RESULTS $SIM_DIR/$INN_RESULTS



# --------------------------------- POWER CONSUMPTION ----------------------------------
echo -e "$GREEN\n--------------------------------- STARTING INNOVUS POWER ESTIMATION \
---------------------------------\n$RM_ATTRIBUTES"

# Extract the vcd fil/$MODELSIM_SCRIPT $CURRENT_DIR $SIM_DIR $VCD_SYNTHESIS
./$MODELSIM_SCRIPT $CURRENT_DIR $SIM_DIR $VCD_DESIGN

# Compute the power consumption
./$INNOVUS_SCRIPT $CURRENT_DIR $INNOVUS_DIR $DESIGN_POWER


echo -e "$GREEN\n--------------------------------- END \
---------------------------------\n$RM_ATTRIBUTES"
