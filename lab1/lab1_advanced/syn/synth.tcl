# Internal variables
set TOP_ENTITY  iir_o1_n9_optimized
set ARCH BEHAVIOUR
set CLOCK_SIGNAL MY_CLK
set IIR_CLK_INPUT clk
set CLK_PERIOD 6.92
set CLK_UNCERT 0.07
set IN_DELAY 0.5
set OUT_DELAY 0.5

# Output files
set REPORT_TIMING report_timing.txt
set REPORT_AREA report_area.txt

# Import vhdl source files in the current design
# 	-f = format of the source files
analyze -f vhdl -lib WORK ../src/adder_bhv9.vhd
analyze -f vhdl -lib WORK ../src/multiplier_bhv9.vhd
analyze -f vhdl -lib WORK ../src/register_9bit.vhd
analyze -f vhdl -lib WORK ../src/register_9bit_rst.vhd
analyze -f vhdl -lib WORK ../src/delay_ff.vhd
analyze -f vhdl -lib WORK ../src/$TOP_ENTITY.vhd


# Preserve RTL names in the generated netlist
set power_preserve_rtl_hier_names true

# Synthetize the desired architecture (-arch) of the main entity
elaborate $TOP_ENTITY -arch $ARCH -lib WORK

# Make the various instances of a component to refer to the same
# component
uniquify

link

# Create the clock with the proper characteristics: for our design
# the minimum clock period is 2.69ns. Here we want to work with
# f = f_clk_min/4 and so with T = 4*T_clk_min
create_clock -name $CLOCK_SIGNAL -period $CLK_PERIOD $IIR_CLK_INPUT
set_dont_touch_network $CLOCK_SIGNAL
set_clock_uncertainty $CLK_UNCERT [get_clocks $CLOCK_SIGNAL]
set_input_delay $IN_DELAY -max -clock $CLOCK_SIGNAL \
[remove_from_collection [all_inputs] $IIR_CLK_INPUT]
set_output_delay $OUT_DELAY -max -clock $CLOCK_SIGNAL [all_outputs]

set OLOAD [load_of NangateOpenCellLibrary/BUF_X4/A]
set_load $OLOAD [all_outputs]

compile

# Report results
report_timing > $REPORT_TIMING
report_area > $REPORT_AREA


# Create the netlist and the delay file
ungroup -all -flatten
change_names -hierarchy -rules verilog
write_sdf ../netlist/$TOP_ENTITY.sdf
write -f verilog -hierarchy -output ../netlist/$TOP_ENTITY.v
write_sdc ../netlist/$TOP_ENTITY.sdc

quit







