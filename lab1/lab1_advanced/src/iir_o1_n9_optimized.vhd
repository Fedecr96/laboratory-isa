library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity iir_o1_n9_optimized is
	port(
		--input signals
		clk			: in std_logic;
		v_in		: in std_logic;
		rst_n		: in std_logic;
		x_n			: in signed(8 downto 0);
		coeff_a 	: in signed(8 downto 0);
		coeff_aa	: in signed(8 downto 0);
		coeff_b 	: in signed(8 downto 0);

		--output signals
		y_n		: out signed(8 downto 0);
		v_out		: out std_logic
	);
end entity iir_o1_n9_optimized;

architecture behaviour of iir_o1_n9_optimized is

	-- COMPONENTS --

	component register_9bit_rst is 
		port(
			--input signals
			clk		: in std_logic;
			reg_en	: in std_logic;
			rst		: in std_logic;
			d_in	: in signed(8 downto 0);

			--output signals
			d_out		: out signed(8 downto 0)
		);
	end component register_9bit_rst;
	
	component delay_ff is
		port(
			--input signals
			clk			: in std_logic;
			reg_en		: in std_logic;
			rst			: in std_logic;
			d_in		: in std_logic;

			--output signals
			d_out		: out std_logic
		);
	end component delay_ff;
	
	component adder_bhv9 is
		port(
			in1	: in signed (8 downto 0);
			in2	: in signed (8 downto 0);
		
			sum_out	: out signed(8 downto 0)
		);
	end component adder_bhv9;

	component multiplier_bhv9 is
		port(
			in1		: in signed (8 downto 0);
			in2		: in signed (8 downto 0);
		
			mpy_out	: out signed(8 downto 0)
		);
	end component multiplier_bhv9;
	
	-- SIGNALS --
	signal D1_out, D2_out, D3_out, D4_out, D5_out, D6_out, D7_out, M1_out, M2_out, M3_out, A1_out, A2_out, A3_out	: signed (8 downto 0);
	signal vin_delay1_out, D5_en, D4_en, vin_delay4_out : std_logic;
	signal rst_int	: std_logic;

	BEGIN

	-- rst_int <= not rst_n;	

	D1 : register_9bit_rst port map(
		clk 	=> clk,
		reg_en 	=> v_in,
		rst	=> rst_n,
		d_in	=> x_n,
		d_out	=> D1_out
	);

	D2 : register_9bit_rst port map(
		clk	=> clk,
		reg_en	=> vin_delay1_out,
		rst 	=> rst_n,
		d_in	=> M1_out,
		d_out	=> D2_out
	);

	D3 : register_9bit_rst port map(
		clk	=> clk,
		reg_en	=> vin_delay1_out,
		rst 	=> rst_n,
		d_in	=> A1_out,
		d_out	=> D3_out
	);
	
	D4 : register_9bit_rst port map(
		clk		=> clk,
		reg_en	=> '1',
		rst 	=> rst_n,
		d_in	=> M2_out,
		d_out	=> D4_out
	);
	
	D5 : register_9bit_rst port map(
		clk		=> clk,
		reg_en	=> '1',
		rst 	=> rst_n,
		d_in	=> A2_out,
		d_out	=> D5_out
	);
	
	D6 : register_9bit_rst port map(
		clk	=> clk,
		reg_en	=> D4_en,
		rst 	=> rst_n,
		d_in	=> D5_out,
		d_out	=> D6_out
	);
	
	D7 : register_9bit_rst port map(
		clk	=> clk,
		reg_en	=> D4_en,
		rst 	=> rst_n,
		d_in	=> A3_out,
		d_out	=> D7_out
	);
	
	D8 : register_9bit_rst port map(
		clk	=> clk,
		reg_en	=> vin_delay4_out,
		rst 	=> rst_n,
		d_in	=> M3_out,
		d_out	=> y_n
	);

	A1: adder_bhv9 port map(
		in1		=> D1_out,
		in2		=> D2_out,
		sum_out		=> A1_out
	);

	A2: adder_bhv9 port map(
		in1		=> D3_out,
		in2		=> D4_out,
		sum_out		=> A2_out
	);
	
	A3: adder_bhv9 port map(
		in1		=> D5_out,
		in2		=> D6_out,
		sum_out		=> A3_out
	);

	M1: multiplier_bhv9 port map(
		in1		=> D1_out,
		in2		=> coeff_a,
		mpy_out		=> M1_out
	);

	M2: multiplier_bhv9 port map(
		in1		=> D5_out,
		in2		=> coeff_aa,
		mpy_out		=> M2_out
	);
	
	M3: multiplier_bhv9 port map(
		in1		=> D7_out,
		in2		=> coeff_b,
		mpy_out		=> M3_out
	);
	
	VIN_DELAY1: delay_ff port map(
		clk		=> clk,
		reg_en		=> '1',
		rst		=> rst_n,
		d_in		=> v_in,
		d_out		=> vin_delay1_out
	);
	
	VIN_DELAY2: delay_ff port map(
		clk		=> clk,
		reg_en		=> '1',
		rst		=> rst_n,
		d_in		=> vin_delay1_out,
		d_out		=> D5_en
	);
	
	VIN_DELAY3: delay_ff port map(
		clk		=> clk,
		reg_en		=> '1',
		rst		=> rst_n,
		d_in		=> D5_en,
		d_out		=> D4_en
	);
	
	VIN_DELAY4: delay_ff port map(
		clk		=> clk,
		reg_en		=> '1',
		rst		=> rst_n,
		d_in		=> D4_en,
 		d_out		=> vin_delay4_out
	);
	
	VIN_DELAY5: delay_ff port map(
		clk		=> clk,
		reg_en		=> '1',
		rst		=> rst_n,
		d_in		=> vin_delay4_out,
		d_out		=> v_out
	);

end architecture behaviour;
