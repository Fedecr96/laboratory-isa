library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder_bhv9 is
	port(
		in1		: in signed (8 downto 0);
		in2		: in signed (8 downto 0);
		
		sum_out	: out signed(8 downto 0)
	);
end entity adder_bhv9;


architecture bhv of adder_bhv9 is

signal in_1, in_2 : signed (9 downto 0);
signal out_10bit : signed(9 downto 0);

begin

	in_1 <= in1(8) & in1;
	in_2 <= in2(8) & in2;

		out_10bit <= in_1 + in_2;
	
	sum_out <= out_10bit(9 downto 1);

end architecture;
