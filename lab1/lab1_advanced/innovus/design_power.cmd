#######################################################
#                                                     
#  Innovus Command Logging File                     
#  Created on Sun Nov  1 18:34:20 2020                
#                                                     
#######################################################

#@(#)CDS: Innovus v17.11-s080_1 (64bit) 08/04/2017 11:13 (Linux 2.6.18-194.el5)
#@(#)CDS: NanoRoute 17.11-s080_1 NR170721-2155/17_11-UB (database version 2.30, 390.7.1) {superthreading v1.44}
#@(#)CDS: AAE 17.11-s034 (64bit) 08/04/2017 (Linux 2.6.18-194.el5)
#@(#)CDS: CTE 17.11-s053_1 () Aug  1 2017 23:31:41 ( )
#@(#)CDS: SYNTECH 17.11-s012_1 () Jul 21 2017 02:29:12 ( )
#@(#)CDS: CPE v17.11-s095
#@(#)CDS: IQRC/TQRC 16.1.1-s215 (64bit) Thu Jul  6 20:18:10 PDT 2017 (Linux 2.6.18-194.el5)


set REPORT_POWER "report_power.txt"
set REPORT_POWER_TOT "report_power_tot.txt"

set TOP_ENTITY "iir_o1_n9_optimized"
set TB "tb_iir_o1_n9_optimized"
set RESTORE_DESIGN "${TOP_ENTITY}.enc.dat"

set VCD_DIR "../vcd"
set VCD_FILE "design.vcd"

set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default
suppressMessage ENCEXT-2799
#getDrawView
#loadWorkspace -name Physical
#win
encMessage warning 0
encMessage debug 0
encMessage info 0


# Restore the design
# -------------------------------------------------------------------------------------------------
restoreDesign ${RESTORE_DESIGN} ${TOP_ENTITY}
#setDrawView fplan
encMessage warning 1
encMessage debug 0
encMessage info 1


reset_parasitics
extractRC
rcOut -setload ${TOP_ENTITY}.setload -rc_corner my_rc
rcOut -setres ${TOP_ENTITY}.setres -rc_corner my_rc
rcOut -spf ${TOP_ENTITY}.spf -rc_corner my_rc
rcOut -spef ${TOP_ENTITY}.spef -rc_corner my_rc
set_power_analysis_mode -reset
set_power_analysis_mode -method static -corner max -create_binary_db true -write_static_currents true -honor_negative_energy true -ignore_control_signals true
set_power_output_dir -reset
set_power_output_dir ./
set_default_switching_activity -reset
set_default_switching_activity -input_activity 0.2 -period 10.0
read_activity_file -reset
read_activity_file -format VCD -scope ${TB}/UUT -start {} -end {} -block {} ${VCD_DIR}/${VCD_FILE}
set_power -reset
set_powerup_analysis -reset
set_dynamic_power_simulation -reset
report_power -rail_analysis_format VS -outfile ./${TOP_ENTITY}.rpt
report_power -outfile ${REPORT_POWER} -sort { total }
report_power -outfile ${REPORT_POWER_TOT} -clock_network all -hierarchy all -cell_type all -power_domain all -pg_net all -net -sort { total }
