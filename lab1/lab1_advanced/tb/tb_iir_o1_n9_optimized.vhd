library ieee;
library std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity tb_iir_o1_n9_optimized is
end entity tb_iir_o1_n9_optimized;


architecture simulation of tb_iir_o1_n9_optimized is
   

	component iir_o1_n9_optimized is
		port(
		   --input signals
		   clk		: in std_logic;
		   v_in		: in std_logic;
		   rst_n	: in std_logic;
		   x_n		: in signed(8 downto 0);
		   coeff_a	: in signed(8 downto 0);
		   coeff_aa	: in signed(8 downto 0);
		   coeff_b	: in signed(8 downto 0);
   
		   --output signals
		   y_n		: out signed(8 downto 0);
		   v_out	: out std_logic
		   );
	end component iir_o1_n9_optimized; 
    

	signal test_clk,test_rst_gen,test_v_out            	 	: std_logic;
	signal test_v_in                                       : std_logic := '0';
	signal test_x_n,test_coeff_a,test_coeff_b,test_y_n, test_coeff_aa	: signed(8 downto 0);
	signal in_start                                                		: std_logic;

	file infile  : text open read_mode is "../scripts/samples.txt";
	file outfile : text open write_mode is "results.txt";    


begin
    
	-- filter coefficients
	test_coeff_a 	<= to_signed(-41, 9);
	test_coeff_aa 	<= to_signed(210, 9);
	test_coeff_b 	<= to_signed(107, 9);


	DUT: iir_o1_n9_optimized port map(
		clk       => test_clk,
		v_in      => test_v_in,
		rst_n	  => test_rst_gen,
		x_n       => test_x_n,
		coeff_a   => test_coeff_a,
		coeff_aa  => test_coeff_aa,
		coeff_b   => test_coeff_b,
		y_n       => test_y_n,
		v_out     => test_v_out
	);

	

	clk_gen : process
	begin
		test_clk <='0';
		wait for 10 ns;
		test_clk <= '1';
		wait for 10 ns;
	end process clk_gen;


	rst_gen : process
	begin
		test_rst_gen <= '0';
		wait for 5 ns;
		test_rst_gen <= '1';
		wait;
	end process rst_gen;

	

	-- control signal to decide when to give Vin with valid data
	input_start : process
	begin
		in_start <= '0';
		wait for 50 ns;
		in_start <= '1';
		--wait for 490 ns;
		--in_start <= '0';
		--wait for 20 ns;
		--in_start <= '0';
		wait for 50 ns;
		in_start <= '1';
		wait for 260 ns;
		in_start <= '0';
		wait for 200 ns;
		in_start <= '1';
     		 wait for 20 ns;
      		in_start <= '0';
      		wait for 20 ns;
      		in_start <= '1';
		--in_start <= '1';
		wait;
	end process input_start;



	-- generation of Vin and valid data from file
	data_vin_gen : process (test_clk) --, in_start)

		variable inLine : line;
		variable x : integer;

	begin
		if test_clk'event and test_clk='1' 
		then
			if in_start = '1' 
			then
				   if ( not endfile(infile) )
				   then
					   readline(infile,inline);
					   read(inline,x);
                  test_v_in <= '1';
					   test_x_n <= to_signed(x,test_x_n'length);
				   else
					   test_v_in <= '0';
				   end if;
			else
				test_v_in <= '0';
			end if;
		end if;
	end process data_vin_gen;



	-- Vout check and output data writing on file
	vout_data_sample : process(test_clk, test_v_out)

		variable y		: integer;
		variable outLine	: line;

	begin

		if test_clk'event and test_clk = '1'
		then
			if test_v_out = '1'
			then
				y := to_integer(test_y_n);
				write(outLine, y);
				writeline(outFile, outLine);
			end if;
		end if;
	end process vout_data_sample;


end architecture simulation;
