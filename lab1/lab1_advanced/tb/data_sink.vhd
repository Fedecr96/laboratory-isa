library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;

entity data_sink is
	port (
		-- input signals
		CLK   : in std_logic;
		RST_n : in std_logic;
		VIN   : in std_logic;
		DIN   : in signed(8 downto 0)
	);
end data_sink;

architecture beh of data_sink is

begin  -- beh

	output_results : process (CLK, RST_n)
		
		file res_fp		: text open WRITE_MODE is "./results.txt";
		variable line_out	: line;    
	
	begin  -- process

		if RST_n = '0' -- asynchronous reset (active low)
		then
			null;

		elsif CLK'event and CLK = '1' -- rising clock edge
		then
		
			if (VIN = '1')
			then
				write(line_out, to_integer(DIN));
				writeline(res_fp, line_out);
			end if;
		end if;
	end process output_results;

end beh;

