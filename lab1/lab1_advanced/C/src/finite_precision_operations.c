#include "finite_precision_operations.h"

// Addition between two integer numbers on N bits.
// Returns the result on N bits
int finiteSum(int x, int y){

	return (x+y) >>1;
}


// Multiplication between two integer numbers on Nbits.
// Returns the result on N bits.
// One of the two operands is supposed to be lower
// than one in magnitude. So to obtain a value that is
// representable on N bits it is sufficient to shift it
// by N-1 bits townards right.
int finiteMult(int x, int y, int n){

	return (x*y) >>(n-1);
}
