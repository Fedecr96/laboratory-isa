#include "my_IIR_filter.h"
#include "stdio.h"
// IIR filter on 9 bits. It works on a single sample
// and returns the value of a single output sample.
// 
// The filter implements the optimized solution, obtained
// using the J-look-ahead technique. In particular the
// new equations considered are:
//
// 	w[n] = x[n] + a1*x[n-1] + a1^2*w[n-2]
// 	y[n] = b0*(w[n] + w[n-1])

int myIIRfilter(int inSample){

	static int firstExe = 0;
	static int x_n1,w_n1,w_n2;
	int w_n;
	int a1_square=210;
	int outSample;
	
	// If the filter is in its first execution
	// the internal register must be initialized
	if(firstExe == 0){
		firstExe = 1;
		x_n1 = 0;
		w_n1 = 0;
		w_n2 = 0;
	}

	// First partial sum w[n] = x[n] + a1*x[n-1]
	w_n = finiteSum(inSample,finiteMult(a1,x_n1,N_BITS));

	// Second partial sum w[n] = w[n] + a1^2*w[n-w]
	w_n = finiteSum(w_n,finiteMult(a1_square,w_n2,N_BITS));

	// y[n] = b0*(w[n] + w[n-1])
	outSample = finiteMult(b0,finiteSum(w_n,w_n1),N_BITS);

	// Update the delayed samples x[n-1], w[n-1] and w[n-2]
	x_n1 = inSample;
	w_n2 = w_n1;
	w_n1=w_n;

	return outSample;
}


