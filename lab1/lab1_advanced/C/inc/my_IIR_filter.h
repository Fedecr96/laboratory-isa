#ifndef __my_IIR_filter__
#define __my_IIR_filter__

#include "finite_precision_operations.h"

#define N 1
#define N_BITS 9

extern const int a1;
extern const int b0;

// IIR filter on 9 bits. It works on a single sample
// and returns the value of a single output sample.
int myIIRfilter(int inSample);

#endif
