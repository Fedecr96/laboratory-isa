/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP4
// Date      : Wed Nov 11 17:18:51 2020
/////////////////////////////////////////////////////////////


module iir_o1_n9_optimized ( clk, v_in, rst_n, x_n, coeff_a, coeff_aa, coeff_b, 
        y_n, v_out );
  input [8:0] x_n;
  input [8:0] coeff_a;
  input [8:0] coeff_aa;
  input [8:0] coeff_b;
  output [8:0] y_n;
  input clk, v_in, rst_n;
  output v_out;
  wire   vin_delay1_out, D4_en, vin_delay4_out, D5_en, D1_n27, D1_n26, D1_n25,
         D1_n24, D1_n23, D1_n22, D1_n21, D1_n20, D1_n19, D1_n18, D1_n17,
         D1_n16, D1_n15, D1_n14, D1_n13, D1_n12, D1_n11, D1_n10, D1_n9, D1_n8,
         D1_n7, D1_n6, D1_n5, D1_n4, D1_n3, D1_n2, D1_n1, D2_n56, D2_n55,
         D2_n54, D2_n53, D2_n52, D2_n51, D2_n50, D2_n49, D2_n48, D2_n47,
         D2_n46, D2_n45, D2_n44, D2_n43, D2_n42, D2_n41, D2_n40, D2_n39,
         D2_n38, D2_n37, D2_n36, D2_n35, D2_n34, D2_n33, D2_n32, D2_n31,
         D2_n30, D2_n29, D2_n28, D3_n56, D3_n55, D3_n54, D3_n53, D3_n52,
         D3_n51, D3_n50, D3_n49, D3_n48, D3_n47, D3_n46, D3_n45, D3_n44,
         D3_n43, D3_n42, D3_n41, D3_n40, D3_n39, D3_n38, D3_n37, D3_n36,
         D3_n35, D3_n34, D3_n33, D3_n32, D3_n31, D3_n30, D3_n29, D3_n28,
         D4_n54, D4_n53, D4_n52, D4_n51, D4_n50, D4_n49, D4_n48, D4_n47,
         D4_n46, D4_n45, D4_n44, D4_n43, D4_n42, D4_n41, D4_n40, D4_n39,
         D4_n38, D4_n37, D4_n36, D4_n35, D4_n34, D4_n33, D4_n32, D4_n31,
         D4_n30, D4_n29, D4_n28, D5_n54, D5_n53, D5_n52, D5_n51, D5_n50,
         D5_n49, D5_n48, D5_n47, D5_n46, D5_n45, D5_n44, D5_n43, D5_n42,
         D5_n41, D5_n40, D5_n39, D5_n38, D5_n37, D5_n36, D5_n35, D5_n34,
         D5_n33, D5_n32, D5_n31, D5_n30, D5_n29, D5_n28, D6_n56, D6_n55,
         D6_n54, D6_n53, D6_n52, D6_n51, D6_n50, D6_n49, D6_n48, D6_n47,
         D6_n46, D6_n45, D6_n44, D6_n43, D6_n42, D6_n41, D6_n40, D6_n39,
         D6_n38, D6_n37, D6_n36, D6_n35, D6_n34, D6_n33, D6_n32, D6_n31,
         D6_n30, D6_n29, D6_n28, D7_n56, D7_n55, D7_n54, D7_n53, D7_n52,
         D7_n51, D7_n50, D7_n49, D7_n48, D7_n47, D7_n46, D7_n45, D7_n44,
         D7_n43, D7_n42, D7_n41, D7_n40, D7_n39, D7_n38, D7_n37, D7_n36,
         D7_n35, D7_n34, D7_n33, D7_n32, D7_n31, D7_n30, D7_n29, D7_n28,
         D8_n54, D8_n53, D8_n52, D8_n51, D8_n50, D8_n49, D8_n48, D8_n47,
         D8_n46, D8_n45, D8_n44, D8_n43, D8_n42, D8_n41, D8_n40, D8_n39,
         D8_n38, D8_n37, D8_n36, D8_n35, D8_n34, D8_n33, D8_n32, D8_n31,
         D8_n30, D8_n29, D8_n28, M1_mult_28_n362, M1_mult_28_n361,
         M1_mult_28_n360, M1_mult_28_n359, M1_mult_28_n358, M1_mult_28_n357,
         M1_mult_28_n356, M1_mult_28_n355, M1_mult_28_n354, M1_mult_28_n353,
         M1_mult_28_n352, M1_mult_28_n351, M1_mult_28_n350, M1_mult_28_n349,
         M1_mult_28_n348, M1_mult_28_n347, M1_mult_28_n346, M1_mult_28_n345,
         M1_mult_28_n344, M1_mult_28_n343, M1_mult_28_n342, M1_mult_28_n341,
         M1_mult_28_n340, M1_mult_28_n339, M1_mult_28_n338, M1_mult_28_n337,
         M1_mult_28_n336, M1_mult_28_n335, M1_mult_28_n334, M1_mult_28_n333,
         M1_mult_28_n332, M1_mult_28_n331, M1_mult_28_n330, M1_mult_28_n329,
         M1_mult_28_n328, M1_mult_28_n327, M1_mult_28_n326, M1_mult_28_n325,
         M1_mult_28_n324, M1_mult_28_n323, M1_mult_28_n322, M1_mult_28_n321,
         M1_mult_28_n320, M1_mult_28_n319, M1_mult_28_n318, M1_mult_28_n317,
         M1_mult_28_n316, M1_mult_28_n315, M1_mult_28_n314, M1_mult_28_n313,
         M1_mult_28_n312, M1_mult_28_n311, M1_mult_28_n310, M1_mult_28_n309,
         M1_mult_28_n308, M1_mult_28_n307, M1_mult_28_n306, M1_mult_28_n305,
         M1_mult_28_n304, M1_mult_28_n303, M1_mult_28_n302, M1_mult_28_n301,
         M1_mult_28_n300, M1_mult_28_n299, M1_mult_28_n298, M1_mult_28_n297,
         M1_mult_28_n296, M1_mult_28_n295, M1_mult_28_n294, M1_mult_28_n293,
         M1_mult_28_n292, M1_mult_28_n291, M1_mult_28_n290, M1_mult_28_n289,
         M1_mult_28_n288, M1_mult_28_n287, M1_mult_28_n286, M1_mult_28_n285,
         M1_mult_28_n284, M1_mult_28_n283, M1_mult_28_n282, M1_mult_28_n281,
         M1_mult_28_n280, M1_mult_28_n279, M1_mult_28_n278, M1_mult_28_n277,
         M1_mult_28_n276, M1_mult_28_n275, M1_mult_28_n274, M1_mult_28_n273,
         M1_mult_28_n272, M1_mult_28_n271, M1_mult_28_n270, M1_mult_28_n269,
         M1_mult_28_n268, M1_mult_28_n140, M1_mult_28_n139, M1_mult_28_n138,
         M1_mult_28_n137, M1_mult_28_n136, M1_mult_28_n135, M1_mult_28_n132,
         M1_mult_28_n131, M1_mult_28_n130, M1_mult_28_n129, M1_mult_28_n128,
         M1_mult_28_n127, M1_mult_28_n126, M1_mult_28_n124, M1_mult_28_n123,
         M1_mult_28_n122, M1_mult_28_n121, M1_mult_28_n120, M1_mult_28_n119,
         M1_mult_28_n118, M1_mult_28_n117, M1_mult_28_n115, M1_mult_28_n114,
         M1_mult_28_n113, M1_mult_28_n111, M1_mult_28_n110, M1_mult_28_n109,
         M1_mult_28_n108, M1_mult_28_n106, M1_mult_28_n104, M1_mult_28_n103,
         M1_mult_28_n102, M1_mult_28_n101, M1_mult_28_n100, M1_mult_28_n99,
         M1_mult_28_n95, M1_mult_28_n94, M1_mult_28_n93, M1_mult_28_n79,
         M1_mult_28_n78, M1_mult_28_n77, M1_mult_28_n76, M1_mult_28_n75,
         M1_mult_28_n74, M1_mult_28_n73, M1_mult_28_n72, M1_mult_28_n71,
         M1_mult_28_n70, M1_mult_28_n69, M1_mult_28_n68, M1_mult_28_n67,
         M1_mult_28_n66, M1_mult_28_n65, M1_mult_28_n64, M1_mult_28_n63,
         M1_mult_28_n62, M1_mult_28_n61, M1_mult_28_n60, M1_mult_28_n59,
         M1_mult_28_n58, M1_mult_28_n57, M1_mult_28_n56, M1_mult_28_n55,
         M1_mult_28_n54, M1_mult_28_n53, M1_mult_28_n52, M1_mult_28_n51,
         M1_mult_28_n50, M1_mult_28_n49, M1_mult_28_n48, M1_mult_28_n46,
         M1_mult_28_n45, M1_mult_28_n44, M1_mult_28_n43, M1_mult_28_n42,
         M1_mult_28_n41, M1_mult_28_n40, M1_mult_28_n39, M1_mult_28_n38,
         M1_mult_28_n37, M1_mult_28_n36, M1_mult_28_n35, M1_mult_28_n34,
         M1_mult_28_n32, M1_mult_28_n31, M1_mult_28_n30, M1_mult_28_n29,
         M1_mult_28_n28, M1_mult_28_n27, M1_mult_28_n26, M1_mult_28_n25,
         M1_mult_28_n24, M1_mult_28_n22, M1_mult_28_n21, M1_mult_28_n20,
         M1_mult_28_n19, M1_mult_28_n18, M1_mult_28_n9, M1_mult_28_n8,
         M1_mult_28_n7, M1_mult_28_n6, M1_mult_28_n5, M1_mult_28_n4,
         M1_mult_28_n3, M1_mult_28_n2, M2_mult_28_n362, M2_mult_28_n361,
         M2_mult_28_n360, M2_mult_28_n359, M2_mult_28_n358, M2_mult_28_n357,
         M2_mult_28_n356, M2_mult_28_n355, M2_mult_28_n354, M2_mult_28_n353,
         M2_mult_28_n352, M2_mult_28_n351, M2_mult_28_n350, M2_mult_28_n349,
         M2_mult_28_n348, M2_mult_28_n347, M2_mult_28_n346, M2_mult_28_n345,
         M2_mult_28_n344, M2_mult_28_n343, M2_mult_28_n342, M2_mult_28_n341,
         M2_mult_28_n340, M2_mult_28_n339, M2_mult_28_n338, M2_mult_28_n337,
         M2_mult_28_n336, M2_mult_28_n335, M2_mult_28_n334, M2_mult_28_n333,
         M2_mult_28_n332, M2_mult_28_n331, M2_mult_28_n330, M2_mult_28_n329,
         M2_mult_28_n328, M2_mult_28_n327, M2_mult_28_n326, M2_mult_28_n325,
         M2_mult_28_n324, M2_mult_28_n323, M2_mult_28_n322, M2_mult_28_n321,
         M2_mult_28_n320, M2_mult_28_n319, M2_mult_28_n318, M2_mult_28_n317,
         M2_mult_28_n316, M2_mult_28_n315, M2_mult_28_n314, M2_mult_28_n313,
         M2_mult_28_n312, M2_mult_28_n311, M2_mult_28_n310, M2_mult_28_n309,
         M2_mult_28_n308, M2_mult_28_n307, M2_mult_28_n306, M2_mult_28_n305,
         M2_mult_28_n304, M2_mult_28_n303, M2_mult_28_n302, M2_mult_28_n301,
         M2_mult_28_n300, M2_mult_28_n299, M2_mult_28_n298, M2_mult_28_n297,
         M2_mult_28_n296, M2_mult_28_n295, M2_mult_28_n294, M2_mult_28_n293,
         M2_mult_28_n292, M2_mult_28_n291, M2_mult_28_n290, M2_mult_28_n289,
         M2_mult_28_n288, M2_mult_28_n287, M2_mult_28_n286, M2_mult_28_n285,
         M2_mult_28_n284, M2_mult_28_n283, M2_mult_28_n282, M2_mult_28_n281,
         M2_mult_28_n280, M2_mult_28_n279, M2_mult_28_n278, M2_mult_28_n277,
         M2_mult_28_n276, M2_mult_28_n275, M2_mult_28_n274, M2_mult_28_n273,
         M2_mult_28_n272, M2_mult_28_n271, M2_mult_28_n270, M2_mult_28_n269,
         M2_mult_28_n268, M2_mult_28_n140, M2_mult_28_n139, M2_mult_28_n138,
         M2_mult_28_n137, M2_mult_28_n136, M2_mult_28_n135, M2_mult_28_n132,
         M2_mult_28_n131, M2_mult_28_n130, M2_mult_28_n129, M2_mult_28_n128,
         M2_mult_28_n127, M2_mult_28_n126, M2_mult_28_n124, M2_mult_28_n123,
         M2_mult_28_n122, M2_mult_28_n121, M2_mult_28_n120, M2_mult_28_n119,
         M2_mult_28_n118, M2_mult_28_n117, M2_mult_28_n115, M2_mult_28_n114,
         M2_mult_28_n113, M2_mult_28_n111, M2_mult_28_n110, M2_mult_28_n109,
         M2_mult_28_n108, M2_mult_28_n106, M2_mult_28_n104, M2_mult_28_n103,
         M2_mult_28_n102, M2_mult_28_n101, M2_mult_28_n100, M2_mult_28_n99,
         M2_mult_28_n95, M2_mult_28_n94, M2_mult_28_n93, M2_mult_28_n79,
         M2_mult_28_n78, M2_mult_28_n77, M2_mult_28_n76, M2_mult_28_n75,
         M2_mult_28_n74, M2_mult_28_n73, M2_mult_28_n72, M2_mult_28_n71,
         M2_mult_28_n70, M2_mult_28_n69, M2_mult_28_n68, M2_mult_28_n67,
         M2_mult_28_n66, M2_mult_28_n65, M2_mult_28_n64, M2_mult_28_n63,
         M2_mult_28_n62, M2_mult_28_n61, M2_mult_28_n60, M2_mult_28_n59,
         M2_mult_28_n58, M2_mult_28_n57, M2_mult_28_n56, M2_mult_28_n55,
         M2_mult_28_n54, M2_mult_28_n53, M2_mult_28_n52, M2_mult_28_n51,
         M2_mult_28_n50, M2_mult_28_n49, M2_mult_28_n48, M2_mult_28_n46,
         M2_mult_28_n45, M2_mult_28_n44, M2_mult_28_n43, M2_mult_28_n42,
         M2_mult_28_n41, M2_mult_28_n40, M2_mult_28_n39, M2_mult_28_n38,
         M2_mult_28_n37, M2_mult_28_n36, M2_mult_28_n35, M2_mult_28_n34,
         M2_mult_28_n32, M2_mult_28_n31, M2_mult_28_n30, M2_mult_28_n29,
         M2_mult_28_n28, M2_mult_28_n27, M2_mult_28_n26, M2_mult_28_n25,
         M2_mult_28_n24, M2_mult_28_n22, M2_mult_28_n21, M2_mult_28_n20,
         M2_mult_28_n19, M2_mult_28_n18, M2_mult_28_n9, M2_mult_28_n8,
         M2_mult_28_n7, M2_mult_28_n6, M2_mult_28_n5, M2_mult_28_n4,
         M2_mult_28_n3, M2_mult_28_n2, M3_mult_28_n362, M3_mult_28_n361,
         M3_mult_28_n360, M3_mult_28_n359, M3_mult_28_n358, M3_mult_28_n357,
         M3_mult_28_n356, M3_mult_28_n355, M3_mult_28_n354, M3_mult_28_n353,
         M3_mult_28_n352, M3_mult_28_n351, M3_mult_28_n350, M3_mult_28_n349,
         M3_mult_28_n348, M3_mult_28_n347, M3_mult_28_n346, M3_mult_28_n345,
         M3_mult_28_n344, M3_mult_28_n343, M3_mult_28_n342, M3_mult_28_n341,
         M3_mult_28_n340, M3_mult_28_n339, M3_mult_28_n338, M3_mult_28_n337,
         M3_mult_28_n336, M3_mult_28_n335, M3_mult_28_n334, M3_mult_28_n333,
         M3_mult_28_n332, M3_mult_28_n331, M3_mult_28_n330, M3_mult_28_n329,
         M3_mult_28_n328, M3_mult_28_n327, M3_mult_28_n326, M3_mult_28_n325,
         M3_mult_28_n324, M3_mult_28_n323, M3_mult_28_n322, M3_mult_28_n321,
         M3_mult_28_n320, M3_mult_28_n319, M3_mult_28_n318, M3_mult_28_n317,
         M3_mult_28_n316, M3_mult_28_n315, M3_mult_28_n314, M3_mult_28_n313,
         M3_mult_28_n312, M3_mult_28_n311, M3_mult_28_n310, M3_mult_28_n309,
         M3_mult_28_n308, M3_mult_28_n307, M3_mult_28_n306, M3_mult_28_n305,
         M3_mult_28_n304, M3_mult_28_n303, M3_mult_28_n302, M3_mult_28_n301,
         M3_mult_28_n300, M3_mult_28_n299, M3_mult_28_n298, M3_mult_28_n297,
         M3_mult_28_n296, M3_mult_28_n295, M3_mult_28_n294, M3_mult_28_n293,
         M3_mult_28_n292, M3_mult_28_n291, M3_mult_28_n290, M3_mult_28_n289,
         M3_mult_28_n288, M3_mult_28_n287, M3_mult_28_n286, M3_mult_28_n285,
         M3_mult_28_n284, M3_mult_28_n283, M3_mult_28_n282, M3_mult_28_n281,
         M3_mult_28_n280, M3_mult_28_n279, M3_mult_28_n278, M3_mult_28_n277,
         M3_mult_28_n276, M3_mult_28_n275, M3_mult_28_n274, M3_mult_28_n273,
         M3_mult_28_n272, M3_mult_28_n271, M3_mult_28_n270, M3_mult_28_n269,
         M3_mult_28_n268, M3_mult_28_n140, M3_mult_28_n139, M3_mult_28_n138,
         M3_mult_28_n137, M3_mult_28_n136, M3_mult_28_n135, M3_mult_28_n132,
         M3_mult_28_n131, M3_mult_28_n130, M3_mult_28_n129, M3_mult_28_n128,
         M3_mult_28_n127, M3_mult_28_n126, M3_mult_28_n124, M3_mult_28_n123,
         M3_mult_28_n122, M3_mult_28_n121, M3_mult_28_n120, M3_mult_28_n119,
         M3_mult_28_n118, M3_mult_28_n117, M3_mult_28_n115, M3_mult_28_n114,
         M3_mult_28_n113, M3_mult_28_n111, M3_mult_28_n110, M3_mult_28_n109,
         M3_mult_28_n108, M3_mult_28_n106, M3_mult_28_n104, M3_mult_28_n103,
         M3_mult_28_n102, M3_mult_28_n101, M3_mult_28_n100, M3_mult_28_n99,
         M3_mult_28_n95, M3_mult_28_n94, M3_mult_28_n93, M3_mult_28_n79,
         M3_mult_28_n78, M3_mult_28_n77, M3_mult_28_n76, M3_mult_28_n75,
         M3_mult_28_n74, M3_mult_28_n73, M3_mult_28_n72, M3_mult_28_n71,
         M3_mult_28_n70, M3_mult_28_n69, M3_mult_28_n68, M3_mult_28_n67,
         M3_mult_28_n66, M3_mult_28_n65, M3_mult_28_n64, M3_mult_28_n63,
         M3_mult_28_n62, M3_mult_28_n61, M3_mult_28_n60, M3_mult_28_n59,
         M3_mult_28_n58, M3_mult_28_n57, M3_mult_28_n56, M3_mult_28_n55,
         M3_mult_28_n54, M3_mult_28_n53, M3_mult_28_n52, M3_mult_28_n51,
         M3_mult_28_n50, M3_mult_28_n49, M3_mult_28_n48, M3_mult_28_n46,
         M3_mult_28_n45, M3_mult_28_n44, M3_mult_28_n43, M3_mult_28_n42,
         M3_mult_28_n41, M3_mult_28_n40, M3_mult_28_n39, M3_mult_28_n38,
         M3_mult_28_n37, M3_mult_28_n36, M3_mult_28_n35, M3_mult_28_n34,
         M3_mult_28_n32, M3_mult_28_n31, M3_mult_28_n30, M3_mult_28_n29,
         M3_mult_28_n28, M3_mult_28_n27, M3_mult_28_n26, M3_mult_28_n25,
         M3_mult_28_n24, M3_mult_28_n22, M3_mult_28_n21, M3_mult_28_n20,
         M3_mult_28_n19, M3_mult_28_n18, M3_mult_28_n9, M3_mult_28_n8,
         M3_mult_28_n7, M3_mult_28_n6, M3_mult_28_n5, M3_mult_28_n4,
         M3_mult_28_n3, M3_mult_28_n2, VIN_DELAY1_n3, VIN_DELAY1_n2,
         VIN_DELAY1_n1, VIN_DELAY2_n6, VIN_DELAY2_n5, VIN_DELAY2_n4,
         VIN_DELAY3_n6, VIN_DELAY3_n5, VIN_DELAY3_n4, VIN_DELAY4_n6,
         VIN_DELAY4_n5, VIN_DELAY4_n4, VIN_DELAY5_n6, VIN_DELAY5_n5,
         VIN_DELAY5_n4;
  wire   [8:0] D1_out;
  wire   [8:0] M1_out;
  wire   [8:0] D2_out;
  wire   [8:0] A1_out;
  wire   [8:0] D3_out;
  wire   [8:0] M2_out;
  wire   [8:0] D4_out;
  wire   [8:0] A2_out;
  wire   [8:0] D5_out;
  wire   [8:0] D6_out;
  wire   [8:0] A3_out;
  wire   [8:0] D7_out;
  wire   [8:0] M3_out;
  wire   [9:1] A1_add_25_carry;
  wire   [9:1] A2_add_25_carry;
  wire   [9:1] A3_add_25_carry;

  NAND2_X1 D1_U19 ( .A1(x_n[8]), .A2(v_in), .ZN(D1_n9) );
  OAI21_X1 D1_U18 ( .B1(v_in), .B2(D1_n18), .A(D1_n9), .ZN(D1_n27) );
  NAND2_X1 D1_U17 ( .A1(x_n[7]), .A2(v_in), .ZN(D1_n8) );
  OAI21_X1 D1_U16 ( .B1(v_in), .B2(D1_n17), .A(D1_n8), .ZN(D1_n26) );
  NAND2_X1 D1_U15 ( .A1(x_n[6]), .A2(v_in), .ZN(D1_n7) );
  OAI21_X1 D1_U14 ( .B1(v_in), .B2(D1_n16), .A(D1_n7), .ZN(D1_n25) );
  NAND2_X1 D1_U13 ( .A1(x_n[5]), .A2(v_in), .ZN(D1_n6) );
  OAI21_X1 D1_U12 ( .B1(v_in), .B2(D1_n15), .A(D1_n6), .ZN(D1_n24) );
  NAND2_X1 D1_U11 ( .A1(x_n[4]), .A2(v_in), .ZN(D1_n5) );
  OAI21_X1 D1_U10 ( .B1(v_in), .B2(D1_n14), .A(D1_n5), .ZN(D1_n23) );
  NAND2_X1 D1_U9 ( .A1(x_n[3]), .A2(v_in), .ZN(D1_n4) );
  OAI21_X1 D1_U8 ( .B1(v_in), .B2(D1_n13), .A(D1_n4), .ZN(D1_n22) );
  NAND2_X1 D1_U7 ( .A1(x_n[2]), .A2(v_in), .ZN(D1_n3) );
  OAI21_X1 D1_U6 ( .B1(v_in), .B2(D1_n12), .A(D1_n3), .ZN(D1_n21) );
  NAND2_X1 D1_U5 ( .A1(x_n[1]), .A2(v_in), .ZN(D1_n2) );
  OAI21_X1 D1_U4 ( .B1(v_in), .B2(D1_n11), .A(D1_n2), .ZN(D1_n20) );
  NAND2_X1 D1_U3 ( .A1(v_in), .A2(x_n[0]), .ZN(D1_n1) );
  OAI21_X1 D1_U2 ( .B1(v_in), .B2(D1_n10), .A(D1_n1), .ZN(D1_n19) );
  DFFR_X1 D1_d_out_reg_0_ ( .D(D1_n19), .CK(clk), .RN(rst_n), .Q(D1_out[0]), 
        .QN(D1_n10) );
  DFFR_X1 D1_d_out_reg_1_ ( .D(D1_n20), .CK(clk), .RN(rst_n), .Q(D1_out[1]), 
        .QN(D1_n11) );
  DFFR_X1 D1_d_out_reg_2_ ( .D(D1_n21), .CK(clk), .RN(rst_n), .Q(D1_out[2]), 
        .QN(D1_n12) );
  DFFR_X1 D1_d_out_reg_3_ ( .D(D1_n22), .CK(clk), .RN(rst_n), .Q(D1_out[3]), 
        .QN(D1_n13) );
  DFFR_X1 D1_d_out_reg_4_ ( .D(D1_n23), .CK(clk), .RN(rst_n), .Q(D1_out[4]), 
        .QN(D1_n14) );
  DFFR_X1 D1_d_out_reg_5_ ( .D(D1_n24), .CK(clk), .RN(rst_n), .Q(D1_out[5]), 
        .QN(D1_n15) );
  DFFR_X1 D1_d_out_reg_6_ ( .D(D1_n25), .CK(clk), .RN(rst_n), .Q(D1_out[6]), 
        .QN(D1_n16) );
  DFFR_X1 D1_d_out_reg_7_ ( .D(D1_n26), .CK(clk), .RN(rst_n), .Q(D1_out[7]), 
        .QN(D1_n17) );
  DFFR_X1 D1_d_out_reg_8_ ( .D(D1_n27), .CK(clk), .RN(rst_n), .Q(D1_out[8]), 
        .QN(D1_n18) );
  BUF_X1 D2_U21 ( .A(vin_delay1_out), .Z(D2_n29) );
  BUF_X1 D2_U20 ( .A(vin_delay1_out), .Z(D2_n28) );
  NAND2_X1 D2_U19 ( .A1(D2_n29), .A2(M1_out[0]), .ZN(D2_n56) );
  OAI21_X1 D2_U18 ( .B1(D2_n28), .B2(D2_n47), .A(D2_n56), .ZN(D2_n38) );
  NAND2_X1 D2_U17 ( .A1(M1_out[2]), .A2(D2_n28), .ZN(D2_n54) );
  OAI21_X1 D2_U16 ( .B1(D2_n29), .B2(D2_n45), .A(D2_n54), .ZN(D2_n36) );
  NAND2_X1 D2_U15 ( .A1(M1_out[1]), .A2(D2_n28), .ZN(D2_n55) );
  OAI21_X1 D2_U14 ( .B1(D2_n29), .B2(D2_n46), .A(D2_n55), .ZN(D2_n37) );
  NAND2_X1 D2_U13 ( .A1(M1_out[8]), .A2(D2_n28), .ZN(D2_n48) );
  OAI21_X1 D2_U12 ( .B1(D2_n28), .B2(D2_n39), .A(D2_n48), .ZN(D2_n30) );
  NAND2_X1 D2_U11 ( .A1(M1_out[7]), .A2(D2_n28), .ZN(D2_n49) );
  OAI21_X1 D2_U10 ( .B1(D2_n28), .B2(D2_n40), .A(D2_n49), .ZN(D2_n31) );
  NAND2_X1 D2_U9 ( .A1(M1_out[6]), .A2(D2_n28), .ZN(D2_n50) );
  OAI21_X1 D2_U8 ( .B1(D2_n28), .B2(D2_n41), .A(D2_n50), .ZN(D2_n32) );
  NAND2_X1 D2_U7 ( .A1(M1_out[5]), .A2(D2_n28), .ZN(D2_n51) );
  OAI21_X1 D2_U6 ( .B1(D2_n29), .B2(D2_n42), .A(D2_n51), .ZN(D2_n33) );
  NAND2_X1 D2_U5 ( .A1(M1_out[4]), .A2(D2_n28), .ZN(D2_n52) );
  OAI21_X1 D2_U4 ( .B1(D2_n29), .B2(D2_n43), .A(D2_n52), .ZN(D2_n34) );
  NAND2_X1 D2_U3 ( .A1(M1_out[3]), .A2(D2_n28), .ZN(D2_n53) );
  OAI21_X1 D2_U2 ( .B1(D2_n29), .B2(D2_n44), .A(D2_n53), .ZN(D2_n35) );
  DFFR_X1 D2_d_out_reg_0_ ( .D(D2_n38), .CK(clk), .RN(rst_n), .Q(D2_out[0]), 
        .QN(D2_n47) );
  DFFR_X1 D2_d_out_reg_1_ ( .D(D2_n37), .CK(clk), .RN(rst_n), .Q(D2_out[1]), 
        .QN(D2_n46) );
  DFFR_X1 D2_d_out_reg_2_ ( .D(D2_n36), .CK(clk), .RN(rst_n), .Q(D2_out[2]), 
        .QN(D2_n45) );
  DFFR_X1 D2_d_out_reg_3_ ( .D(D2_n35), .CK(clk), .RN(rst_n), .Q(D2_out[3]), 
        .QN(D2_n44) );
  DFFR_X1 D2_d_out_reg_4_ ( .D(D2_n34), .CK(clk), .RN(rst_n), .Q(D2_out[4]), 
        .QN(D2_n43) );
  DFFR_X1 D2_d_out_reg_5_ ( .D(D2_n33), .CK(clk), .RN(rst_n), .Q(D2_out[5]), 
        .QN(D2_n42) );
  DFFR_X1 D2_d_out_reg_6_ ( .D(D2_n32), .CK(clk), .RN(rst_n), .Q(D2_out[6]), 
        .QN(D2_n41) );
  DFFR_X1 D2_d_out_reg_7_ ( .D(D2_n31), .CK(clk), .RN(rst_n), .Q(D2_out[7]), 
        .QN(D2_n40) );
  DFFR_X1 D2_d_out_reg_8_ ( .D(D2_n30), .CK(clk), .RN(rst_n), .Q(D2_out[8]), 
        .QN(D2_n39) );
  NAND2_X1 D3_U21 ( .A1(A1_out[1]), .A2(D3_n28), .ZN(D3_n55) );
  OAI21_X1 D3_U20 ( .B1(D3_n29), .B2(D3_n46), .A(D3_n55), .ZN(D3_n37) );
  NAND2_X1 D3_U19 ( .A1(D3_n29), .A2(A1_out[0]), .ZN(D3_n56) );
  OAI21_X1 D3_U18 ( .B1(D3_n28), .B2(D3_n47), .A(D3_n56), .ZN(D3_n38) );
  NAND2_X1 D3_U17 ( .A1(A1_out[4]), .A2(D3_n28), .ZN(D3_n52) );
  OAI21_X1 D3_U16 ( .B1(D3_n29), .B2(D3_n43), .A(D3_n52), .ZN(D3_n34) );
  NAND2_X1 D3_U15 ( .A1(A1_out[3]), .A2(D3_n28), .ZN(D3_n53) );
  OAI21_X1 D3_U14 ( .B1(D3_n29), .B2(D3_n44), .A(D3_n53), .ZN(D3_n35) );
  NAND2_X1 D3_U13 ( .A1(A1_out[2]), .A2(D3_n28), .ZN(D3_n54) );
  OAI21_X1 D3_U12 ( .B1(D3_n29), .B2(D3_n45), .A(D3_n54), .ZN(D3_n36) );
  BUF_X1 D3_U11 ( .A(vin_delay1_out), .Z(D3_n29) );
  BUF_X1 D3_U10 ( .A(vin_delay1_out), .Z(D3_n28) );
  NAND2_X1 D3_U9 ( .A1(A1_out[8]), .A2(D3_n28), .ZN(D3_n48) );
  OAI21_X1 D3_U8 ( .B1(D3_n28), .B2(D3_n39), .A(D3_n48), .ZN(D3_n30) );
  NAND2_X1 D3_U7 ( .A1(A1_out[7]), .A2(D3_n28), .ZN(D3_n49) );
  OAI21_X1 D3_U6 ( .B1(D3_n28), .B2(D3_n40), .A(D3_n49), .ZN(D3_n31) );
  NAND2_X1 D3_U5 ( .A1(A1_out[6]), .A2(D3_n28), .ZN(D3_n50) );
  OAI21_X1 D3_U4 ( .B1(D3_n28), .B2(D3_n41), .A(D3_n50), .ZN(D3_n32) );
  NAND2_X1 D3_U3 ( .A1(A1_out[5]), .A2(D3_n28), .ZN(D3_n51) );
  OAI21_X1 D3_U2 ( .B1(D3_n29), .B2(D3_n42), .A(D3_n51), .ZN(D3_n33) );
  DFFR_X1 D3_d_out_reg_0_ ( .D(D3_n38), .CK(clk), .RN(rst_n), .Q(D3_out[0]), 
        .QN(D3_n47) );
  DFFR_X1 D3_d_out_reg_1_ ( .D(D3_n37), .CK(clk), .RN(rst_n), .Q(D3_out[1]), 
        .QN(D3_n46) );
  DFFR_X1 D3_d_out_reg_2_ ( .D(D3_n36), .CK(clk), .RN(rst_n), .Q(D3_out[2]), 
        .QN(D3_n45) );
  DFFR_X1 D3_d_out_reg_3_ ( .D(D3_n35), .CK(clk), .RN(rst_n), .Q(D3_out[3]), 
        .QN(D3_n44) );
  DFFR_X1 D3_d_out_reg_4_ ( .D(D3_n34), .CK(clk), .RN(rst_n), .Q(D3_out[4]), 
        .QN(D3_n43) );
  DFFR_X1 D3_d_out_reg_5_ ( .D(D3_n33), .CK(clk), .RN(rst_n), .Q(D3_out[5]), 
        .QN(D3_n42) );
  DFFR_X1 D3_d_out_reg_6_ ( .D(D3_n32), .CK(clk), .RN(rst_n), .Q(D3_out[6]), 
        .QN(D3_n41) );
  DFFR_X1 D3_d_out_reg_7_ ( .D(D3_n31), .CK(clk), .RN(rst_n), .Q(D3_out[7]), 
        .QN(D3_n40) );
  DFFR_X1 D3_d_out_reg_8_ ( .D(D3_n30), .CK(clk), .RN(rst_n), .Q(D3_out[8]), 
        .QN(D3_n39) );
  NAND2_X1 D4_U19 ( .A1(1'b1), .A2(M2_out[0]), .ZN(D4_n54) );
  OAI21_X1 D4_U18 ( .B1(1'b1), .B2(D4_n45), .A(D4_n54), .ZN(D4_n36) );
  NAND2_X1 D4_U17 ( .A1(M2_out[2]), .A2(1'b1), .ZN(D4_n52) );
  OAI21_X1 D4_U16 ( .B1(1'b1), .B2(D4_n43), .A(D4_n52), .ZN(D4_n34) );
  NAND2_X1 D4_U15 ( .A1(M2_out[1]), .A2(1'b1), .ZN(D4_n53) );
  OAI21_X1 D4_U14 ( .B1(1'b1), .B2(D4_n44), .A(D4_n53), .ZN(D4_n35) );
  NAND2_X1 D4_U13 ( .A1(M2_out[7]), .A2(1'b1), .ZN(D4_n47) );
  OAI21_X1 D4_U12 ( .B1(1'b1), .B2(D4_n38), .A(D4_n47), .ZN(D4_n29) );
  NAND2_X1 D4_U11 ( .A1(M2_out[6]), .A2(1'b1), .ZN(D4_n48) );
  OAI21_X1 D4_U10 ( .B1(1'b1), .B2(D4_n39), .A(D4_n48), .ZN(D4_n30) );
  NAND2_X1 D4_U9 ( .A1(M2_out[5]), .A2(1'b1), .ZN(D4_n49) );
  OAI21_X1 D4_U8 ( .B1(1'b1), .B2(D4_n40), .A(D4_n49), .ZN(D4_n31) );
  NAND2_X1 D4_U7 ( .A1(M2_out[4]), .A2(1'b1), .ZN(D4_n50) );
  OAI21_X1 D4_U6 ( .B1(1'b1), .B2(D4_n41), .A(D4_n50), .ZN(D4_n32) );
  NAND2_X1 D4_U5 ( .A1(M2_out[3]), .A2(1'b1), .ZN(D4_n51) );
  OAI21_X1 D4_U4 ( .B1(1'b1), .B2(D4_n42), .A(D4_n51), .ZN(D4_n33) );
  NAND2_X1 D4_U3 ( .A1(M2_out[8]), .A2(1'b1), .ZN(D4_n46) );
  OAI21_X1 D4_U2 ( .B1(1'b1), .B2(D4_n37), .A(D4_n46), .ZN(D4_n28) );
  DFFR_X1 D4_d_out_reg_0_ ( .D(D4_n36), .CK(clk), .RN(rst_n), .Q(D4_out[0]), 
        .QN(D4_n45) );
  DFFR_X1 D4_d_out_reg_1_ ( .D(D4_n35), .CK(clk), .RN(rst_n), .Q(D4_out[1]), 
        .QN(D4_n44) );
  DFFR_X1 D4_d_out_reg_2_ ( .D(D4_n34), .CK(clk), .RN(rst_n), .Q(D4_out[2]), 
        .QN(D4_n43) );
  DFFR_X1 D4_d_out_reg_3_ ( .D(D4_n33), .CK(clk), .RN(rst_n), .Q(D4_out[3]), 
        .QN(D4_n42) );
  DFFR_X1 D4_d_out_reg_4_ ( .D(D4_n32), .CK(clk), .RN(rst_n), .Q(D4_out[4]), 
        .QN(D4_n41) );
  DFFR_X1 D4_d_out_reg_5_ ( .D(D4_n31), .CK(clk), .RN(rst_n), .Q(D4_out[5]), 
        .QN(D4_n40) );
  DFFR_X1 D4_d_out_reg_6_ ( .D(D4_n30), .CK(clk), .RN(rst_n), .Q(D4_out[6]), 
        .QN(D4_n39) );
  DFFR_X1 D4_d_out_reg_7_ ( .D(D4_n29), .CK(clk), .RN(rst_n), .Q(D4_out[7]), 
        .QN(D4_n38) );
  DFFR_X1 D4_d_out_reg_8_ ( .D(D4_n28), .CK(clk), .RN(rst_n), .Q(D4_out[8]), 
        .QN(D4_n37) );
  NAND2_X1 D5_U19 ( .A1(A2_out[5]), .A2(1'b1), .ZN(D5_n49) );
  OAI21_X1 D5_U18 ( .B1(1'b1), .B2(D5_n40), .A(D5_n49), .ZN(D5_n31) );
  NAND2_X1 D5_U17 ( .A1(A2_out[4]), .A2(1'b1), .ZN(D5_n50) );
  OAI21_X1 D5_U16 ( .B1(1'b1), .B2(D5_n41), .A(D5_n50), .ZN(D5_n32) );
  NAND2_X1 D5_U15 ( .A1(A2_out[3]), .A2(1'b1), .ZN(D5_n51) );
  OAI21_X1 D5_U14 ( .B1(1'b1), .B2(D5_n42), .A(D5_n51), .ZN(D5_n33) );
  NAND2_X1 D5_U13 ( .A1(A2_out[2]), .A2(1'b1), .ZN(D5_n52) );
  OAI21_X1 D5_U12 ( .B1(1'b1), .B2(D5_n43), .A(D5_n52), .ZN(D5_n34) );
  NAND2_X1 D5_U11 ( .A1(A2_out[1]), .A2(1'b1), .ZN(D5_n53) );
  OAI21_X1 D5_U10 ( .B1(1'b1), .B2(D5_n44), .A(D5_n53), .ZN(D5_n35) );
  NAND2_X1 D5_U9 ( .A1(1'b1), .A2(A2_out[0]), .ZN(D5_n54) );
  OAI21_X1 D5_U8 ( .B1(1'b1), .B2(D5_n45), .A(D5_n54), .ZN(D5_n36) );
  NAND2_X1 D5_U7 ( .A1(A2_out[8]), .A2(1'b1), .ZN(D5_n46) );
  OAI21_X1 D5_U6 ( .B1(1'b1), .B2(D5_n37), .A(D5_n46), .ZN(D5_n28) );
  NAND2_X1 D5_U5 ( .A1(A2_out[7]), .A2(1'b1), .ZN(D5_n47) );
  OAI21_X1 D5_U4 ( .B1(1'b1), .B2(D5_n38), .A(D5_n47), .ZN(D5_n29) );
  NAND2_X1 D5_U3 ( .A1(A2_out[6]), .A2(1'b1), .ZN(D5_n48) );
  OAI21_X1 D5_U2 ( .B1(1'b1), .B2(D5_n39), .A(D5_n48), .ZN(D5_n30) );
  DFFR_X1 D5_d_out_reg_0_ ( .D(D5_n36), .CK(clk), .RN(rst_n), .Q(D5_out[0]), 
        .QN(D5_n45) );
  DFFR_X1 D5_d_out_reg_1_ ( .D(D5_n35), .CK(clk), .RN(rst_n), .Q(D5_out[1]), 
        .QN(D5_n44) );
  DFFR_X1 D5_d_out_reg_2_ ( .D(D5_n34), .CK(clk), .RN(rst_n), .Q(D5_out[2]), 
        .QN(D5_n43) );
  DFFR_X1 D5_d_out_reg_3_ ( .D(D5_n33), .CK(clk), .RN(rst_n), .Q(D5_out[3]), 
        .QN(D5_n42) );
  DFFR_X1 D5_d_out_reg_4_ ( .D(D5_n32), .CK(clk), .RN(rst_n), .Q(D5_out[4]), 
        .QN(D5_n41) );
  DFFR_X1 D5_d_out_reg_5_ ( .D(D5_n31), .CK(clk), .RN(rst_n), .Q(D5_out[5]), 
        .QN(D5_n40) );
  DFFR_X1 D5_d_out_reg_6_ ( .D(D5_n30), .CK(clk), .RN(rst_n), .Q(D5_out[6]), 
        .QN(D5_n39) );
  DFFR_X1 D5_d_out_reg_7_ ( .D(D5_n29), .CK(clk), .RN(rst_n), .Q(D5_out[7]), 
        .QN(D5_n38) );
  DFFR_X1 D5_d_out_reg_8_ ( .D(D5_n28), .CK(clk), .RN(rst_n), .Q(D5_out[8]), 
        .QN(D5_n37) );
  NAND2_X1 D6_U21 ( .A1(D5_out[8]), .A2(D6_n28), .ZN(D6_n48) );
  OAI21_X1 D6_U20 ( .B1(D6_n28), .B2(D6_n39), .A(D6_n48), .ZN(D6_n30) );
  NAND2_X1 D6_U19 ( .A1(D5_out[7]), .A2(D6_n28), .ZN(D6_n49) );
  OAI21_X1 D6_U18 ( .B1(D6_n28), .B2(D6_n40), .A(D6_n49), .ZN(D6_n31) );
  NAND2_X1 D6_U17 ( .A1(D5_out[6]), .A2(D6_n28), .ZN(D6_n50) );
  OAI21_X1 D6_U16 ( .B1(D6_n28), .B2(D6_n41), .A(D6_n50), .ZN(D6_n32) );
  NAND2_X1 D6_U15 ( .A1(D6_n29), .A2(D5_out[0]), .ZN(D6_n56) );
  OAI21_X1 D6_U14 ( .B1(D6_n28), .B2(D6_n47), .A(D6_n56), .ZN(D6_n38) );
  NAND2_X1 D6_U13 ( .A1(D5_out[5]), .A2(D6_n28), .ZN(D6_n51) );
  OAI21_X1 D6_U12 ( .B1(D6_n29), .B2(D6_n42), .A(D6_n51), .ZN(D6_n33) );
  NAND2_X1 D6_U11 ( .A1(D5_out[4]), .A2(D6_n28), .ZN(D6_n52) );
  OAI21_X1 D6_U10 ( .B1(D6_n29), .B2(D6_n43), .A(D6_n52), .ZN(D6_n34) );
  NAND2_X1 D6_U9 ( .A1(D5_out[3]), .A2(D6_n28), .ZN(D6_n53) );
  OAI21_X1 D6_U8 ( .B1(D6_n29), .B2(D6_n44), .A(D6_n53), .ZN(D6_n35) );
  NAND2_X1 D6_U7 ( .A1(D5_out[2]), .A2(D6_n28), .ZN(D6_n54) );
  OAI21_X1 D6_U6 ( .B1(D6_n29), .B2(D6_n45), .A(D6_n54), .ZN(D6_n36) );
  NAND2_X1 D6_U5 ( .A1(D5_out[1]), .A2(D6_n28), .ZN(D6_n55) );
  OAI21_X1 D6_U4 ( .B1(D6_n29), .B2(D6_n46), .A(D6_n55), .ZN(D6_n37) );
  BUF_X1 D6_U3 ( .A(D4_en), .Z(D6_n29) );
  BUF_X1 D6_U2 ( .A(D4_en), .Z(D6_n28) );
  DFFR_X1 D6_d_out_reg_0_ ( .D(D6_n38), .CK(clk), .RN(rst_n), .Q(D6_out[0]), 
        .QN(D6_n47) );
  DFFR_X1 D6_d_out_reg_1_ ( .D(D6_n37), .CK(clk), .RN(rst_n), .Q(D6_out[1]), 
        .QN(D6_n46) );
  DFFR_X1 D6_d_out_reg_2_ ( .D(D6_n36), .CK(clk), .RN(rst_n), .Q(D6_out[2]), 
        .QN(D6_n45) );
  DFFR_X1 D6_d_out_reg_3_ ( .D(D6_n35), .CK(clk), .RN(rst_n), .Q(D6_out[3]), 
        .QN(D6_n44) );
  DFFR_X1 D6_d_out_reg_4_ ( .D(D6_n34), .CK(clk), .RN(rst_n), .Q(D6_out[4]), 
        .QN(D6_n43) );
  DFFR_X1 D6_d_out_reg_5_ ( .D(D6_n33), .CK(clk), .RN(rst_n), .Q(D6_out[5]), 
        .QN(D6_n42) );
  DFFR_X1 D6_d_out_reg_6_ ( .D(D6_n32), .CK(clk), .RN(rst_n), .Q(D6_out[6]), 
        .QN(D6_n41) );
  DFFR_X1 D6_d_out_reg_7_ ( .D(D6_n31), .CK(clk), .RN(rst_n), .Q(D6_out[7]), 
        .QN(D6_n40) );
  DFFR_X1 D6_d_out_reg_8_ ( .D(D6_n30), .CK(clk), .RN(rst_n), .Q(D6_out[8]), 
        .QN(D6_n39) );
  NAND2_X1 D7_U21 ( .A1(A3_out[2]), .A2(D7_n28), .ZN(D7_n54) );
  OAI21_X1 D7_U20 ( .B1(D7_n29), .B2(D7_n45), .A(D7_n54), .ZN(D7_n36) );
  NAND2_X1 D7_U19 ( .A1(D7_n29), .A2(A3_out[0]), .ZN(D7_n56) );
  OAI21_X1 D7_U18 ( .B1(D7_n28), .B2(D7_n47), .A(D7_n56), .ZN(D7_n38) );
  NAND2_X1 D7_U17 ( .A1(A3_out[4]), .A2(D7_n28), .ZN(D7_n52) );
  OAI21_X1 D7_U16 ( .B1(D7_n29), .B2(D7_n43), .A(D7_n52), .ZN(D7_n34) );
  NAND2_X1 D7_U15 ( .A1(A3_out[3]), .A2(D7_n28), .ZN(D7_n53) );
  OAI21_X1 D7_U14 ( .B1(D7_n29), .B2(D7_n44), .A(D7_n53), .ZN(D7_n35) );
  NAND2_X1 D7_U13 ( .A1(A3_out[1]), .A2(D7_n28), .ZN(D7_n55) );
  OAI21_X1 D7_U12 ( .B1(D7_n29), .B2(D7_n46), .A(D7_n55), .ZN(D7_n37) );
  BUF_X1 D7_U11 ( .A(D4_en), .Z(D7_n29) );
  BUF_X1 D7_U10 ( .A(D4_en), .Z(D7_n28) );
  NAND2_X1 D7_U9 ( .A1(A3_out[8]), .A2(D7_n28), .ZN(D7_n48) );
  OAI21_X1 D7_U8 ( .B1(D7_n28), .B2(D7_n39), .A(D7_n48), .ZN(D7_n30) );
  NAND2_X1 D7_U7 ( .A1(A3_out[7]), .A2(D7_n28), .ZN(D7_n49) );
  OAI21_X1 D7_U6 ( .B1(D7_n28), .B2(D7_n40), .A(D7_n49), .ZN(D7_n31) );
  NAND2_X1 D7_U5 ( .A1(A3_out[6]), .A2(D7_n28), .ZN(D7_n50) );
  OAI21_X1 D7_U4 ( .B1(D7_n28), .B2(D7_n41), .A(D7_n50), .ZN(D7_n32) );
  NAND2_X1 D7_U3 ( .A1(A3_out[5]), .A2(D7_n28), .ZN(D7_n51) );
  OAI21_X1 D7_U2 ( .B1(D7_n29), .B2(D7_n42), .A(D7_n51), .ZN(D7_n33) );
  DFFR_X1 D7_d_out_reg_0_ ( .D(D7_n38), .CK(clk), .RN(rst_n), .Q(D7_out[0]), 
        .QN(D7_n47) );
  DFFR_X1 D7_d_out_reg_1_ ( .D(D7_n37), .CK(clk), .RN(rst_n), .Q(D7_out[1]), 
        .QN(D7_n46) );
  DFFR_X1 D7_d_out_reg_2_ ( .D(D7_n36), .CK(clk), .RN(rst_n), .Q(D7_out[2]), 
        .QN(D7_n45) );
  DFFR_X1 D7_d_out_reg_3_ ( .D(D7_n35), .CK(clk), .RN(rst_n), .Q(D7_out[3]), 
        .QN(D7_n44) );
  DFFR_X1 D7_d_out_reg_4_ ( .D(D7_n34), .CK(clk), .RN(rst_n), .Q(D7_out[4]), 
        .QN(D7_n43) );
  DFFR_X1 D7_d_out_reg_5_ ( .D(D7_n33), .CK(clk), .RN(rst_n), .Q(D7_out[5]), 
        .QN(D7_n42) );
  DFFR_X1 D7_d_out_reg_6_ ( .D(D7_n32), .CK(clk), .RN(rst_n), .Q(D7_out[6]), 
        .QN(D7_n41) );
  DFFR_X1 D7_d_out_reg_7_ ( .D(D7_n31), .CK(clk), .RN(rst_n), .Q(D7_out[7]), 
        .QN(D7_n40) );
  DFFR_X1 D7_d_out_reg_8_ ( .D(D7_n30), .CK(clk), .RN(rst_n), .Q(D7_out[8]), 
        .QN(D7_n39) );
  NAND2_X1 D8_U19 ( .A1(M3_out[2]), .A2(vin_delay4_out), .ZN(D8_n52) );
  OAI21_X1 D8_U18 ( .B1(vin_delay4_out), .B2(D8_n43), .A(D8_n52), .ZN(D8_n34)
         );
  NAND2_X1 D8_U17 ( .A1(M3_out[1]), .A2(vin_delay4_out), .ZN(D8_n53) );
  OAI21_X1 D8_U16 ( .B1(vin_delay4_out), .B2(D8_n44), .A(D8_n53), .ZN(D8_n35)
         );
  NAND2_X1 D8_U15 ( .A1(vin_delay4_out), .A2(M3_out[0]), .ZN(D8_n54) );
  OAI21_X1 D8_U14 ( .B1(vin_delay4_out), .B2(D8_n45), .A(D8_n54), .ZN(D8_n36)
         );
  NAND2_X1 D8_U13 ( .A1(M3_out[8]), .A2(vin_delay4_out), .ZN(D8_n46) );
  OAI21_X1 D8_U12 ( .B1(vin_delay4_out), .B2(D8_n37), .A(D8_n46), .ZN(D8_n28)
         );
  NAND2_X1 D8_U11 ( .A1(M3_out[7]), .A2(vin_delay4_out), .ZN(D8_n47) );
  OAI21_X1 D8_U10 ( .B1(vin_delay4_out), .B2(D8_n38), .A(D8_n47), .ZN(D8_n29)
         );
  NAND2_X1 D8_U9 ( .A1(M3_out[6]), .A2(vin_delay4_out), .ZN(D8_n48) );
  OAI21_X1 D8_U8 ( .B1(vin_delay4_out), .B2(D8_n39), .A(D8_n48), .ZN(D8_n30)
         );
  NAND2_X1 D8_U7 ( .A1(M3_out[5]), .A2(vin_delay4_out), .ZN(D8_n49) );
  OAI21_X1 D8_U6 ( .B1(vin_delay4_out), .B2(D8_n40), .A(D8_n49), .ZN(D8_n31)
         );
  NAND2_X1 D8_U5 ( .A1(M3_out[4]), .A2(vin_delay4_out), .ZN(D8_n50) );
  OAI21_X1 D8_U4 ( .B1(vin_delay4_out), .B2(D8_n41), .A(D8_n50), .ZN(D8_n32)
         );
  NAND2_X1 D8_U3 ( .A1(M3_out[3]), .A2(vin_delay4_out), .ZN(D8_n51) );
  OAI21_X1 D8_U2 ( .B1(vin_delay4_out), .B2(D8_n42), .A(D8_n51), .ZN(D8_n33)
         );
  DFFR_X1 D8_d_out_reg_0_ ( .D(D8_n36), .CK(clk), .RN(rst_n), .Q(y_n[0]), .QN(
        D8_n45) );
  DFFR_X1 D8_d_out_reg_1_ ( .D(D8_n35), .CK(clk), .RN(rst_n), .Q(y_n[1]), .QN(
        D8_n44) );
  DFFR_X1 D8_d_out_reg_2_ ( .D(D8_n34), .CK(clk), .RN(rst_n), .Q(y_n[2]), .QN(
        D8_n43) );
  DFFR_X1 D8_d_out_reg_3_ ( .D(D8_n33), .CK(clk), .RN(rst_n), .Q(y_n[3]), .QN(
        D8_n42) );
  DFFR_X1 D8_d_out_reg_4_ ( .D(D8_n32), .CK(clk), .RN(rst_n), .Q(y_n[4]), .QN(
        D8_n41) );
  DFFR_X1 D8_d_out_reg_5_ ( .D(D8_n31), .CK(clk), .RN(rst_n), .Q(y_n[5]), .QN(
        D8_n40) );
  DFFR_X1 D8_d_out_reg_6_ ( .D(D8_n30), .CK(clk), .RN(rst_n), .Q(y_n[6]), .QN(
        D8_n39) );
  DFFR_X1 D8_d_out_reg_7_ ( .D(D8_n29), .CK(clk), .RN(rst_n), .Q(y_n[7]), .QN(
        D8_n38) );
  DFFR_X1 D8_d_out_reg_8_ ( .D(D8_n28), .CK(clk), .RN(rst_n), .Q(y_n[8]), .QN(
        D8_n37) );
  AND2_X1 A1_add_25_U1 ( .A1(D2_out[0]), .A2(D1_out[0]), .ZN(
        A1_add_25_carry[1]) );
  FA_X1 A1_add_25_U1_1 ( .A(D1_out[1]), .B(D2_out[1]), .CI(A1_add_25_carry[1]), 
        .CO(A1_add_25_carry[2]), .S(A1_out[0]) );
  FA_X1 A1_add_25_U1_2 ( .A(D1_out[2]), .B(D2_out[2]), .CI(A1_add_25_carry[2]), 
        .CO(A1_add_25_carry[3]), .S(A1_out[1]) );
  FA_X1 A1_add_25_U1_3 ( .A(D1_out[3]), .B(D2_out[3]), .CI(A1_add_25_carry[3]), 
        .CO(A1_add_25_carry[4]), .S(A1_out[2]) );
  FA_X1 A1_add_25_U1_4 ( .A(D1_out[4]), .B(D2_out[4]), .CI(A1_add_25_carry[4]), 
        .CO(A1_add_25_carry[5]), .S(A1_out[3]) );
  FA_X1 A1_add_25_U1_5 ( .A(D1_out[5]), .B(D2_out[5]), .CI(A1_add_25_carry[5]), 
        .CO(A1_add_25_carry[6]), .S(A1_out[4]) );
  FA_X1 A1_add_25_U1_6 ( .A(D1_out[6]), .B(D2_out[6]), .CI(A1_add_25_carry[6]), 
        .CO(A1_add_25_carry[7]), .S(A1_out[5]) );
  FA_X1 A1_add_25_U1_7 ( .A(D1_out[7]), .B(D2_out[7]), .CI(A1_add_25_carry[7]), 
        .CO(A1_add_25_carry[8]), .S(A1_out[6]) );
  FA_X1 A1_add_25_U1_8 ( .A(D1_out[8]), .B(D2_out[8]), .CI(A1_add_25_carry[8]), 
        .CO(A1_add_25_carry[9]), .S(A1_out[7]) );
  FA_X1 A1_add_25_U1_9 ( .A(D1_out[8]), .B(D2_out[8]), .CI(A1_add_25_carry[9]), 
        .S(A1_out[8]) );
  AND2_X1 A2_add_25_U1 ( .A1(D4_out[0]), .A2(D3_out[0]), .ZN(
        A2_add_25_carry[1]) );
  FA_X1 A2_add_25_U1_1 ( .A(D3_out[1]), .B(D4_out[1]), .CI(A2_add_25_carry[1]), 
        .CO(A2_add_25_carry[2]), .S(A2_out[0]) );
  FA_X1 A2_add_25_U1_2 ( .A(D3_out[2]), .B(D4_out[2]), .CI(A2_add_25_carry[2]), 
        .CO(A2_add_25_carry[3]), .S(A2_out[1]) );
  FA_X1 A2_add_25_U1_3 ( .A(D3_out[3]), .B(D4_out[3]), .CI(A2_add_25_carry[3]), 
        .CO(A2_add_25_carry[4]), .S(A2_out[2]) );
  FA_X1 A2_add_25_U1_4 ( .A(D3_out[4]), .B(D4_out[4]), .CI(A2_add_25_carry[4]), 
        .CO(A2_add_25_carry[5]), .S(A2_out[3]) );
  FA_X1 A2_add_25_U1_5 ( .A(D3_out[5]), .B(D4_out[5]), .CI(A2_add_25_carry[5]), 
        .CO(A2_add_25_carry[6]), .S(A2_out[4]) );
  FA_X1 A2_add_25_U1_6 ( .A(D3_out[6]), .B(D4_out[6]), .CI(A2_add_25_carry[6]), 
        .CO(A2_add_25_carry[7]), .S(A2_out[5]) );
  FA_X1 A2_add_25_U1_7 ( .A(D3_out[7]), .B(D4_out[7]), .CI(A2_add_25_carry[7]), 
        .CO(A2_add_25_carry[8]), .S(A2_out[6]) );
  FA_X1 A2_add_25_U1_8 ( .A(D3_out[8]), .B(D4_out[8]), .CI(A2_add_25_carry[8]), 
        .CO(A2_add_25_carry[9]), .S(A2_out[7]) );
  FA_X1 A2_add_25_U1_9 ( .A(D3_out[8]), .B(D4_out[8]), .CI(A2_add_25_carry[9]), 
        .S(A2_out[8]) );
  AND2_X1 A3_add_25_U1 ( .A1(D6_out[0]), .A2(D5_out[0]), .ZN(
        A3_add_25_carry[1]) );
  FA_X1 A3_add_25_U1_1 ( .A(D5_out[1]), .B(D6_out[1]), .CI(A3_add_25_carry[1]), 
        .CO(A3_add_25_carry[2]), .S(A3_out[0]) );
  FA_X1 A3_add_25_U1_2 ( .A(D5_out[2]), .B(D6_out[2]), .CI(A3_add_25_carry[2]), 
        .CO(A3_add_25_carry[3]), .S(A3_out[1]) );
  FA_X1 A3_add_25_U1_3 ( .A(D5_out[3]), .B(D6_out[3]), .CI(A3_add_25_carry[3]), 
        .CO(A3_add_25_carry[4]), .S(A3_out[2]) );
  FA_X1 A3_add_25_U1_4 ( .A(D5_out[4]), .B(D6_out[4]), .CI(A3_add_25_carry[4]), 
        .CO(A3_add_25_carry[5]), .S(A3_out[3]) );
  FA_X1 A3_add_25_U1_5 ( .A(D5_out[5]), .B(D6_out[5]), .CI(A3_add_25_carry[5]), 
        .CO(A3_add_25_carry[6]), .S(A3_out[4]) );
  FA_X1 A3_add_25_U1_6 ( .A(D5_out[6]), .B(D6_out[6]), .CI(A3_add_25_carry[6]), 
        .CO(A3_add_25_carry[7]), .S(A3_out[5]) );
  FA_X1 A3_add_25_U1_7 ( .A(D5_out[7]), .B(D6_out[7]), .CI(A3_add_25_carry[7]), 
        .CO(A3_add_25_carry[8]), .S(A3_out[6]) );
  FA_X1 A3_add_25_U1_8 ( .A(D5_out[8]), .B(D6_out[8]), .CI(A3_add_25_carry[8]), 
        .CO(A3_add_25_carry[9]), .S(A3_out[7]) );
  FA_X1 A3_add_25_U1_9 ( .A(D5_out[8]), .B(D6_out[8]), .CI(A3_add_25_carry[9]), 
        .S(A3_out[8]) );
  XOR2_X1 M1_mult_28_U351 ( .A(D1_out[2]), .B(D1_out[1]), .Z(M1_mult_28_n360)
         );
  NAND2_X1 M1_mult_28_U350 ( .A1(D1_out[1]), .A2(M1_mult_28_n289), .ZN(
        M1_mult_28_n316) );
  XNOR2_X1 M1_mult_28_U349 ( .A(coeff_a[2]), .B(D1_out[1]), .ZN(
        M1_mult_28_n315) );
  OAI22_X1 M1_mult_28_U348 ( .A1(coeff_a[1]), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n315), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n362) );
  XNOR2_X1 M1_mult_28_U347 ( .A(M1_mult_28_n286), .B(D1_out[2]), .ZN(
        M1_mult_28_n361) );
  NAND2_X1 M1_mult_28_U346 ( .A1(M1_mult_28_n287), .A2(M1_mult_28_n361), .ZN(
        M1_mult_28_n309) );
  NAND3_X1 M1_mult_28_U345 ( .A1(M1_mult_28_n360), .A2(M1_mult_28_n291), .A3(
        D1_out[3]), .ZN(M1_mult_28_n359) );
  OAI21_X1 M1_mult_28_U344 ( .B1(M1_mult_28_n286), .B2(M1_mult_28_n309), .A(
        M1_mult_28_n359), .ZN(M1_mult_28_n358) );
  AOI222_X1 M1_mult_28_U343 ( .A1(M1_mult_28_n268), .A2(M1_mult_28_n79), .B1(
        M1_mult_28_n358), .B2(M1_mult_28_n268), .C1(M1_mult_28_n358), .C2(
        M1_mult_28_n79), .ZN(M1_mult_28_n357) );
  AOI222_X1 M1_mult_28_U342 ( .A1(M1_mult_28_n283), .A2(M1_mult_28_n77), .B1(
        M1_mult_28_n283), .B2(M1_mult_28_n78), .C1(M1_mult_28_n78), .C2(
        M1_mult_28_n77), .ZN(M1_mult_28_n356) );
  AOI222_X1 M1_mult_28_U341 ( .A1(M1_mult_28_n282), .A2(M1_mult_28_n73), .B1(
        M1_mult_28_n282), .B2(M1_mult_28_n76), .C1(M1_mult_28_n76), .C2(
        M1_mult_28_n73), .ZN(M1_mult_28_n355) );
  AOI222_X1 M1_mult_28_U340 ( .A1(M1_mult_28_n278), .A2(M1_mult_28_n69), .B1(
        M1_mult_28_n278), .B2(M1_mult_28_n72), .C1(M1_mult_28_n72), .C2(
        M1_mult_28_n69), .ZN(M1_mult_28_n354) );
  AOI222_X1 M1_mult_28_U339 ( .A1(M1_mult_28_n277), .A2(M1_mult_28_n63), .B1(
        M1_mult_28_n277), .B2(M1_mult_28_n68), .C1(M1_mult_28_n68), .C2(
        M1_mult_28_n63), .ZN(M1_mult_28_n353) );
  XOR2_X1 M1_mult_28_U338 ( .A(D1_out[8]), .B(M1_mult_28_n276), .Z(
        M1_mult_28_n295) );
  XNOR2_X1 M1_mult_28_U337 ( .A(coeff_a[6]), .B(D1_out[8]), .ZN(
        M1_mult_28_n352) );
  NOR2_X1 M1_mult_28_U336 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n352), .ZN(
        M1_mult_28_n100) );
  XNOR2_X1 M1_mult_28_U335 ( .A(coeff_a[5]), .B(D1_out[8]), .ZN(
        M1_mult_28_n351) );
  NOR2_X1 M1_mult_28_U334 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n351), .ZN(
        M1_mult_28_n101) );
  XNOR2_X1 M1_mult_28_U333 ( .A(coeff_a[4]), .B(D1_out[8]), .ZN(
        M1_mult_28_n350) );
  NOR2_X1 M1_mult_28_U332 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n350), .ZN(
        M1_mult_28_n102) );
  XNOR2_X1 M1_mult_28_U331 ( .A(coeff_a[3]), .B(D1_out[8]), .ZN(
        M1_mult_28_n349) );
  NOR2_X1 M1_mult_28_U330 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n349), .ZN(
        M1_mult_28_n103) );
  XNOR2_X1 M1_mult_28_U329 ( .A(coeff_a[2]), .B(D1_out[8]), .ZN(
        M1_mult_28_n348) );
  NOR2_X1 M1_mult_28_U328 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n348), .ZN(
        M1_mult_28_n104) );
  NOR2_X1 M1_mult_28_U327 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n291), .ZN(
        M1_mult_28_n106) );
  XNOR2_X1 M1_mult_28_U326 ( .A(coeff_a[8]), .B(D1_out[7]), .ZN(
        M1_mult_28_n314) );
  XNOR2_X1 M1_mult_28_U325 ( .A(M1_mult_28_n276), .B(D1_out[6]), .ZN(
        M1_mult_28_n347) );
  NAND2_X1 M1_mult_28_U324 ( .A1(M1_mult_28_n302), .A2(M1_mult_28_n347), .ZN(
        M1_mult_28_n300) );
  OAI22_X1 M1_mult_28_U323 ( .A1(M1_mult_28_n314), .A2(M1_mult_28_n302), .B1(
        M1_mult_28_n300), .B2(M1_mult_28_n314), .ZN(M1_mult_28_n346) );
  XNOR2_X1 M1_mult_28_U322 ( .A(coeff_a[6]), .B(D1_out[7]), .ZN(
        M1_mult_28_n345) );
  XNOR2_X1 M1_mult_28_U321 ( .A(coeff_a[7]), .B(D1_out[7]), .ZN(
        M1_mult_28_n313) );
  OAI22_X1 M1_mult_28_U320 ( .A1(M1_mult_28_n345), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n313), .ZN(M1_mult_28_n108) );
  XNOR2_X1 M1_mult_28_U319 ( .A(coeff_a[5]), .B(D1_out[7]), .ZN(
        M1_mult_28_n344) );
  OAI22_X1 M1_mult_28_U318 ( .A1(M1_mult_28_n344), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n345), .ZN(M1_mult_28_n109) );
  XNOR2_X1 M1_mult_28_U317 ( .A(coeff_a[4]), .B(D1_out[7]), .ZN(
        M1_mult_28_n343) );
  OAI22_X1 M1_mult_28_U316 ( .A1(M1_mult_28_n343), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n344), .ZN(M1_mult_28_n110) );
  XNOR2_X1 M1_mult_28_U315 ( .A(coeff_a[3]), .B(D1_out[7]), .ZN(
        M1_mult_28_n307) );
  OAI22_X1 M1_mult_28_U314 ( .A1(M1_mult_28_n307), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n343), .ZN(M1_mult_28_n111) );
  XNOR2_X1 M1_mult_28_U313 ( .A(coeff_a[1]), .B(D1_out[7]), .ZN(
        M1_mult_28_n342) );
  XNOR2_X1 M1_mult_28_U312 ( .A(coeff_a[2]), .B(D1_out[7]), .ZN(
        M1_mult_28_n306) );
  OAI22_X1 M1_mult_28_U311 ( .A1(M1_mult_28_n342), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n306), .ZN(M1_mult_28_n113) );
  XNOR2_X1 M1_mult_28_U310 ( .A(coeff_a[0]), .B(D1_out[7]), .ZN(
        M1_mult_28_n341) );
  OAI22_X1 M1_mult_28_U309 ( .A1(M1_mult_28_n341), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n342), .ZN(M1_mult_28_n114) );
  NOR2_X1 M1_mult_28_U308 ( .A1(M1_mult_28_n302), .A2(M1_mult_28_n291), .ZN(
        M1_mult_28_n115) );
  XNOR2_X1 M1_mult_28_U307 ( .A(coeff_a[8]), .B(D1_out[5]), .ZN(
        M1_mult_28_n312) );
  XNOR2_X1 M1_mult_28_U306 ( .A(M1_mult_28_n281), .B(D1_out[4]), .ZN(
        M1_mult_28_n340) );
  NAND2_X1 M1_mult_28_U305 ( .A1(M1_mult_28_n299), .A2(M1_mult_28_n340), .ZN(
        M1_mult_28_n297) );
  OAI22_X1 M1_mult_28_U304 ( .A1(M1_mult_28_n312), .A2(M1_mult_28_n299), .B1(
        M1_mult_28_n297), .B2(M1_mult_28_n312), .ZN(M1_mult_28_n339) );
  XNOR2_X1 M1_mult_28_U303 ( .A(coeff_a[6]), .B(D1_out[5]), .ZN(
        M1_mult_28_n338) );
  XNOR2_X1 M1_mult_28_U302 ( .A(coeff_a[7]), .B(D1_out[5]), .ZN(
        M1_mult_28_n311) );
  OAI22_X1 M1_mult_28_U301 ( .A1(M1_mult_28_n338), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n311), .ZN(M1_mult_28_n117) );
  XNOR2_X1 M1_mult_28_U300 ( .A(coeff_a[5]), .B(D1_out[5]), .ZN(
        M1_mult_28_n337) );
  OAI22_X1 M1_mult_28_U299 ( .A1(M1_mult_28_n337), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n338), .ZN(M1_mult_28_n118) );
  XNOR2_X1 M1_mult_28_U298 ( .A(coeff_a[4]), .B(D1_out[5]), .ZN(
        M1_mult_28_n336) );
  OAI22_X1 M1_mult_28_U297 ( .A1(M1_mult_28_n336), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n337), .ZN(M1_mult_28_n119) );
  XNOR2_X1 M1_mult_28_U296 ( .A(coeff_a[3]), .B(D1_out[5]), .ZN(
        M1_mult_28_n335) );
  OAI22_X1 M1_mult_28_U295 ( .A1(M1_mult_28_n335), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n336), .ZN(M1_mult_28_n120) );
  XNOR2_X1 M1_mult_28_U294 ( .A(coeff_a[2]), .B(D1_out[5]), .ZN(
        M1_mult_28_n334) );
  OAI22_X1 M1_mult_28_U293 ( .A1(M1_mult_28_n334), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n335), .ZN(M1_mult_28_n121) );
  XNOR2_X1 M1_mult_28_U292 ( .A(coeff_a[1]), .B(D1_out[5]), .ZN(
        M1_mult_28_n333) );
  OAI22_X1 M1_mult_28_U291 ( .A1(M1_mult_28_n333), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n334), .ZN(M1_mult_28_n122) );
  XNOR2_X1 M1_mult_28_U290 ( .A(coeff_a[0]), .B(D1_out[5]), .ZN(
        M1_mult_28_n332) );
  OAI22_X1 M1_mult_28_U289 ( .A1(M1_mult_28_n332), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n333), .ZN(M1_mult_28_n123) );
  NOR2_X1 M1_mult_28_U288 ( .A1(M1_mult_28_n299), .A2(M1_mult_28_n291), .ZN(
        M1_mult_28_n124) );
  XOR2_X1 M1_mult_28_U287 ( .A(coeff_a[8]), .B(M1_mult_28_n286), .Z(
        M1_mult_28_n310) );
  OAI22_X1 M1_mult_28_U286 ( .A1(M1_mult_28_n310), .A2(M1_mult_28_n287), .B1(
        M1_mult_28_n309), .B2(M1_mult_28_n310), .ZN(M1_mult_28_n331) );
  XNOR2_X1 M1_mult_28_U285 ( .A(coeff_a[6]), .B(D1_out[3]), .ZN(
        M1_mult_28_n330) );
  XNOR2_X1 M1_mult_28_U284 ( .A(coeff_a[7]), .B(D1_out[3]), .ZN(
        M1_mult_28_n308) );
  OAI22_X1 M1_mult_28_U283 ( .A1(M1_mult_28_n330), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n308), .ZN(M1_mult_28_n126) );
  XNOR2_X1 M1_mult_28_U282 ( .A(coeff_a[5]), .B(D1_out[3]), .ZN(
        M1_mult_28_n329) );
  OAI22_X1 M1_mult_28_U281 ( .A1(M1_mult_28_n329), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n330), .ZN(M1_mult_28_n127) );
  XNOR2_X1 M1_mult_28_U280 ( .A(coeff_a[4]), .B(D1_out[3]), .ZN(
        M1_mult_28_n328) );
  OAI22_X1 M1_mult_28_U279 ( .A1(M1_mult_28_n328), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n329), .ZN(M1_mult_28_n128) );
  XNOR2_X1 M1_mult_28_U278 ( .A(coeff_a[3]), .B(D1_out[3]), .ZN(
        M1_mult_28_n327) );
  OAI22_X1 M1_mult_28_U277 ( .A1(M1_mult_28_n327), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n328), .ZN(M1_mult_28_n129) );
  XNOR2_X1 M1_mult_28_U276 ( .A(coeff_a[2]), .B(D1_out[3]), .ZN(
        M1_mult_28_n326) );
  OAI22_X1 M1_mult_28_U275 ( .A1(M1_mult_28_n326), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n327), .ZN(M1_mult_28_n130) );
  XNOR2_X1 M1_mult_28_U274 ( .A(coeff_a[1]), .B(D1_out[3]), .ZN(
        M1_mult_28_n325) );
  OAI22_X1 M1_mult_28_U273 ( .A1(M1_mult_28_n325), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n326), .ZN(M1_mult_28_n131) );
  XNOR2_X1 M1_mult_28_U272 ( .A(coeff_a[0]), .B(D1_out[3]), .ZN(
        M1_mult_28_n324) );
  OAI22_X1 M1_mult_28_U271 ( .A1(M1_mult_28_n324), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n325), .ZN(M1_mult_28_n132) );
  XNOR2_X1 M1_mult_28_U270 ( .A(coeff_a[8]), .B(D1_out[1]), .ZN(
        M1_mult_28_n322) );
  OAI22_X1 M1_mult_28_U269 ( .A1(M1_mult_28_n289), .A2(M1_mult_28_n322), .B1(
        M1_mult_28_n316), .B2(M1_mult_28_n322), .ZN(M1_mult_28_n323) );
  XNOR2_X1 M1_mult_28_U268 ( .A(coeff_a[7]), .B(D1_out[1]), .ZN(
        M1_mult_28_n321) );
  OAI22_X1 M1_mult_28_U267 ( .A1(M1_mult_28_n321), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n322), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n135) );
  XNOR2_X1 M1_mult_28_U266 ( .A(coeff_a[6]), .B(D1_out[1]), .ZN(
        M1_mult_28_n320) );
  OAI22_X1 M1_mult_28_U265 ( .A1(M1_mult_28_n320), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n321), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n136) );
  XNOR2_X1 M1_mult_28_U264 ( .A(coeff_a[5]), .B(D1_out[1]), .ZN(
        M1_mult_28_n319) );
  OAI22_X1 M1_mult_28_U263 ( .A1(M1_mult_28_n319), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n320), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n137) );
  XNOR2_X1 M1_mult_28_U262 ( .A(coeff_a[4]), .B(D1_out[1]), .ZN(
        M1_mult_28_n318) );
  OAI22_X1 M1_mult_28_U261 ( .A1(M1_mult_28_n318), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n319), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n138) );
  XNOR2_X1 M1_mult_28_U260 ( .A(coeff_a[3]), .B(D1_out[1]), .ZN(
        M1_mult_28_n317) );
  OAI22_X1 M1_mult_28_U259 ( .A1(M1_mult_28_n317), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n318), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n139) );
  OAI22_X1 M1_mult_28_U258 ( .A1(M1_mult_28_n315), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n317), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n140) );
  OAI22_X1 M1_mult_28_U257 ( .A1(M1_mult_28_n313), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n314), .ZN(M1_mult_28_n22) );
  OAI22_X1 M1_mult_28_U256 ( .A1(M1_mult_28_n311), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n312), .ZN(M1_mult_28_n32) );
  OAI22_X1 M1_mult_28_U255 ( .A1(M1_mult_28_n308), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n310), .ZN(M1_mult_28_n46) );
  OAI22_X1 M1_mult_28_U254 ( .A1(M1_mult_28_n306), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n307), .ZN(M1_mult_28_n305) );
  XNOR2_X1 M1_mult_28_U253 ( .A(M1_mult_28_n290), .B(D1_out[8]), .ZN(
        M1_mult_28_n304) );
  NAND2_X1 M1_mult_28_U252 ( .A1(M1_mult_28_n304), .A2(M1_mult_28_n271), .ZN(
        M1_mult_28_n303) );
  NAND2_X1 M1_mult_28_U251 ( .A1(M1_mult_28_n273), .A2(M1_mult_28_n303), .ZN(
        M1_mult_28_n54) );
  XNOR2_X1 M1_mult_28_U250 ( .A(M1_mult_28_n303), .B(M1_mult_28_n273), .ZN(
        M1_mult_28_n55) );
  AND3_X1 M1_mult_28_U249 ( .A1(D1_out[8]), .A2(M1_mult_28_n291), .A3(
        M1_mult_28_n271), .ZN(M1_mult_28_n93) );
  OR3_X1 M1_mult_28_U248 ( .A1(M1_mult_28_n302), .A2(coeff_a[0]), .A3(
        M1_mult_28_n276), .ZN(M1_mult_28_n301) );
  OAI21_X1 M1_mult_28_U247 ( .B1(M1_mult_28_n276), .B2(M1_mult_28_n300), .A(
        M1_mult_28_n301), .ZN(M1_mult_28_n94) );
  OR3_X1 M1_mult_28_U246 ( .A1(M1_mult_28_n299), .A2(coeff_a[0]), .A3(
        M1_mult_28_n281), .ZN(M1_mult_28_n298) );
  OAI21_X1 M1_mult_28_U245 ( .B1(M1_mult_28_n281), .B2(M1_mult_28_n297), .A(
        M1_mult_28_n298), .ZN(M1_mult_28_n95) );
  XNOR2_X1 M1_mult_28_U244 ( .A(coeff_a[7]), .B(D1_out[8]), .ZN(
        M1_mult_28_n296) );
  NOR2_X1 M1_mult_28_U243 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n296), .ZN(
        M1_mult_28_n99) );
  XOR2_X1 M1_mult_28_U242 ( .A(coeff_a[8]), .B(D1_out[8]), .Z(M1_mult_28_n294)
         );
  NAND2_X1 M1_mult_28_U241 ( .A1(M1_mult_28_n294), .A2(M1_mult_28_n271), .ZN(
        M1_mult_28_n292) );
  XOR2_X1 M1_mult_28_U240 ( .A(M1_mult_28_n2), .B(M1_mult_28_n18), .Z(
        M1_mult_28_n293) );
  XOR2_X1 M1_mult_28_U239 ( .A(M1_mult_28_n292), .B(M1_mult_28_n293), .Z(
        M1_out[8]) );
  INV_X1 M1_mult_28_U238 ( .A(M1_mult_28_n346), .ZN(M1_mult_28_n275) );
  INV_X1 M1_mult_28_U237 ( .A(M1_mult_28_n22), .ZN(M1_mult_28_n274) );
  INV_X1 M1_mult_28_U236 ( .A(M1_mult_28_n323), .ZN(M1_mult_28_n288) );
  INV_X1 M1_mult_28_U235 ( .A(D1_out[7]), .ZN(M1_mult_28_n276) );
  INV_X1 M1_mult_28_U234 ( .A(coeff_a[1]), .ZN(M1_mult_28_n290) );
  AND3_X1 M1_mult_28_U233 ( .A1(M1_mult_28_n362), .A2(M1_mult_28_n290), .A3(
        D1_out[1]), .ZN(M1_mult_28_n270) );
  AND2_X1 M1_mult_28_U232 ( .A1(M1_mult_28_n360), .A2(M1_mult_28_n362), .ZN(
        M1_mult_28_n269) );
  MUX2_X1 M1_mult_28_U231 ( .A(M1_mult_28_n269), .B(M1_mult_28_n270), .S(
        M1_mult_28_n291), .Z(M1_mult_28_n268) );
  INV_X1 M1_mult_28_U230 ( .A(coeff_a[0]), .ZN(M1_mult_28_n291) );
  INV_X1 M1_mult_28_U229 ( .A(D1_out[5]), .ZN(M1_mult_28_n281) );
  INV_X1 M1_mult_28_U228 ( .A(D1_out[3]), .ZN(M1_mult_28_n286) );
  INV_X1 M1_mult_28_U227 ( .A(D1_out[0]), .ZN(M1_mult_28_n289) );
  XOR2_X1 M1_mult_28_U226 ( .A(D1_out[6]), .B(M1_mult_28_n281), .Z(
        M1_mult_28_n302) );
  XOR2_X1 M1_mult_28_U225 ( .A(D1_out[4]), .B(M1_mult_28_n286), .Z(
        M1_mult_28_n299) );
  INV_X1 M1_mult_28_U224 ( .A(M1_mult_28_n46), .ZN(M1_mult_28_n284) );
  INV_X1 M1_mult_28_U223 ( .A(M1_mult_28_n339), .ZN(M1_mult_28_n280) );
  INV_X1 M1_mult_28_U222 ( .A(M1_mult_28_n331), .ZN(M1_mult_28_n285) );
  INV_X1 M1_mult_28_U221 ( .A(M1_mult_28_n32), .ZN(M1_mult_28_n279) );
  INV_X1 M1_mult_28_U220 ( .A(M1_mult_28_n295), .ZN(M1_mult_28_n271) );
  INV_X1 M1_mult_28_U219 ( .A(M1_mult_28_n305), .ZN(M1_mult_28_n273) );
  INV_X1 M1_mult_28_U218 ( .A(M1_mult_28_n357), .ZN(M1_mult_28_n283) );
  INV_X1 M1_mult_28_U217 ( .A(M1_mult_28_n356), .ZN(M1_mult_28_n282) );
  INV_X1 M1_mult_28_U216 ( .A(M1_mult_28_n353), .ZN(M1_mult_28_n272) );
  INV_X1 M1_mult_28_U215 ( .A(M1_mult_28_n360), .ZN(M1_mult_28_n287) );
  INV_X1 M1_mult_28_U214 ( .A(M1_mult_28_n355), .ZN(M1_mult_28_n278) );
  INV_X1 M1_mult_28_U213 ( .A(M1_mult_28_n354), .ZN(M1_mult_28_n277) );
  HA_X1 M1_mult_28_U50 ( .A(M1_mult_28_n132), .B(M1_mult_28_n140), .CO(
        M1_mult_28_n78), .S(M1_mult_28_n79) );
  FA_X1 M1_mult_28_U49 ( .A(M1_mult_28_n139), .B(M1_mult_28_n124), .CI(
        M1_mult_28_n131), .CO(M1_mult_28_n76), .S(M1_mult_28_n77) );
  HA_X1 M1_mult_28_U48 ( .A(M1_mult_28_n95), .B(M1_mult_28_n123), .CO(
        M1_mult_28_n74), .S(M1_mult_28_n75) );
  FA_X1 M1_mult_28_U47 ( .A(M1_mult_28_n130), .B(M1_mult_28_n138), .CI(
        M1_mult_28_n75), .CO(M1_mult_28_n72), .S(M1_mult_28_n73) );
  FA_X1 M1_mult_28_U46 ( .A(M1_mult_28_n137), .B(M1_mult_28_n115), .CI(
        M1_mult_28_n129), .CO(M1_mult_28_n70), .S(M1_mult_28_n71) );
  FA_X1 M1_mult_28_U45 ( .A(M1_mult_28_n74), .B(M1_mult_28_n122), .CI(
        M1_mult_28_n71), .CO(M1_mult_28_n68), .S(M1_mult_28_n69) );
  HA_X1 M1_mult_28_U44 ( .A(M1_mult_28_n94), .B(M1_mult_28_n114), .CO(
        M1_mult_28_n66), .S(M1_mult_28_n67) );
  FA_X1 M1_mult_28_U43 ( .A(M1_mult_28_n121), .B(M1_mult_28_n136), .CI(
        M1_mult_28_n128), .CO(M1_mult_28_n64), .S(M1_mult_28_n65) );
  FA_X1 M1_mult_28_U42 ( .A(M1_mult_28_n70), .B(M1_mult_28_n67), .CI(
        M1_mult_28_n65), .CO(M1_mult_28_n62), .S(M1_mult_28_n63) );
  FA_X1 M1_mult_28_U41 ( .A(M1_mult_28_n120), .B(M1_mult_28_n106), .CI(
        M1_mult_28_n135), .CO(M1_mult_28_n60), .S(M1_mult_28_n61) );
  FA_X1 M1_mult_28_U40 ( .A(M1_mult_28_n113), .B(M1_mult_28_n127), .CI(
        M1_mult_28_n66), .CO(M1_mult_28_n58), .S(M1_mult_28_n59) );
  FA_X1 M1_mult_28_U39 ( .A(M1_mult_28_n61), .B(M1_mult_28_n64), .CI(
        M1_mult_28_n59), .CO(M1_mult_28_n56), .S(M1_mult_28_n57) );
  FA_X1 M1_mult_28_U36 ( .A(M1_mult_28_n93), .B(M1_mult_28_n119), .CI(
        M1_mult_28_n288), .CO(M1_mult_28_n52), .S(M1_mult_28_n53) );
  FA_X1 M1_mult_28_U35 ( .A(M1_mult_28_n55), .B(M1_mult_28_n126), .CI(
        M1_mult_28_n60), .CO(M1_mult_28_n50), .S(M1_mult_28_n51) );
  FA_X1 M1_mult_28_U34 ( .A(M1_mult_28_n53), .B(M1_mult_28_n58), .CI(
        M1_mult_28_n51), .CO(M1_mult_28_n48), .S(M1_mult_28_n49) );
  FA_X1 M1_mult_28_U32 ( .A(M1_mult_28_n111), .B(M1_mult_28_n104), .CI(
        M1_mult_28_n118), .CO(M1_mult_28_n44), .S(M1_mult_28_n45) );
  FA_X1 M1_mult_28_U31 ( .A(M1_mult_28_n54), .B(M1_mult_28_n284), .CI(
        M1_mult_28_n52), .CO(M1_mult_28_n42), .S(M1_mult_28_n43) );
  FA_X1 M1_mult_28_U30 ( .A(M1_mult_28_n50), .B(M1_mult_28_n45), .CI(
        M1_mult_28_n43), .CO(M1_mult_28_n40), .S(M1_mult_28_n41) );
  FA_X1 M1_mult_28_U29 ( .A(M1_mult_28_n110), .B(M1_mult_28_n103), .CI(
        M1_mult_28_n285), .CO(M1_mult_28_n38), .S(M1_mult_28_n39) );
  FA_X1 M1_mult_28_U28 ( .A(M1_mult_28_n46), .B(M1_mult_28_n117), .CI(
        M1_mult_28_n44), .CO(M1_mult_28_n36), .S(M1_mult_28_n37) );
  FA_X1 M1_mult_28_U27 ( .A(M1_mult_28_n42), .B(M1_mult_28_n39), .CI(
        M1_mult_28_n37), .CO(M1_mult_28_n34), .S(M1_mult_28_n35) );
  FA_X1 M1_mult_28_U25 ( .A(M1_mult_28_n102), .B(M1_mult_28_n109), .CI(
        M1_mult_28_n279), .CO(M1_mult_28_n30), .S(M1_mult_28_n31) );
  FA_X1 M1_mult_28_U24 ( .A(M1_mult_28_n31), .B(M1_mult_28_n38), .CI(
        M1_mult_28_n36), .CO(M1_mult_28_n28), .S(M1_mult_28_n29) );
  FA_X1 M1_mult_28_U23 ( .A(M1_mult_28_n108), .B(M1_mult_28_n32), .CI(
        M1_mult_28_n280), .CO(M1_mult_28_n26), .S(M1_mult_28_n27) );
  FA_X1 M1_mult_28_U22 ( .A(M1_mult_28_n30), .B(M1_mult_28_n101), .CI(
        M1_mult_28_n27), .CO(M1_mult_28_n24), .S(M1_mult_28_n25) );
  FA_X1 M1_mult_28_U20 ( .A(M1_mult_28_n274), .B(M1_mult_28_n100), .CI(
        M1_mult_28_n26), .CO(M1_mult_28_n20), .S(M1_mult_28_n21) );
  FA_X1 M1_mult_28_U19 ( .A(M1_mult_28_n99), .B(M1_mult_28_n22), .CI(
        M1_mult_28_n275), .CO(M1_mult_28_n18), .S(M1_mult_28_n19) );
  FA_X1 M1_mult_28_U10 ( .A(M1_mult_28_n57), .B(M1_mult_28_n62), .CI(
        M1_mult_28_n272), .CO(M1_mult_28_n9), .S(M1_out[0]) );
  FA_X1 M1_mult_28_U9 ( .A(M1_mult_28_n49), .B(M1_mult_28_n56), .CI(
        M1_mult_28_n9), .CO(M1_mult_28_n8), .S(M1_out[1]) );
  FA_X1 M1_mult_28_U8 ( .A(M1_mult_28_n41), .B(M1_mult_28_n48), .CI(
        M1_mult_28_n8), .CO(M1_mult_28_n7), .S(M1_out[2]) );
  FA_X1 M1_mult_28_U7 ( .A(M1_mult_28_n35), .B(M1_mult_28_n40), .CI(
        M1_mult_28_n7), .CO(M1_mult_28_n6), .S(M1_out[3]) );
  FA_X1 M1_mult_28_U6 ( .A(M1_mult_28_n29), .B(M1_mult_28_n34), .CI(
        M1_mult_28_n6), .CO(M1_mult_28_n5), .S(M1_out[4]) );
  FA_X1 M1_mult_28_U5 ( .A(M1_mult_28_n25), .B(M1_mult_28_n28), .CI(
        M1_mult_28_n5), .CO(M1_mult_28_n4), .S(M1_out[5]) );
  FA_X1 M1_mult_28_U4 ( .A(M1_mult_28_n21), .B(M1_mult_28_n24), .CI(
        M1_mult_28_n4), .CO(M1_mult_28_n3), .S(M1_out[6]) );
  FA_X1 M1_mult_28_U3 ( .A(M1_mult_28_n20), .B(M1_mult_28_n19), .CI(
        M1_mult_28_n3), .CO(M1_mult_28_n2), .S(M1_out[7]) );
  XOR2_X1 M2_mult_28_U351 ( .A(D5_out[2]), .B(D5_out[1]), .Z(M2_mult_28_n360)
         );
  NAND2_X1 M2_mult_28_U350 ( .A1(D5_out[1]), .A2(M2_mult_28_n289), .ZN(
        M2_mult_28_n316) );
  XNOR2_X1 M2_mult_28_U349 ( .A(coeff_aa[2]), .B(D5_out[1]), .ZN(
        M2_mult_28_n315) );
  OAI22_X1 M2_mult_28_U348 ( .A1(coeff_aa[1]), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n315), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n362) );
  XNOR2_X1 M2_mult_28_U347 ( .A(M2_mult_28_n286), .B(D5_out[2]), .ZN(
        M2_mult_28_n361) );
  NAND2_X1 M2_mult_28_U346 ( .A1(M2_mult_28_n287), .A2(M2_mult_28_n361), .ZN(
        M2_mult_28_n309) );
  NAND3_X1 M2_mult_28_U345 ( .A1(M2_mult_28_n360), .A2(M2_mult_28_n291), .A3(
        D5_out[3]), .ZN(M2_mult_28_n359) );
  OAI21_X1 M2_mult_28_U344 ( .B1(M2_mult_28_n286), .B2(M2_mult_28_n309), .A(
        M2_mult_28_n359), .ZN(M2_mult_28_n358) );
  AOI222_X1 M2_mult_28_U343 ( .A1(M2_mult_28_n268), .A2(M2_mult_28_n79), .B1(
        M2_mult_28_n358), .B2(M2_mult_28_n268), .C1(M2_mult_28_n358), .C2(
        M2_mult_28_n79), .ZN(M2_mult_28_n357) );
  AOI222_X1 M2_mult_28_U342 ( .A1(M2_mult_28_n283), .A2(M2_mult_28_n77), .B1(
        M2_mult_28_n283), .B2(M2_mult_28_n78), .C1(M2_mult_28_n78), .C2(
        M2_mult_28_n77), .ZN(M2_mult_28_n356) );
  AOI222_X1 M2_mult_28_U341 ( .A1(M2_mult_28_n282), .A2(M2_mult_28_n73), .B1(
        M2_mult_28_n282), .B2(M2_mult_28_n76), .C1(M2_mult_28_n76), .C2(
        M2_mult_28_n73), .ZN(M2_mult_28_n355) );
  AOI222_X1 M2_mult_28_U340 ( .A1(M2_mult_28_n278), .A2(M2_mult_28_n69), .B1(
        M2_mult_28_n278), .B2(M2_mult_28_n72), .C1(M2_mult_28_n72), .C2(
        M2_mult_28_n69), .ZN(M2_mult_28_n354) );
  AOI222_X1 M2_mult_28_U339 ( .A1(M2_mult_28_n277), .A2(M2_mult_28_n63), .B1(
        M2_mult_28_n277), .B2(M2_mult_28_n68), .C1(M2_mult_28_n68), .C2(
        M2_mult_28_n63), .ZN(M2_mult_28_n353) );
  XOR2_X1 M2_mult_28_U338 ( .A(D5_out[8]), .B(M2_mult_28_n276), .Z(
        M2_mult_28_n295) );
  XNOR2_X1 M2_mult_28_U337 ( .A(coeff_aa[6]), .B(D5_out[8]), .ZN(
        M2_mult_28_n352) );
  NOR2_X1 M2_mult_28_U336 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n352), .ZN(
        M2_mult_28_n100) );
  XNOR2_X1 M2_mult_28_U335 ( .A(coeff_aa[5]), .B(D5_out[8]), .ZN(
        M2_mult_28_n351) );
  NOR2_X1 M2_mult_28_U334 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n351), .ZN(
        M2_mult_28_n101) );
  XNOR2_X1 M2_mult_28_U333 ( .A(coeff_aa[4]), .B(D5_out[8]), .ZN(
        M2_mult_28_n350) );
  NOR2_X1 M2_mult_28_U332 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n350), .ZN(
        M2_mult_28_n102) );
  XNOR2_X1 M2_mult_28_U331 ( .A(coeff_aa[3]), .B(D5_out[8]), .ZN(
        M2_mult_28_n349) );
  NOR2_X1 M2_mult_28_U330 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n349), .ZN(
        M2_mult_28_n103) );
  XNOR2_X1 M2_mult_28_U329 ( .A(coeff_aa[2]), .B(D5_out[8]), .ZN(
        M2_mult_28_n348) );
  NOR2_X1 M2_mult_28_U328 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n348), .ZN(
        M2_mult_28_n104) );
  NOR2_X1 M2_mult_28_U327 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n291), .ZN(
        M2_mult_28_n106) );
  XNOR2_X1 M2_mult_28_U326 ( .A(coeff_aa[8]), .B(D5_out[7]), .ZN(
        M2_mult_28_n314) );
  XNOR2_X1 M2_mult_28_U325 ( .A(M2_mult_28_n276), .B(D5_out[6]), .ZN(
        M2_mult_28_n347) );
  NAND2_X1 M2_mult_28_U324 ( .A1(M2_mult_28_n302), .A2(M2_mult_28_n347), .ZN(
        M2_mult_28_n300) );
  OAI22_X1 M2_mult_28_U323 ( .A1(M2_mult_28_n314), .A2(M2_mult_28_n302), .B1(
        M2_mult_28_n300), .B2(M2_mult_28_n314), .ZN(M2_mult_28_n346) );
  XNOR2_X1 M2_mult_28_U322 ( .A(coeff_aa[6]), .B(D5_out[7]), .ZN(
        M2_mult_28_n345) );
  XNOR2_X1 M2_mult_28_U321 ( .A(coeff_aa[7]), .B(D5_out[7]), .ZN(
        M2_mult_28_n313) );
  OAI22_X1 M2_mult_28_U320 ( .A1(M2_mult_28_n345), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n313), .ZN(M2_mult_28_n108) );
  XNOR2_X1 M2_mult_28_U319 ( .A(coeff_aa[5]), .B(D5_out[7]), .ZN(
        M2_mult_28_n344) );
  OAI22_X1 M2_mult_28_U318 ( .A1(M2_mult_28_n344), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n345), .ZN(M2_mult_28_n109) );
  XNOR2_X1 M2_mult_28_U317 ( .A(coeff_aa[4]), .B(D5_out[7]), .ZN(
        M2_mult_28_n343) );
  OAI22_X1 M2_mult_28_U316 ( .A1(M2_mult_28_n343), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n344), .ZN(M2_mult_28_n110) );
  XNOR2_X1 M2_mult_28_U315 ( .A(coeff_aa[3]), .B(D5_out[7]), .ZN(
        M2_mult_28_n307) );
  OAI22_X1 M2_mult_28_U314 ( .A1(M2_mult_28_n307), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n343), .ZN(M2_mult_28_n111) );
  XNOR2_X1 M2_mult_28_U313 ( .A(coeff_aa[1]), .B(D5_out[7]), .ZN(
        M2_mult_28_n342) );
  XNOR2_X1 M2_mult_28_U312 ( .A(coeff_aa[2]), .B(D5_out[7]), .ZN(
        M2_mult_28_n306) );
  OAI22_X1 M2_mult_28_U311 ( .A1(M2_mult_28_n342), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n306), .ZN(M2_mult_28_n113) );
  XNOR2_X1 M2_mult_28_U310 ( .A(coeff_aa[0]), .B(D5_out[7]), .ZN(
        M2_mult_28_n341) );
  OAI22_X1 M2_mult_28_U309 ( .A1(M2_mult_28_n341), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n342), .ZN(M2_mult_28_n114) );
  NOR2_X1 M2_mult_28_U308 ( .A1(M2_mult_28_n302), .A2(M2_mult_28_n291), .ZN(
        M2_mult_28_n115) );
  XNOR2_X1 M2_mult_28_U307 ( .A(coeff_aa[8]), .B(D5_out[5]), .ZN(
        M2_mult_28_n312) );
  XNOR2_X1 M2_mult_28_U306 ( .A(M2_mult_28_n281), .B(D5_out[4]), .ZN(
        M2_mult_28_n340) );
  NAND2_X1 M2_mult_28_U305 ( .A1(M2_mult_28_n299), .A2(M2_mult_28_n340), .ZN(
        M2_mult_28_n297) );
  OAI22_X1 M2_mult_28_U304 ( .A1(M2_mult_28_n312), .A2(M2_mult_28_n299), .B1(
        M2_mult_28_n297), .B2(M2_mult_28_n312), .ZN(M2_mult_28_n339) );
  XNOR2_X1 M2_mult_28_U303 ( .A(coeff_aa[6]), .B(D5_out[5]), .ZN(
        M2_mult_28_n338) );
  XNOR2_X1 M2_mult_28_U302 ( .A(coeff_aa[7]), .B(D5_out[5]), .ZN(
        M2_mult_28_n311) );
  OAI22_X1 M2_mult_28_U301 ( .A1(M2_mult_28_n338), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n311), .ZN(M2_mult_28_n117) );
  XNOR2_X1 M2_mult_28_U300 ( .A(coeff_aa[5]), .B(D5_out[5]), .ZN(
        M2_mult_28_n337) );
  OAI22_X1 M2_mult_28_U299 ( .A1(M2_mult_28_n337), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n338), .ZN(M2_mult_28_n118) );
  XNOR2_X1 M2_mult_28_U298 ( .A(coeff_aa[4]), .B(D5_out[5]), .ZN(
        M2_mult_28_n336) );
  OAI22_X1 M2_mult_28_U297 ( .A1(M2_mult_28_n336), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n337), .ZN(M2_mult_28_n119) );
  XNOR2_X1 M2_mult_28_U296 ( .A(coeff_aa[3]), .B(D5_out[5]), .ZN(
        M2_mult_28_n335) );
  OAI22_X1 M2_mult_28_U295 ( .A1(M2_mult_28_n335), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n336), .ZN(M2_mult_28_n120) );
  XNOR2_X1 M2_mult_28_U294 ( .A(coeff_aa[2]), .B(D5_out[5]), .ZN(
        M2_mult_28_n334) );
  OAI22_X1 M2_mult_28_U293 ( .A1(M2_mult_28_n334), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n335), .ZN(M2_mult_28_n121) );
  XNOR2_X1 M2_mult_28_U292 ( .A(coeff_aa[1]), .B(D5_out[5]), .ZN(
        M2_mult_28_n333) );
  OAI22_X1 M2_mult_28_U291 ( .A1(M2_mult_28_n333), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n334), .ZN(M2_mult_28_n122) );
  XNOR2_X1 M2_mult_28_U290 ( .A(coeff_aa[0]), .B(D5_out[5]), .ZN(
        M2_mult_28_n332) );
  OAI22_X1 M2_mult_28_U289 ( .A1(M2_mult_28_n332), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n333), .ZN(M2_mult_28_n123) );
  NOR2_X1 M2_mult_28_U288 ( .A1(M2_mult_28_n299), .A2(M2_mult_28_n291), .ZN(
        M2_mult_28_n124) );
  XOR2_X1 M2_mult_28_U287 ( .A(coeff_aa[8]), .B(M2_mult_28_n286), .Z(
        M2_mult_28_n310) );
  OAI22_X1 M2_mult_28_U286 ( .A1(M2_mult_28_n310), .A2(M2_mult_28_n287), .B1(
        M2_mult_28_n309), .B2(M2_mult_28_n310), .ZN(M2_mult_28_n331) );
  XNOR2_X1 M2_mult_28_U285 ( .A(coeff_aa[6]), .B(D5_out[3]), .ZN(
        M2_mult_28_n330) );
  XNOR2_X1 M2_mult_28_U284 ( .A(coeff_aa[7]), .B(D5_out[3]), .ZN(
        M2_mult_28_n308) );
  OAI22_X1 M2_mult_28_U283 ( .A1(M2_mult_28_n330), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n308), .ZN(M2_mult_28_n126) );
  XNOR2_X1 M2_mult_28_U282 ( .A(coeff_aa[5]), .B(D5_out[3]), .ZN(
        M2_mult_28_n329) );
  OAI22_X1 M2_mult_28_U281 ( .A1(M2_mult_28_n329), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n330), .ZN(M2_mult_28_n127) );
  XNOR2_X1 M2_mult_28_U280 ( .A(coeff_aa[4]), .B(D5_out[3]), .ZN(
        M2_mult_28_n328) );
  OAI22_X1 M2_mult_28_U279 ( .A1(M2_mult_28_n328), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n329), .ZN(M2_mult_28_n128) );
  XNOR2_X1 M2_mult_28_U278 ( .A(coeff_aa[3]), .B(D5_out[3]), .ZN(
        M2_mult_28_n327) );
  OAI22_X1 M2_mult_28_U277 ( .A1(M2_mult_28_n327), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n328), .ZN(M2_mult_28_n129) );
  XNOR2_X1 M2_mult_28_U276 ( .A(coeff_aa[2]), .B(D5_out[3]), .ZN(
        M2_mult_28_n326) );
  OAI22_X1 M2_mult_28_U275 ( .A1(M2_mult_28_n326), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n327), .ZN(M2_mult_28_n130) );
  XNOR2_X1 M2_mult_28_U274 ( .A(coeff_aa[1]), .B(D5_out[3]), .ZN(
        M2_mult_28_n325) );
  OAI22_X1 M2_mult_28_U273 ( .A1(M2_mult_28_n325), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n326), .ZN(M2_mult_28_n131) );
  XNOR2_X1 M2_mult_28_U272 ( .A(coeff_aa[0]), .B(D5_out[3]), .ZN(
        M2_mult_28_n324) );
  OAI22_X1 M2_mult_28_U271 ( .A1(M2_mult_28_n324), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n325), .ZN(M2_mult_28_n132) );
  XNOR2_X1 M2_mult_28_U270 ( .A(coeff_aa[8]), .B(D5_out[1]), .ZN(
        M2_mult_28_n322) );
  OAI22_X1 M2_mult_28_U269 ( .A1(M2_mult_28_n289), .A2(M2_mult_28_n322), .B1(
        M2_mult_28_n316), .B2(M2_mult_28_n322), .ZN(M2_mult_28_n323) );
  XNOR2_X1 M2_mult_28_U268 ( .A(coeff_aa[7]), .B(D5_out[1]), .ZN(
        M2_mult_28_n321) );
  OAI22_X1 M2_mult_28_U267 ( .A1(M2_mult_28_n321), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n322), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n135) );
  XNOR2_X1 M2_mult_28_U266 ( .A(coeff_aa[6]), .B(D5_out[1]), .ZN(
        M2_mult_28_n320) );
  OAI22_X1 M2_mult_28_U265 ( .A1(M2_mult_28_n320), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n321), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n136) );
  XNOR2_X1 M2_mult_28_U264 ( .A(coeff_aa[5]), .B(D5_out[1]), .ZN(
        M2_mult_28_n319) );
  OAI22_X1 M2_mult_28_U263 ( .A1(M2_mult_28_n319), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n320), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n137) );
  XNOR2_X1 M2_mult_28_U262 ( .A(coeff_aa[4]), .B(D5_out[1]), .ZN(
        M2_mult_28_n318) );
  OAI22_X1 M2_mult_28_U261 ( .A1(M2_mult_28_n318), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n319), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n138) );
  XNOR2_X1 M2_mult_28_U260 ( .A(coeff_aa[3]), .B(D5_out[1]), .ZN(
        M2_mult_28_n317) );
  OAI22_X1 M2_mult_28_U259 ( .A1(M2_mult_28_n317), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n318), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n139) );
  OAI22_X1 M2_mult_28_U258 ( .A1(M2_mult_28_n315), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n317), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n140) );
  OAI22_X1 M2_mult_28_U257 ( .A1(M2_mult_28_n313), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n314), .ZN(M2_mult_28_n22) );
  OAI22_X1 M2_mult_28_U256 ( .A1(M2_mult_28_n311), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n312), .ZN(M2_mult_28_n32) );
  OAI22_X1 M2_mult_28_U255 ( .A1(M2_mult_28_n308), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n310), .ZN(M2_mult_28_n46) );
  OAI22_X1 M2_mult_28_U254 ( .A1(M2_mult_28_n306), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n307), .ZN(M2_mult_28_n305) );
  XNOR2_X1 M2_mult_28_U253 ( .A(M2_mult_28_n290), .B(D5_out[8]), .ZN(
        M2_mult_28_n304) );
  NAND2_X1 M2_mult_28_U252 ( .A1(M2_mult_28_n304), .A2(M2_mult_28_n271), .ZN(
        M2_mult_28_n303) );
  NAND2_X1 M2_mult_28_U251 ( .A1(M2_mult_28_n273), .A2(M2_mult_28_n303), .ZN(
        M2_mult_28_n54) );
  XNOR2_X1 M2_mult_28_U250 ( .A(M2_mult_28_n303), .B(M2_mult_28_n273), .ZN(
        M2_mult_28_n55) );
  AND3_X1 M2_mult_28_U249 ( .A1(D5_out[8]), .A2(M2_mult_28_n291), .A3(
        M2_mult_28_n271), .ZN(M2_mult_28_n93) );
  OR3_X1 M2_mult_28_U248 ( .A1(M2_mult_28_n302), .A2(coeff_aa[0]), .A3(
        M2_mult_28_n276), .ZN(M2_mult_28_n301) );
  OAI21_X1 M2_mult_28_U247 ( .B1(M2_mult_28_n276), .B2(M2_mult_28_n300), .A(
        M2_mult_28_n301), .ZN(M2_mult_28_n94) );
  OR3_X1 M2_mult_28_U246 ( .A1(M2_mult_28_n299), .A2(coeff_aa[0]), .A3(
        M2_mult_28_n281), .ZN(M2_mult_28_n298) );
  OAI21_X1 M2_mult_28_U245 ( .B1(M2_mult_28_n281), .B2(M2_mult_28_n297), .A(
        M2_mult_28_n298), .ZN(M2_mult_28_n95) );
  XNOR2_X1 M2_mult_28_U244 ( .A(coeff_aa[7]), .B(D5_out[8]), .ZN(
        M2_mult_28_n296) );
  NOR2_X1 M2_mult_28_U243 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n296), .ZN(
        M2_mult_28_n99) );
  XOR2_X1 M2_mult_28_U242 ( .A(coeff_aa[8]), .B(D5_out[8]), .Z(M2_mult_28_n294) );
  NAND2_X1 M2_mult_28_U241 ( .A1(M2_mult_28_n294), .A2(M2_mult_28_n271), .ZN(
        M2_mult_28_n292) );
  XOR2_X1 M2_mult_28_U240 ( .A(M2_mult_28_n2), .B(M2_mult_28_n18), .Z(
        M2_mult_28_n293) );
  XOR2_X1 M2_mult_28_U239 ( .A(M2_mult_28_n292), .B(M2_mult_28_n293), .Z(
        M2_out[8]) );
  INV_X1 M2_mult_28_U238 ( .A(M2_mult_28_n323), .ZN(M2_mult_28_n288) );
  INV_X1 M2_mult_28_U237 ( .A(M2_mult_28_n346), .ZN(M2_mult_28_n275) );
  INV_X1 M2_mult_28_U236 ( .A(M2_mult_28_n22), .ZN(M2_mult_28_n274) );
  INV_X1 M2_mult_28_U235 ( .A(D5_out[7]), .ZN(M2_mult_28_n276) );
  INV_X1 M2_mult_28_U234 ( .A(coeff_aa[1]), .ZN(M2_mult_28_n290) );
  AND3_X1 M2_mult_28_U233 ( .A1(M2_mult_28_n362), .A2(M2_mult_28_n290), .A3(
        D5_out[1]), .ZN(M2_mult_28_n270) );
  AND2_X1 M2_mult_28_U232 ( .A1(M2_mult_28_n360), .A2(M2_mult_28_n362), .ZN(
        M2_mult_28_n269) );
  MUX2_X1 M2_mult_28_U231 ( .A(M2_mult_28_n269), .B(M2_mult_28_n270), .S(
        M2_mult_28_n291), .Z(M2_mult_28_n268) );
  INV_X1 M2_mult_28_U230 ( .A(coeff_aa[0]), .ZN(M2_mult_28_n291) );
  INV_X1 M2_mult_28_U229 ( .A(D5_out[5]), .ZN(M2_mult_28_n281) );
  INV_X1 M2_mult_28_U228 ( .A(D5_out[3]), .ZN(M2_mult_28_n286) );
  INV_X1 M2_mult_28_U227 ( .A(D5_out[0]), .ZN(M2_mult_28_n289) );
  XOR2_X1 M2_mult_28_U226 ( .A(D5_out[6]), .B(M2_mult_28_n281), .Z(
        M2_mult_28_n302) );
  XOR2_X1 M2_mult_28_U225 ( .A(D5_out[4]), .B(M2_mult_28_n286), .Z(
        M2_mult_28_n299) );
  INV_X1 M2_mult_28_U224 ( .A(M2_mult_28_n46), .ZN(M2_mult_28_n284) );
  INV_X1 M2_mult_28_U223 ( .A(M2_mult_28_n339), .ZN(M2_mult_28_n280) );
  INV_X1 M2_mult_28_U222 ( .A(M2_mult_28_n331), .ZN(M2_mult_28_n285) );
  INV_X1 M2_mult_28_U221 ( .A(M2_mult_28_n32), .ZN(M2_mult_28_n279) );
  INV_X1 M2_mult_28_U220 ( .A(M2_mult_28_n295), .ZN(M2_mult_28_n271) );
  INV_X1 M2_mult_28_U219 ( .A(M2_mult_28_n305), .ZN(M2_mult_28_n273) );
  INV_X1 M2_mult_28_U218 ( .A(M2_mult_28_n357), .ZN(M2_mult_28_n283) );
  INV_X1 M2_mult_28_U217 ( .A(M2_mult_28_n356), .ZN(M2_mult_28_n282) );
  INV_X1 M2_mult_28_U216 ( .A(M2_mult_28_n353), .ZN(M2_mult_28_n272) );
  INV_X1 M2_mult_28_U215 ( .A(M2_mult_28_n360), .ZN(M2_mult_28_n287) );
  INV_X1 M2_mult_28_U214 ( .A(M2_mult_28_n355), .ZN(M2_mult_28_n278) );
  INV_X1 M2_mult_28_U213 ( .A(M2_mult_28_n354), .ZN(M2_mult_28_n277) );
  HA_X1 M2_mult_28_U50 ( .A(M2_mult_28_n132), .B(M2_mult_28_n140), .CO(
        M2_mult_28_n78), .S(M2_mult_28_n79) );
  FA_X1 M2_mult_28_U49 ( .A(M2_mult_28_n139), .B(M2_mult_28_n124), .CI(
        M2_mult_28_n131), .CO(M2_mult_28_n76), .S(M2_mult_28_n77) );
  HA_X1 M2_mult_28_U48 ( .A(M2_mult_28_n95), .B(M2_mult_28_n123), .CO(
        M2_mult_28_n74), .S(M2_mult_28_n75) );
  FA_X1 M2_mult_28_U47 ( .A(M2_mult_28_n130), .B(M2_mult_28_n138), .CI(
        M2_mult_28_n75), .CO(M2_mult_28_n72), .S(M2_mult_28_n73) );
  FA_X1 M2_mult_28_U46 ( .A(M2_mult_28_n137), .B(M2_mult_28_n115), .CI(
        M2_mult_28_n129), .CO(M2_mult_28_n70), .S(M2_mult_28_n71) );
  FA_X1 M2_mult_28_U45 ( .A(M2_mult_28_n74), .B(M2_mult_28_n122), .CI(
        M2_mult_28_n71), .CO(M2_mult_28_n68), .S(M2_mult_28_n69) );
  HA_X1 M2_mult_28_U44 ( .A(M2_mult_28_n94), .B(M2_mult_28_n114), .CO(
        M2_mult_28_n66), .S(M2_mult_28_n67) );
  FA_X1 M2_mult_28_U43 ( .A(M2_mult_28_n121), .B(M2_mult_28_n136), .CI(
        M2_mult_28_n128), .CO(M2_mult_28_n64), .S(M2_mult_28_n65) );
  FA_X1 M2_mult_28_U42 ( .A(M2_mult_28_n70), .B(M2_mult_28_n67), .CI(
        M2_mult_28_n65), .CO(M2_mult_28_n62), .S(M2_mult_28_n63) );
  FA_X1 M2_mult_28_U41 ( .A(M2_mult_28_n120), .B(M2_mult_28_n106), .CI(
        M2_mult_28_n135), .CO(M2_mult_28_n60), .S(M2_mult_28_n61) );
  FA_X1 M2_mult_28_U40 ( .A(M2_mult_28_n113), .B(M2_mult_28_n127), .CI(
        M2_mult_28_n66), .CO(M2_mult_28_n58), .S(M2_mult_28_n59) );
  FA_X1 M2_mult_28_U39 ( .A(M2_mult_28_n61), .B(M2_mult_28_n64), .CI(
        M2_mult_28_n59), .CO(M2_mult_28_n56), .S(M2_mult_28_n57) );
  FA_X1 M2_mult_28_U36 ( .A(M2_mult_28_n93), .B(M2_mult_28_n119), .CI(
        M2_mult_28_n288), .CO(M2_mult_28_n52), .S(M2_mult_28_n53) );
  FA_X1 M2_mult_28_U35 ( .A(M2_mult_28_n55), .B(M2_mult_28_n126), .CI(
        M2_mult_28_n60), .CO(M2_mult_28_n50), .S(M2_mult_28_n51) );
  FA_X1 M2_mult_28_U34 ( .A(M2_mult_28_n53), .B(M2_mult_28_n58), .CI(
        M2_mult_28_n51), .CO(M2_mult_28_n48), .S(M2_mult_28_n49) );
  FA_X1 M2_mult_28_U32 ( .A(M2_mult_28_n111), .B(M2_mult_28_n104), .CI(
        M2_mult_28_n118), .CO(M2_mult_28_n44), .S(M2_mult_28_n45) );
  FA_X1 M2_mult_28_U31 ( .A(M2_mult_28_n54), .B(M2_mult_28_n284), .CI(
        M2_mult_28_n52), .CO(M2_mult_28_n42), .S(M2_mult_28_n43) );
  FA_X1 M2_mult_28_U30 ( .A(M2_mult_28_n50), .B(M2_mult_28_n45), .CI(
        M2_mult_28_n43), .CO(M2_mult_28_n40), .S(M2_mult_28_n41) );
  FA_X1 M2_mult_28_U29 ( .A(M2_mult_28_n110), .B(M2_mult_28_n103), .CI(
        M2_mult_28_n285), .CO(M2_mult_28_n38), .S(M2_mult_28_n39) );
  FA_X1 M2_mult_28_U28 ( .A(M2_mult_28_n46), .B(M2_mult_28_n117), .CI(
        M2_mult_28_n44), .CO(M2_mult_28_n36), .S(M2_mult_28_n37) );
  FA_X1 M2_mult_28_U27 ( .A(M2_mult_28_n42), .B(M2_mult_28_n39), .CI(
        M2_mult_28_n37), .CO(M2_mult_28_n34), .S(M2_mult_28_n35) );
  FA_X1 M2_mult_28_U25 ( .A(M2_mult_28_n102), .B(M2_mult_28_n109), .CI(
        M2_mult_28_n279), .CO(M2_mult_28_n30), .S(M2_mult_28_n31) );
  FA_X1 M2_mult_28_U24 ( .A(M2_mult_28_n31), .B(M2_mult_28_n38), .CI(
        M2_mult_28_n36), .CO(M2_mult_28_n28), .S(M2_mult_28_n29) );
  FA_X1 M2_mult_28_U23 ( .A(M2_mult_28_n108), .B(M2_mult_28_n32), .CI(
        M2_mult_28_n280), .CO(M2_mult_28_n26), .S(M2_mult_28_n27) );
  FA_X1 M2_mult_28_U22 ( .A(M2_mult_28_n30), .B(M2_mult_28_n101), .CI(
        M2_mult_28_n27), .CO(M2_mult_28_n24), .S(M2_mult_28_n25) );
  FA_X1 M2_mult_28_U20 ( .A(M2_mult_28_n274), .B(M2_mult_28_n100), .CI(
        M2_mult_28_n26), .CO(M2_mult_28_n20), .S(M2_mult_28_n21) );
  FA_X1 M2_mult_28_U19 ( .A(M2_mult_28_n99), .B(M2_mult_28_n22), .CI(
        M2_mult_28_n275), .CO(M2_mult_28_n18), .S(M2_mult_28_n19) );
  FA_X1 M2_mult_28_U10 ( .A(M2_mult_28_n57), .B(M2_mult_28_n62), .CI(
        M2_mult_28_n272), .CO(M2_mult_28_n9), .S(M2_out[0]) );
  FA_X1 M2_mult_28_U9 ( .A(M2_mult_28_n49), .B(M2_mult_28_n56), .CI(
        M2_mult_28_n9), .CO(M2_mult_28_n8), .S(M2_out[1]) );
  FA_X1 M2_mult_28_U8 ( .A(M2_mult_28_n41), .B(M2_mult_28_n48), .CI(
        M2_mult_28_n8), .CO(M2_mult_28_n7), .S(M2_out[2]) );
  FA_X1 M2_mult_28_U7 ( .A(M2_mult_28_n35), .B(M2_mult_28_n40), .CI(
        M2_mult_28_n7), .CO(M2_mult_28_n6), .S(M2_out[3]) );
  FA_X1 M2_mult_28_U6 ( .A(M2_mult_28_n29), .B(M2_mult_28_n34), .CI(
        M2_mult_28_n6), .CO(M2_mult_28_n5), .S(M2_out[4]) );
  FA_X1 M2_mult_28_U5 ( .A(M2_mult_28_n25), .B(M2_mult_28_n28), .CI(
        M2_mult_28_n5), .CO(M2_mult_28_n4), .S(M2_out[5]) );
  FA_X1 M2_mult_28_U4 ( .A(M2_mult_28_n21), .B(M2_mult_28_n24), .CI(
        M2_mult_28_n4), .CO(M2_mult_28_n3), .S(M2_out[6]) );
  FA_X1 M2_mult_28_U3 ( .A(M2_mult_28_n20), .B(M2_mult_28_n19), .CI(
        M2_mult_28_n3), .CO(M2_mult_28_n2), .S(M2_out[7]) );
  XOR2_X1 M3_mult_28_U351 ( .A(D7_out[2]), .B(D7_out[1]), .Z(M3_mult_28_n360)
         );
  NAND2_X1 M3_mult_28_U350 ( .A1(D7_out[1]), .A2(M3_mult_28_n289), .ZN(
        M3_mult_28_n316) );
  XNOR2_X1 M3_mult_28_U349 ( .A(coeff_b[2]), .B(D7_out[1]), .ZN(
        M3_mult_28_n315) );
  OAI22_X1 M3_mult_28_U348 ( .A1(coeff_b[1]), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n315), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n362) );
  XNOR2_X1 M3_mult_28_U347 ( .A(M3_mult_28_n286), .B(D7_out[2]), .ZN(
        M3_mult_28_n361) );
  NAND2_X1 M3_mult_28_U346 ( .A1(M3_mult_28_n287), .A2(M3_mult_28_n361), .ZN(
        M3_mult_28_n309) );
  NAND3_X1 M3_mult_28_U345 ( .A1(M3_mult_28_n360), .A2(M3_mult_28_n291), .A3(
        D7_out[3]), .ZN(M3_mult_28_n359) );
  OAI21_X1 M3_mult_28_U344 ( .B1(M3_mult_28_n286), .B2(M3_mult_28_n309), .A(
        M3_mult_28_n359), .ZN(M3_mult_28_n358) );
  AOI222_X1 M3_mult_28_U343 ( .A1(M3_mult_28_n268), .A2(M3_mult_28_n79), .B1(
        M3_mult_28_n358), .B2(M3_mult_28_n268), .C1(M3_mult_28_n358), .C2(
        M3_mult_28_n79), .ZN(M3_mult_28_n357) );
  AOI222_X1 M3_mult_28_U342 ( .A1(M3_mult_28_n283), .A2(M3_mult_28_n77), .B1(
        M3_mult_28_n283), .B2(M3_mult_28_n78), .C1(M3_mult_28_n78), .C2(
        M3_mult_28_n77), .ZN(M3_mult_28_n356) );
  AOI222_X1 M3_mult_28_U341 ( .A1(M3_mult_28_n282), .A2(M3_mult_28_n73), .B1(
        M3_mult_28_n282), .B2(M3_mult_28_n76), .C1(M3_mult_28_n76), .C2(
        M3_mult_28_n73), .ZN(M3_mult_28_n355) );
  AOI222_X1 M3_mult_28_U340 ( .A1(M3_mult_28_n278), .A2(M3_mult_28_n69), .B1(
        M3_mult_28_n278), .B2(M3_mult_28_n72), .C1(M3_mult_28_n72), .C2(
        M3_mult_28_n69), .ZN(M3_mult_28_n354) );
  AOI222_X1 M3_mult_28_U339 ( .A1(M3_mult_28_n277), .A2(M3_mult_28_n63), .B1(
        M3_mult_28_n277), .B2(M3_mult_28_n68), .C1(M3_mult_28_n68), .C2(
        M3_mult_28_n63), .ZN(M3_mult_28_n353) );
  XOR2_X1 M3_mult_28_U338 ( .A(D7_out[8]), .B(M3_mult_28_n276), .Z(
        M3_mult_28_n295) );
  XNOR2_X1 M3_mult_28_U337 ( .A(coeff_b[6]), .B(D7_out[8]), .ZN(
        M3_mult_28_n352) );
  NOR2_X1 M3_mult_28_U336 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n352), .ZN(
        M3_mult_28_n100) );
  XNOR2_X1 M3_mult_28_U335 ( .A(coeff_b[5]), .B(D7_out[8]), .ZN(
        M3_mult_28_n351) );
  NOR2_X1 M3_mult_28_U334 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n351), .ZN(
        M3_mult_28_n101) );
  XNOR2_X1 M3_mult_28_U333 ( .A(coeff_b[4]), .B(D7_out[8]), .ZN(
        M3_mult_28_n350) );
  NOR2_X1 M3_mult_28_U332 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n350), .ZN(
        M3_mult_28_n102) );
  XNOR2_X1 M3_mult_28_U331 ( .A(coeff_b[3]), .B(D7_out[8]), .ZN(
        M3_mult_28_n349) );
  NOR2_X1 M3_mult_28_U330 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n349), .ZN(
        M3_mult_28_n103) );
  XNOR2_X1 M3_mult_28_U329 ( .A(coeff_b[2]), .B(D7_out[8]), .ZN(
        M3_mult_28_n348) );
  NOR2_X1 M3_mult_28_U328 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n348), .ZN(
        M3_mult_28_n104) );
  NOR2_X1 M3_mult_28_U327 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n291), .ZN(
        M3_mult_28_n106) );
  XNOR2_X1 M3_mult_28_U326 ( .A(coeff_b[8]), .B(D7_out[7]), .ZN(
        M3_mult_28_n314) );
  XNOR2_X1 M3_mult_28_U325 ( .A(M3_mult_28_n276), .B(D7_out[6]), .ZN(
        M3_mult_28_n347) );
  NAND2_X1 M3_mult_28_U324 ( .A1(M3_mult_28_n302), .A2(M3_mult_28_n347), .ZN(
        M3_mult_28_n300) );
  OAI22_X1 M3_mult_28_U323 ( .A1(M3_mult_28_n314), .A2(M3_mult_28_n302), .B1(
        M3_mult_28_n300), .B2(M3_mult_28_n314), .ZN(M3_mult_28_n346) );
  XNOR2_X1 M3_mult_28_U322 ( .A(coeff_b[6]), .B(D7_out[7]), .ZN(
        M3_mult_28_n345) );
  XNOR2_X1 M3_mult_28_U321 ( .A(coeff_b[7]), .B(D7_out[7]), .ZN(
        M3_mult_28_n313) );
  OAI22_X1 M3_mult_28_U320 ( .A1(M3_mult_28_n345), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n313), .ZN(M3_mult_28_n108) );
  XNOR2_X1 M3_mult_28_U319 ( .A(coeff_b[5]), .B(D7_out[7]), .ZN(
        M3_mult_28_n344) );
  OAI22_X1 M3_mult_28_U318 ( .A1(M3_mult_28_n344), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n345), .ZN(M3_mult_28_n109) );
  XNOR2_X1 M3_mult_28_U317 ( .A(coeff_b[4]), .B(D7_out[7]), .ZN(
        M3_mult_28_n343) );
  OAI22_X1 M3_mult_28_U316 ( .A1(M3_mult_28_n343), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n344), .ZN(M3_mult_28_n110) );
  XNOR2_X1 M3_mult_28_U315 ( .A(coeff_b[3]), .B(D7_out[7]), .ZN(
        M3_mult_28_n307) );
  OAI22_X1 M3_mult_28_U314 ( .A1(M3_mult_28_n307), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n343), .ZN(M3_mult_28_n111) );
  XNOR2_X1 M3_mult_28_U313 ( .A(coeff_b[1]), .B(D7_out[7]), .ZN(
        M3_mult_28_n342) );
  XNOR2_X1 M3_mult_28_U312 ( .A(coeff_b[2]), .B(D7_out[7]), .ZN(
        M3_mult_28_n306) );
  OAI22_X1 M3_mult_28_U311 ( .A1(M3_mult_28_n342), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n306), .ZN(M3_mult_28_n113) );
  XNOR2_X1 M3_mult_28_U310 ( .A(coeff_b[0]), .B(D7_out[7]), .ZN(
        M3_mult_28_n341) );
  OAI22_X1 M3_mult_28_U309 ( .A1(M3_mult_28_n341), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n342), .ZN(M3_mult_28_n114) );
  NOR2_X1 M3_mult_28_U308 ( .A1(M3_mult_28_n302), .A2(M3_mult_28_n291), .ZN(
        M3_mult_28_n115) );
  XNOR2_X1 M3_mult_28_U307 ( .A(coeff_b[8]), .B(D7_out[5]), .ZN(
        M3_mult_28_n312) );
  XNOR2_X1 M3_mult_28_U306 ( .A(M3_mult_28_n281), .B(D7_out[4]), .ZN(
        M3_mult_28_n340) );
  NAND2_X1 M3_mult_28_U305 ( .A1(M3_mult_28_n299), .A2(M3_mult_28_n340), .ZN(
        M3_mult_28_n297) );
  OAI22_X1 M3_mult_28_U304 ( .A1(M3_mult_28_n312), .A2(M3_mult_28_n299), .B1(
        M3_mult_28_n297), .B2(M3_mult_28_n312), .ZN(M3_mult_28_n339) );
  XNOR2_X1 M3_mult_28_U303 ( .A(coeff_b[6]), .B(D7_out[5]), .ZN(
        M3_mult_28_n338) );
  XNOR2_X1 M3_mult_28_U302 ( .A(coeff_b[7]), .B(D7_out[5]), .ZN(
        M3_mult_28_n311) );
  OAI22_X1 M3_mult_28_U301 ( .A1(M3_mult_28_n338), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n311), .ZN(M3_mult_28_n117) );
  XNOR2_X1 M3_mult_28_U300 ( .A(coeff_b[5]), .B(D7_out[5]), .ZN(
        M3_mult_28_n337) );
  OAI22_X1 M3_mult_28_U299 ( .A1(M3_mult_28_n337), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n338), .ZN(M3_mult_28_n118) );
  XNOR2_X1 M3_mult_28_U298 ( .A(coeff_b[4]), .B(D7_out[5]), .ZN(
        M3_mult_28_n336) );
  OAI22_X1 M3_mult_28_U297 ( .A1(M3_mult_28_n336), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n337), .ZN(M3_mult_28_n119) );
  XNOR2_X1 M3_mult_28_U296 ( .A(coeff_b[3]), .B(D7_out[5]), .ZN(
        M3_mult_28_n335) );
  OAI22_X1 M3_mult_28_U295 ( .A1(M3_mult_28_n335), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n336), .ZN(M3_mult_28_n120) );
  XNOR2_X1 M3_mult_28_U294 ( .A(coeff_b[2]), .B(D7_out[5]), .ZN(
        M3_mult_28_n334) );
  OAI22_X1 M3_mult_28_U293 ( .A1(M3_mult_28_n334), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n335), .ZN(M3_mult_28_n121) );
  XNOR2_X1 M3_mult_28_U292 ( .A(coeff_b[1]), .B(D7_out[5]), .ZN(
        M3_mult_28_n333) );
  OAI22_X1 M3_mult_28_U291 ( .A1(M3_mult_28_n333), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n334), .ZN(M3_mult_28_n122) );
  XNOR2_X1 M3_mult_28_U290 ( .A(coeff_b[0]), .B(D7_out[5]), .ZN(
        M3_mult_28_n332) );
  OAI22_X1 M3_mult_28_U289 ( .A1(M3_mult_28_n332), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n333), .ZN(M3_mult_28_n123) );
  NOR2_X1 M3_mult_28_U288 ( .A1(M3_mult_28_n299), .A2(M3_mult_28_n291), .ZN(
        M3_mult_28_n124) );
  XOR2_X1 M3_mult_28_U287 ( .A(coeff_b[8]), .B(M3_mult_28_n286), .Z(
        M3_mult_28_n310) );
  OAI22_X1 M3_mult_28_U286 ( .A1(M3_mult_28_n310), .A2(M3_mult_28_n287), .B1(
        M3_mult_28_n309), .B2(M3_mult_28_n310), .ZN(M3_mult_28_n331) );
  XNOR2_X1 M3_mult_28_U285 ( .A(coeff_b[6]), .B(D7_out[3]), .ZN(
        M3_mult_28_n330) );
  XNOR2_X1 M3_mult_28_U284 ( .A(coeff_b[7]), .B(D7_out[3]), .ZN(
        M3_mult_28_n308) );
  OAI22_X1 M3_mult_28_U283 ( .A1(M3_mult_28_n330), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n308), .ZN(M3_mult_28_n126) );
  XNOR2_X1 M3_mult_28_U282 ( .A(coeff_b[5]), .B(D7_out[3]), .ZN(
        M3_mult_28_n329) );
  OAI22_X1 M3_mult_28_U281 ( .A1(M3_mult_28_n329), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n330), .ZN(M3_mult_28_n127) );
  XNOR2_X1 M3_mult_28_U280 ( .A(coeff_b[4]), .B(D7_out[3]), .ZN(
        M3_mult_28_n328) );
  OAI22_X1 M3_mult_28_U279 ( .A1(M3_mult_28_n328), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n329), .ZN(M3_mult_28_n128) );
  XNOR2_X1 M3_mult_28_U278 ( .A(coeff_b[3]), .B(D7_out[3]), .ZN(
        M3_mult_28_n327) );
  OAI22_X1 M3_mult_28_U277 ( .A1(M3_mult_28_n327), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n328), .ZN(M3_mult_28_n129) );
  XNOR2_X1 M3_mult_28_U276 ( .A(coeff_b[2]), .B(D7_out[3]), .ZN(
        M3_mult_28_n326) );
  OAI22_X1 M3_mult_28_U275 ( .A1(M3_mult_28_n326), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n327), .ZN(M3_mult_28_n130) );
  XNOR2_X1 M3_mult_28_U274 ( .A(coeff_b[1]), .B(D7_out[3]), .ZN(
        M3_mult_28_n325) );
  OAI22_X1 M3_mult_28_U273 ( .A1(M3_mult_28_n325), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n326), .ZN(M3_mult_28_n131) );
  XNOR2_X1 M3_mult_28_U272 ( .A(coeff_b[0]), .B(D7_out[3]), .ZN(
        M3_mult_28_n324) );
  OAI22_X1 M3_mult_28_U271 ( .A1(M3_mult_28_n324), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n325), .ZN(M3_mult_28_n132) );
  XNOR2_X1 M3_mult_28_U270 ( .A(coeff_b[8]), .B(D7_out[1]), .ZN(
        M3_mult_28_n322) );
  OAI22_X1 M3_mult_28_U269 ( .A1(M3_mult_28_n289), .A2(M3_mult_28_n322), .B1(
        M3_mult_28_n316), .B2(M3_mult_28_n322), .ZN(M3_mult_28_n323) );
  XNOR2_X1 M3_mult_28_U268 ( .A(coeff_b[7]), .B(D7_out[1]), .ZN(
        M3_mult_28_n321) );
  OAI22_X1 M3_mult_28_U267 ( .A1(M3_mult_28_n321), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n322), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n135) );
  XNOR2_X1 M3_mult_28_U266 ( .A(coeff_b[6]), .B(D7_out[1]), .ZN(
        M3_mult_28_n320) );
  OAI22_X1 M3_mult_28_U265 ( .A1(M3_mult_28_n320), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n321), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n136) );
  XNOR2_X1 M3_mult_28_U264 ( .A(coeff_b[5]), .B(D7_out[1]), .ZN(
        M3_mult_28_n319) );
  OAI22_X1 M3_mult_28_U263 ( .A1(M3_mult_28_n319), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n320), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n137) );
  XNOR2_X1 M3_mult_28_U262 ( .A(coeff_b[4]), .B(D7_out[1]), .ZN(
        M3_mult_28_n318) );
  OAI22_X1 M3_mult_28_U261 ( .A1(M3_mult_28_n318), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n319), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n138) );
  XNOR2_X1 M3_mult_28_U260 ( .A(coeff_b[3]), .B(D7_out[1]), .ZN(
        M3_mult_28_n317) );
  OAI22_X1 M3_mult_28_U259 ( .A1(M3_mult_28_n317), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n318), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n139) );
  OAI22_X1 M3_mult_28_U258 ( .A1(M3_mult_28_n315), .A2(M3_mult_28_n316), .B1(
        M3_mult_28_n317), .B2(M3_mult_28_n289), .ZN(M3_mult_28_n140) );
  OAI22_X1 M3_mult_28_U257 ( .A1(M3_mult_28_n313), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n314), .ZN(M3_mult_28_n22) );
  OAI22_X1 M3_mult_28_U256 ( .A1(M3_mult_28_n311), .A2(M3_mult_28_n297), .B1(
        M3_mult_28_n299), .B2(M3_mult_28_n312), .ZN(M3_mult_28_n32) );
  OAI22_X1 M3_mult_28_U255 ( .A1(M3_mult_28_n308), .A2(M3_mult_28_n309), .B1(
        M3_mult_28_n287), .B2(M3_mult_28_n310), .ZN(M3_mult_28_n46) );
  OAI22_X1 M3_mult_28_U254 ( .A1(M3_mult_28_n306), .A2(M3_mult_28_n300), .B1(
        M3_mult_28_n302), .B2(M3_mult_28_n307), .ZN(M3_mult_28_n305) );
  XNOR2_X1 M3_mult_28_U253 ( .A(M3_mult_28_n290), .B(D7_out[8]), .ZN(
        M3_mult_28_n304) );
  NAND2_X1 M3_mult_28_U252 ( .A1(M3_mult_28_n304), .A2(M3_mult_28_n271), .ZN(
        M3_mult_28_n303) );
  NAND2_X1 M3_mult_28_U251 ( .A1(M3_mult_28_n273), .A2(M3_mult_28_n303), .ZN(
        M3_mult_28_n54) );
  XNOR2_X1 M3_mult_28_U250 ( .A(M3_mult_28_n303), .B(M3_mult_28_n273), .ZN(
        M3_mult_28_n55) );
  AND3_X1 M3_mult_28_U249 ( .A1(D7_out[8]), .A2(M3_mult_28_n291), .A3(
        M3_mult_28_n271), .ZN(M3_mult_28_n93) );
  OR3_X1 M3_mult_28_U248 ( .A1(M3_mult_28_n302), .A2(coeff_b[0]), .A3(
        M3_mult_28_n276), .ZN(M3_mult_28_n301) );
  OAI21_X1 M3_mult_28_U247 ( .B1(M3_mult_28_n276), .B2(M3_mult_28_n300), .A(
        M3_mult_28_n301), .ZN(M3_mult_28_n94) );
  OR3_X1 M3_mult_28_U246 ( .A1(M3_mult_28_n299), .A2(coeff_b[0]), .A3(
        M3_mult_28_n281), .ZN(M3_mult_28_n298) );
  OAI21_X1 M3_mult_28_U245 ( .B1(M3_mult_28_n281), .B2(M3_mult_28_n297), .A(
        M3_mult_28_n298), .ZN(M3_mult_28_n95) );
  XNOR2_X1 M3_mult_28_U244 ( .A(coeff_b[7]), .B(D7_out[8]), .ZN(
        M3_mult_28_n296) );
  NOR2_X1 M3_mult_28_U243 ( .A1(M3_mult_28_n295), .A2(M3_mult_28_n296), .ZN(
        M3_mult_28_n99) );
  XOR2_X1 M3_mult_28_U242 ( .A(coeff_b[8]), .B(D7_out[8]), .Z(M3_mult_28_n294)
         );
  NAND2_X1 M3_mult_28_U241 ( .A1(M3_mult_28_n294), .A2(M3_mult_28_n271), .ZN(
        M3_mult_28_n292) );
  XOR2_X1 M3_mult_28_U240 ( .A(M3_mult_28_n2), .B(M3_mult_28_n18), .Z(
        M3_mult_28_n293) );
  XOR2_X1 M3_mult_28_U239 ( .A(M3_mult_28_n292), .B(M3_mult_28_n293), .Z(
        M3_out[8]) );
  INV_X1 M3_mult_28_U238 ( .A(M3_mult_28_n346), .ZN(M3_mult_28_n275) );
  INV_X1 M3_mult_28_U237 ( .A(M3_mult_28_n22), .ZN(M3_mult_28_n274) );
  INV_X1 M3_mult_28_U236 ( .A(M3_mult_28_n323), .ZN(M3_mult_28_n288) );
  INV_X1 M3_mult_28_U235 ( .A(D7_out[7]), .ZN(M3_mult_28_n276) );
  INV_X1 M3_mult_28_U234 ( .A(coeff_b[1]), .ZN(M3_mult_28_n290) );
  AND3_X1 M3_mult_28_U233 ( .A1(M3_mult_28_n362), .A2(M3_mult_28_n290), .A3(
        D7_out[1]), .ZN(M3_mult_28_n270) );
  AND2_X1 M3_mult_28_U232 ( .A1(M3_mult_28_n360), .A2(M3_mult_28_n362), .ZN(
        M3_mult_28_n269) );
  MUX2_X1 M3_mult_28_U231 ( .A(M3_mult_28_n269), .B(M3_mult_28_n270), .S(
        M3_mult_28_n291), .Z(M3_mult_28_n268) );
  INV_X1 M3_mult_28_U230 ( .A(coeff_b[0]), .ZN(M3_mult_28_n291) );
  INV_X1 M3_mult_28_U229 ( .A(D7_out[5]), .ZN(M3_mult_28_n281) );
  INV_X1 M3_mult_28_U228 ( .A(D7_out[3]), .ZN(M3_mult_28_n286) );
  INV_X1 M3_mult_28_U227 ( .A(D7_out[0]), .ZN(M3_mult_28_n289) );
  XOR2_X1 M3_mult_28_U226 ( .A(D7_out[6]), .B(M3_mult_28_n281), .Z(
        M3_mult_28_n302) );
  XOR2_X1 M3_mult_28_U225 ( .A(D7_out[4]), .B(M3_mult_28_n286), .Z(
        M3_mult_28_n299) );
  INV_X1 M3_mult_28_U224 ( .A(M3_mult_28_n46), .ZN(M3_mult_28_n284) );
  INV_X1 M3_mult_28_U223 ( .A(M3_mult_28_n339), .ZN(M3_mult_28_n280) );
  INV_X1 M3_mult_28_U222 ( .A(M3_mult_28_n331), .ZN(M3_mult_28_n285) );
  INV_X1 M3_mult_28_U221 ( .A(M3_mult_28_n32), .ZN(M3_mult_28_n279) );
  INV_X1 M3_mult_28_U220 ( .A(M3_mult_28_n295), .ZN(M3_mult_28_n271) );
  INV_X1 M3_mult_28_U219 ( .A(M3_mult_28_n305), .ZN(M3_mult_28_n273) );
  INV_X1 M3_mult_28_U218 ( .A(M3_mult_28_n357), .ZN(M3_mult_28_n283) );
  INV_X1 M3_mult_28_U217 ( .A(M3_mult_28_n356), .ZN(M3_mult_28_n282) );
  INV_X1 M3_mult_28_U216 ( .A(M3_mult_28_n353), .ZN(M3_mult_28_n272) );
  INV_X1 M3_mult_28_U215 ( .A(M3_mult_28_n360), .ZN(M3_mult_28_n287) );
  INV_X1 M3_mult_28_U214 ( .A(M3_mult_28_n355), .ZN(M3_mult_28_n278) );
  INV_X1 M3_mult_28_U213 ( .A(M3_mult_28_n354), .ZN(M3_mult_28_n277) );
  HA_X1 M3_mult_28_U50 ( .A(M3_mult_28_n132), .B(M3_mult_28_n140), .CO(
        M3_mult_28_n78), .S(M3_mult_28_n79) );
  FA_X1 M3_mult_28_U49 ( .A(M3_mult_28_n139), .B(M3_mult_28_n124), .CI(
        M3_mult_28_n131), .CO(M3_mult_28_n76), .S(M3_mult_28_n77) );
  HA_X1 M3_mult_28_U48 ( .A(M3_mult_28_n95), .B(M3_mult_28_n123), .CO(
        M3_mult_28_n74), .S(M3_mult_28_n75) );
  FA_X1 M3_mult_28_U47 ( .A(M3_mult_28_n130), .B(M3_mult_28_n138), .CI(
        M3_mult_28_n75), .CO(M3_mult_28_n72), .S(M3_mult_28_n73) );
  FA_X1 M3_mult_28_U46 ( .A(M3_mult_28_n137), .B(M3_mult_28_n115), .CI(
        M3_mult_28_n129), .CO(M3_mult_28_n70), .S(M3_mult_28_n71) );
  FA_X1 M3_mult_28_U45 ( .A(M3_mult_28_n74), .B(M3_mult_28_n122), .CI(
        M3_mult_28_n71), .CO(M3_mult_28_n68), .S(M3_mult_28_n69) );
  HA_X1 M3_mult_28_U44 ( .A(M3_mult_28_n94), .B(M3_mult_28_n114), .CO(
        M3_mult_28_n66), .S(M3_mult_28_n67) );
  FA_X1 M3_mult_28_U43 ( .A(M3_mult_28_n121), .B(M3_mult_28_n136), .CI(
        M3_mult_28_n128), .CO(M3_mult_28_n64), .S(M3_mult_28_n65) );
  FA_X1 M3_mult_28_U42 ( .A(M3_mult_28_n70), .B(M3_mult_28_n67), .CI(
        M3_mult_28_n65), .CO(M3_mult_28_n62), .S(M3_mult_28_n63) );
  FA_X1 M3_mult_28_U41 ( .A(M3_mult_28_n120), .B(M3_mult_28_n106), .CI(
        M3_mult_28_n135), .CO(M3_mult_28_n60), .S(M3_mult_28_n61) );
  FA_X1 M3_mult_28_U40 ( .A(M3_mult_28_n113), .B(M3_mult_28_n127), .CI(
        M3_mult_28_n66), .CO(M3_mult_28_n58), .S(M3_mult_28_n59) );
  FA_X1 M3_mult_28_U39 ( .A(M3_mult_28_n61), .B(M3_mult_28_n64), .CI(
        M3_mult_28_n59), .CO(M3_mult_28_n56), .S(M3_mult_28_n57) );
  FA_X1 M3_mult_28_U36 ( .A(M3_mult_28_n93), .B(M3_mult_28_n119), .CI(
        M3_mult_28_n288), .CO(M3_mult_28_n52), .S(M3_mult_28_n53) );
  FA_X1 M3_mult_28_U35 ( .A(M3_mult_28_n55), .B(M3_mult_28_n126), .CI(
        M3_mult_28_n60), .CO(M3_mult_28_n50), .S(M3_mult_28_n51) );
  FA_X1 M3_mult_28_U34 ( .A(M3_mult_28_n53), .B(M3_mult_28_n58), .CI(
        M3_mult_28_n51), .CO(M3_mult_28_n48), .S(M3_mult_28_n49) );
  FA_X1 M3_mult_28_U32 ( .A(M3_mult_28_n111), .B(M3_mult_28_n104), .CI(
        M3_mult_28_n118), .CO(M3_mult_28_n44), .S(M3_mult_28_n45) );
  FA_X1 M3_mult_28_U31 ( .A(M3_mult_28_n54), .B(M3_mult_28_n284), .CI(
        M3_mult_28_n52), .CO(M3_mult_28_n42), .S(M3_mult_28_n43) );
  FA_X1 M3_mult_28_U30 ( .A(M3_mult_28_n50), .B(M3_mult_28_n45), .CI(
        M3_mult_28_n43), .CO(M3_mult_28_n40), .S(M3_mult_28_n41) );
  FA_X1 M3_mult_28_U29 ( .A(M3_mult_28_n110), .B(M3_mult_28_n103), .CI(
        M3_mult_28_n285), .CO(M3_mult_28_n38), .S(M3_mult_28_n39) );
  FA_X1 M3_mult_28_U28 ( .A(M3_mult_28_n46), .B(M3_mult_28_n117), .CI(
        M3_mult_28_n44), .CO(M3_mult_28_n36), .S(M3_mult_28_n37) );
  FA_X1 M3_mult_28_U27 ( .A(M3_mult_28_n42), .B(M3_mult_28_n39), .CI(
        M3_mult_28_n37), .CO(M3_mult_28_n34), .S(M3_mult_28_n35) );
  FA_X1 M3_mult_28_U25 ( .A(M3_mult_28_n102), .B(M3_mult_28_n109), .CI(
        M3_mult_28_n279), .CO(M3_mult_28_n30), .S(M3_mult_28_n31) );
  FA_X1 M3_mult_28_U24 ( .A(M3_mult_28_n31), .B(M3_mult_28_n38), .CI(
        M3_mult_28_n36), .CO(M3_mult_28_n28), .S(M3_mult_28_n29) );
  FA_X1 M3_mult_28_U23 ( .A(M3_mult_28_n108), .B(M3_mult_28_n32), .CI(
        M3_mult_28_n280), .CO(M3_mult_28_n26), .S(M3_mult_28_n27) );
  FA_X1 M3_mult_28_U22 ( .A(M3_mult_28_n30), .B(M3_mult_28_n101), .CI(
        M3_mult_28_n27), .CO(M3_mult_28_n24), .S(M3_mult_28_n25) );
  FA_X1 M3_mult_28_U20 ( .A(M3_mult_28_n274), .B(M3_mult_28_n100), .CI(
        M3_mult_28_n26), .CO(M3_mult_28_n20), .S(M3_mult_28_n21) );
  FA_X1 M3_mult_28_U19 ( .A(M3_mult_28_n99), .B(M3_mult_28_n22), .CI(
        M3_mult_28_n275), .CO(M3_mult_28_n18), .S(M3_mult_28_n19) );
  FA_X1 M3_mult_28_U10 ( .A(M3_mult_28_n57), .B(M3_mult_28_n62), .CI(
        M3_mult_28_n272), .CO(M3_mult_28_n9), .S(M3_out[0]) );
  FA_X1 M3_mult_28_U9 ( .A(M3_mult_28_n49), .B(M3_mult_28_n56), .CI(
        M3_mult_28_n9), .CO(M3_mult_28_n8), .S(M3_out[1]) );
  FA_X1 M3_mult_28_U8 ( .A(M3_mult_28_n41), .B(M3_mult_28_n48), .CI(
        M3_mult_28_n8), .CO(M3_mult_28_n7), .S(M3_out[2]) );
  FA_X1 M3_mult_28_U7 ( .A(M3_mult_28_n35), .B(M3_mult_28_n40), .CI(
        M3_mult_28_n7), .CO(M3_mult_28_n6), .S(M3_out[3]) );
  FA_X1 M3_mult_28_U6 ( .A(M3_mult_28_n29), .B(M3_mult_28_n34), .CI(
        M3_mult_28_n6), .CO(M3_mult_28_n5), .S(M3_out[4]) );
  FA_X1 M3_mult_28_U5 ( .A(M3_mult_28_n25), .B(M3_mult_28_n28), .CI(
        M3_mult_28_n5), .CO(M3_mult_28_n4), .S(M3_out[5]) );
  FA_X1 M3_mult_28_U4 ( .A(M3_mult_28_n21), .B(M3_mult_28_n24), .CI(
        M3_mult_28_n4), .CO(M3_mult_28_n3), .S(M3_out[6]) );
  FA_X1 M3_mult_28_U3 ( .A(M3_mult_28_n20), .B(M3_mult_28_n19), .CI(
        M3_mult_28_n3), .CO(M3_mult_28_n2), .S(M3_out[7]) );
  NAND2_X1 VIN_DELAY1_U3 ( .A1(1'b1), .A2(v_in), .ZN(VIN_DELAY1_n1) );
  OAI21_X1 VIN_DELAY1_U2 ( .B1(1'b1), .B2(VIN_DELAY1_n2), .A(VIN_DELAY1_n1), 
        .ZN(VIN_DELAY1_n3) );
  DFFR_X1 VIN_DELAY1_d_out_reg ( .D(VIN_DELAY1_n3), .CK(clk), .RN(rst_n), .Q(
        vin_delay1_out), .QN(VIN_DELAY1_n2) );
  NAND2_X1 VIN_DELAY2_U3 ( .A1(1'b1), .A2(vin_delay1_out), .ZN(VIN_DELAY2_n6)
         );
  OAI21_X1 VIN_DELAY2_U2 ( .B1(1'b1), .B2(VIN_DELAY2_n5), .A(VIN_DELAY2_n6), 
        .ZN(VIN_DELAY2_n4) );
  DFFR_X1 VIN_DELAY2_d_out_reg ( .D(VIN_DELAY2_n4), .CK(clk), .RN(rst_n), .Q(
        D5_en), .QN(VIN_DELAY2_n5) );
  NAND2_X1 VIN_DELAY3_U3 ( .A1(1'b1), .A2(D5_en), .ZN(VIN_DELAY3_n6) );
  OAI21_X1 VIN_DELAY3_U2 ( .B1(1'b1), .B2(VIN_DELAY3_n5), .A(VIN_DELAY3_n6), 
        .ZN(VIN_DELAY3_n4) );
  DFFR_X1 VIN_DELAY3_d_out_reg ( .D(VIN_DELAY3_n4), .CK(clk), .RN(rst_n), .Q(
        D4_en), .QN(VIN_DELAY3_n5) );
  NAND2_X1 VIN_DELAY4_U3 ( .A1(1'b1), .A2(D4_en), .ZN(VIN_DELAY4_n6) );
  OAI21_X1 VIN_DELAY4_U2 ( .B1(1'b1), .B2(VIN_DELAY4_n5), .A(VIN_DELAY4_n6), 
        .ZN(VIN_DELAY4_n4) );
  DFFR_X1 VIN_DELAY4_d_out_reg ( .D(VIN_DELAY4_n4), .CK(clk), .RN(rst_n), .Q(
        vin_delay4_out), .QN(VIN_DELAY4_n5) );
  NAND2_X1 VIN_DELAY5_U3 ( .A1(1'b1), .A2(vin_delay4_out), .ZN(VIN_DELAY5_n6)
         );
  OAI21_X1 VIN_DELAY5_U2 ( .B1(1'b1), .B2(VIN_DELAY5_n5), .A(VIN_DELAY5_n6), 
        .ZN(VIN_DELAY5_n4) );
  DFFR_X1 VIN_DELAY5_d_out_reg ( .D(VIN_DELAY5_n4), .CK(clk), .RN(rst_n), .Q(
        v_out), .QN(VIN_DELAY5_n5) );
endmodule

