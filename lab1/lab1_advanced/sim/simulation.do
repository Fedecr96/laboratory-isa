set SRC_DIR ../src
set TB_DIR ../tb

set DUT iir_o1_n9_optimized
set TB tb_iir_o1_n9_optimized

# Create work library
vlib work

# Compile all the vhdl files
vcom -93 -work ./work $SRC_DIR/*.vhd

# Compile the testbench files
vcom -93 -work ./work $TB_DIR/data_maker.vhd
vcom -93 -work ./work $TB_DIR/data_sink.vhd
vcom -93 -work ./work $TB_DIR/clk_gen.vhd
vlog $TB_DIR/$TB.v

# Compile the vhdl testbench 
# vcom -93 -work ./work $TB_DIR/$TB.vhd

# Simulate in command line mode (-c) without otimizations
# (-novopt) with time resolution ns
vsim -t ns -c -novopt work.$TB

# Run the simulation
run 10 us

# Quit Modelsim
quit -f
