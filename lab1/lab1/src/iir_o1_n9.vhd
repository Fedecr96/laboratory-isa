library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity iir_o1_n9 is
	port(
		--input signals
		clk		: in std_logic;
		v_in		: in std_logic;
		rst_n		: in std_logic;
		x_n		: in signed(8 downto 0);
		coeff_a 	: in signed(8 downto 0);
		coeff_b 	: in signed(8 downto 0);

		--output signals
		y_n		: out signed(8 downto 0);
		v_out		: out std_logic
	);
end entity iir_o1_n9;

architecture behaviour of IIR_o1_n9 is

	-- COMPONENTS --

	component register_9bit is 
		port(
			--input signals
			clk		: in std_logic;
			reg_en		: in std_logic;
			d_in		: in signed(8 downto 0);

			--output signals
			d_out		: out signed(8 downto 0)
		);
	end component register_9bit;

	component register_9bit_rst is 
		port(
			--input signals
			clk		: in std_logic;
			reg_en		: in std_logic;
			rst		: in std_logic;
			d_in		: in signed(8 downto 0);

			--output signals
			d_out		: out signed(8 downto 0)
		);
	end component register_9bit_rst;

	component register_delay is
		port(
			--input signals
			clk		: in std_logic;
			d_in		: in std_logic;
			rst		: in std_logic;
			--output signals
			d_out		: out std_logic
		);
	end component register_delay;
	
	component adder_bhv9 is
		port(
			in1	: in signed (8 downto 0);
			in2	: in signed (8 downto 0);
		
			sum_out	: out signed(8 downto 0)
		);
	end component adder_bhv9;

	component multiplier_bhv9 is
		port(
			in1		: in signed (8 downto 0);
			in2		: in signed (8 downto 0);
		
			mpy_out	: out signed(8 downto 0)
		);
	end component multiplier_bhv9;
	
	-- SIGNALS --
	signal reg_in_out, A1_out, reg_feed_out, A2_out, M1_out, M2_out	: signed (8 downto 0);
	signal reg_feed_en, reg_out_en : std_logic;
	signal rst_int	: std_logic;

	BEGIN

	rst_int <= not rst_n;

	REG_IN : register_9bit port map(
		clk 	=> clk,
		reg_en 	=> v_in,
		d_in	=> x_n,
		d_out	=> reg_in_out
	);

	REG_FEED : register_9bit_rst port map(
		clk	=> clk,
		reg_en	=> reg_feed_en,
		rst 	=> rst_int,
		d_in	=> A1_out,
		d_out	=> reg_feed_out
	);

	REG_OUT : register_9bit port map(
		clk 	=> clk,
		reg_en 	=> reg_feed_en,
		d_in	=> M2_out,
		d_out	=> y_n
	);

	DELAY_FEED : register_delay port map(
		clk	=> clk,
		d_in	=> v_in,
		rst	=> rst_int,
		d_out	=> reg_feed_en
	);
	

	DELAY_VOUT : register_delay port map(
		clk	=> clk,
		d_in	=> reg_feed_en,
		rst 	=> rst_int,
		d_out	=> v_out
	);

	A1: adder_bhv9 port map(
		in1	=> reg_in_out,
		in2	=> M1_out,
		sum_out	=> A1_out
	);

	A2: adder_bhv9 port map(
		in1	=> A1_out,
		in2	=> reg_feed_out,
		sum_out	=> A2_out
	);

	M1: multiplier_bhv9 port map(
		in1	=> reg_feed_out,
		in2	=> coeff_a,
		mpy_out	=> M1_out
	);

	M2: multiplier_bhv9 port map(
		in1	=> A2_out,
		in2	=> coeff_b,
		mpy_out	=> M2_out
	);

end architecture behaviour;
