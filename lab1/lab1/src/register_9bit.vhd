library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_9bit is
	port(
		--input signals
		clk			: in std_logic;
		reg_en		: in std_logic;
		d_in		: in signed(8 downto 0);

		--output signals
		d_out		: out signed(8 downto 0)
	);
end entity register_9bit;

architecture behaviour of register_9bit is
begin

	process(clk)
	begin
		if(clk' event and clk='1') then
			if(reg_en='1') then
				d_out <= d_in;	
			end if;
		end if;
	end process;
	
end architecture behaviour;
