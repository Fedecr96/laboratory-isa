library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multiplier_bhv9 is
	port(
		in1		: in signed (8 downto 0);
		in2		: in signed (8 downto 0);
		
		mpy_out	: out signed(8 downto 0)
	);
end entity multiplier_bhv9;


architecture bhv of multiplier_bhv9 is

signal in_1, in_2 : signed (8 downto 0);
signal out_18bit : signed(17 downto 0);

begin

	--in_1 <= (others => in1(8));
	in_1(8 downto 0) <= in1;
	
	--in_2 <= (others => in2(8));
	in_2(8 downto 0) <= in2;

		out_18bit <= in_1 * in_2;
	
	mpy_out <= out_18bit(16 downto 8);

end architecture;
