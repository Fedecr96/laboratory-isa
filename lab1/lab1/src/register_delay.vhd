library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_delay is
	port(
		--input signals
		clk			: in std_logic;
		d_in		   : in std_logic;
      rst      : in std_logic; 
		--output signals
		d_out		: out std_logic
	);
end entity register_delay;

architecture behaviour of register_delay is
begin

	process(clk,rst)
	begin
		if rst='1' then
			d_out <= '0';
		elsif(clk' event and clk='1') then
			d_out <= d_in;	
		end if;
	end process;
	
end architecture behaviour;
