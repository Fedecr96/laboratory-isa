/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP4
// Date      : Wed Nov  4 13:14:05 2020
/////////////////////////////////////////////////////////////


module iir_o1_n9 ( clk, v_in, rst_n, x_n, coeff_a, coeff_b, y_n, v_out );
  input [8:0] x_n;
  input [8:0] coeff_a;
  input [8:0] coeff_b;
  output [8:0] y_n;
  input clk, v_in, rst_n;
  output v_out;
  wire   reg_feed_en, n2, REG_IN_n27, REG_IN_n26, REG_IN_n25, REG_IN_n24,
         REG_IN_n23, REG_IN_n22, REG_IN_n21, REG_IN_n20, REG_IN_n19,
         REG_IN_n18, REG_IN_n17, REG_IN_n16, REG_IN_n15, REG_IN_n14,
         REG_IN_n13, REG_IN_n12, REG_IN_n11, REG_IN_n10, REG_IN_n9, REG_IN_n8,
         REG_IN_n7, REG_IN_n6, REG_IN_n5, REG_IN_n4, REG_IN_n3, REG_IN_n2,
         REG_IN_n1, REG_FEED_n30, REG_FEED_n29, REG_FEED_n27, REG_FEED_n28,
         REG_FEED_n26, REG_FEED_n25, REG_FEED_n24, REG_FEED_n23, REG_FEED_n22,
         REG_FEED_n21, REG_FEED_n20, REG_FEED_n19, REG_FEED_n18, REG_FEED_n17,
         REG_FEED_n16, REG_FEED_n15, REG_FEED_n14, REG_FEED_n13, REG_FEED_n12,
         REG_FEED_n11, REG_FEED_n10, REG_FEED_n9, REG_FEED_n8, REG_FEED_n7,
         REG_FEED_n6, REG_FEED_n5, REG_FEED_n4, REG_FEED_n3, REG_FEED_n2,
         REG_FEED_n1, REG_OUT_n56, REG_OUT_n55, REG_OUT_n54, REG_OUT_n53,
         REG_OUT_n52, REG_OUT_n51, REG_OUT_n50, REG_OUT_n49, REG_OUT_n48,
         REG_OUT_n47, REG_OUT_n46, REG_OUT_n45, REG_OUT_n44, REG_OUT_n43,
         REG_OUT_n42, REG_OUT_n41, REG_OUT_n40, REG_OUT_n39, REG_OUT_n38,
         REG_OUT_n37, REG_OUT_n36, REG_OUT_n35, REG_OUT_n34, REG_OUT_n33,
         REG_OUT_n32, REG_OUT_n31, REG_OUT_n30, REG_OUT_n29, REG_OUT_n28,
         DELAY_FEED_n1, DELAY_VOUT_n1, M1_mult_28_n362, M1_mult_28_n361,
         M1_mult_28_n360, M1_mult_28_n359, M1_mult_28_n358, M1_mult_28_n357,
         M1_mult_28_n356, M1_mult_28_n355, M1_mult_28_n354, M1_mult_28_n353,
         M1_mult_28_n352, M1_mult_28_n351, M1_mult_28_n350, M1_mult_28_n349,
         M1_mult_28_n348, M1_mult_28_n347, M1_mult_28_n346, M1_mult_28_n345,
         M1_mult_28_n344, M1_mult_28_n343, M1_mult_28_n342, M1_mult_28_n341,
         M1_mult_28_n340, M1_mult_28_n339, M1_mult_28_n338, M1_mult_28_n337,
         M1_mult_28_n336, M1_mult_28_n335, M1_mult_28_n334, M1_mult_28_n333,
         M1_mult_28_n332, M1_mult_28_n331, M1_mult_28_n330, M1_mult_28_n329,
         M1_mult_28_n328, M1_mult_28_n327, M1_mult_28_n326, M1_mult_28_n325,
         M1_mult_28_n324, M1_mult_28_n323, M1_mult_28_n322, M1_mult_28_n321,
         M1_mult_28_n320, M1_mult_28_n319, M1_mult_28_n318, M1_mult_28_n317,
         M1_mult_28_n316, M1_mult_28_n315, M1_mult_28_n314, M1_mult_28_n313,
         M1_mult_28_n312, M1_mult_28_n311, M1_mult_28_n310, M1_mult_28_n309,
         M1_mult_28_n308, M1_mult_28_n307, M1_mult_28_n306, M1_mult_28_n305,
         M1_mult_28_n304, M1_mult_28_n303, M1_mult_28_n302, M1_mult_28_n301,
         M1_mult_28_n300, M1_mult_28_n299, M1_mult_28_n298, M1_mult_28_n297,
         M1_mult_28_n296, M1_mult_28_n295, M1_mult_28_n294, M1_mult_28_n293,
         M1_mult_28_n292, M1_mult_28_n291, M1_mult_28_n290, M1_mult_28_n289,
         M1_mult_28_n288, M1_mult_28_n287, M1_mult_28_n286, M1_mult_28_n285,
         M1_mult_28_n284, M1_mult_28_n283, M1_mult_28_n282, M1_mult_28_n281,
         M1_mult_28_n280, M1_mult_28_n279, M1_mult_28_n278, M1_mult_28_n277,
         M1_mult_28_n276, M1_mult_28_n275, M1_mult_28_n274, M1_mult_28_n273,
         M1_mult_28_n272, M1_mult_28_n271, M1_mult_28_n270, M1_mult_28_n269,
         M1_mult_28_n268, M1_mult_28_n140, M1_mult_28_n139, M1_mult_28_n138,
         M1_mult_28_n137, M1_mult_28_n136, M1_mult_28_n135, M1_mult_28_n132,
         M1_mult_28_n131, M1_mult_28_n130, M1_mult_28_n129, M1_mult_28_n128,
         M1_mult_28_n127, M1_mult_28_n126, M1_mult_28_n124, M1_mult_28_n123,
         M1_mult_28_n122, M1_mult_28_n121, M1_mult_28_n120, M1_mult_28_n119,
         M1_mult_28_n118, M1_mult_28_n117, M1_mult_28_n115, M1_mult_28_n114,
         M1_mult_28_n113, M1_mult_28_n111, M1_mult_28_n110, M1_mult_28_n109,
         M1_mult_28_n108, M1_mult_28_n106, M1_mult_28_n104, M1_mult_28_n103,
         M1_mult_28_n102, M1_mult_28_n101, M1_mult_28_n100, M1_mult_28_n99,
         M1_mult_28_n95, M1_mult_28_n94, M1_mult_28_n93, M1_mult_28_n79,
         M1_mult_28_n78, M1_mult_28_n77, M1_mult_28_n76, M1_mult_28_n75,
         M1_mult_28_n74, M1_mult_28_n73, M1_mult_28_n72, M1_mult_28_n71,
         M1_mult_28_n70, M1_mult_28_n69, M1_mult_28_n68, M1_mult_28_n67,
         M1_mult_28_n66, M1_mult_28_n65, M1_mult_28_n64, M1_mult_28_n63,
         M1_mult_28_n62, M1_mult_28_n61, M1_mult_28_n60, M1_mult_28_n59,
         M1_mult_28_n58, M1_mult_28_n57, M1_mult_28_n56, M1_mult_28_n55,
         M1_mult_28_n54, M1_mult_28_n53, M1_mult_28_n52, M1_mult_28_n51,
         M1_mult_28_n50, M1_mult_28_n49, M1_mult_28_n48, M1_mult_28_n46,
         M1_mult_28_n45, M1_mult_28_n44, M1_mult_28_n43, M1_mult_28_n42,
         M1_mult_28_n41, M1_mult_28_n40, M1_mult_28_n39, M1_mult_28_n38,
         M1_mult_28_n37, M1_mult_28_n36, M1_mult_28_n35, M1_mult_28_n34,
         M1_mult_28_n32, M1_mult_28_n31, M1_mult_28_n30, M1_mult_28_n29,
         M1_mult_28_n28, M1_mult_28_n27, M1_mult_28_n26, M1_mult_28_n25,
         M1_mult_28_n24, M1_mult_28_n22, M1_mult_28_n21, M1_mult_28_n20,
         M1_mult_28_n19, M1_mult_28_n18, M1_mult_28_n9, M1_mult_28_n8,
         M1_mult_28_n7, M1_mult_28_n6, M1_mult_28_n5, M1_mult_28_n4,
         M1_mult_28_n3, M1_mult_28_n2, M2_mult_28_n362, M2_mult_28_n361,
         M2_mult_28_n360, M2_mult_28_n359, M2_mult_28_n358, M2_mult_28_n357,
         M2_mult_28_n356, M2_mult_28_n355, M2_mult_28_n354, M2_mult_28_n353,
         M2_mult_28_n352, M2_mult_28_n351, M2_mult_28_n350, M2_mult_28_n349,
         M2_mult_28_n348, M2_mult_28_n347, M2_mult_28_n346, M2_mult_28_n345,
         M2_mult_28_n344, M2_mult_28_n343, M2_mult_28_n342, M2_mult_28_n341,
         M2_mult_28_n340, M2_mult_28_n339, M2_mult_28_n338, M2_mult_28_n337,
         M2_mult_28_n336, M2_mult_28_n335, M2_mult_28_n334, M2_mult_28_n333,
         M2_mult_28_n332, M2_mult_28_n331, M2_mult_28_n330, M2_mult_28_n329,
         M2_mult_28_n328, M2_mult_28_n327, M2_mult_28_n326, M2_mult_28_n325,
         M2_mult_28_n324, M2_mult_28_n323, M2_mult_28_n322, M2_mult_28_n321,
         M2_mult_28_n320, M2_mult_28_n319, M2_mult_28_n318, M2_mult_28_n317,
         M2_mult_28_n316, M2_mult_28_n315, M2_mult_28_n314, M2_mult_28_n313,
         M2_mult_28_n312, M2_mult_28_n311, M2_mult_28_n310, M2_mult_28_n309,
         M2_mult_28_n308, M2_mult_28_n307, M2_mult_28_n306, M2_mult_28_n305,
         M2_mult_28_n304, M2_mult_28_n303, M2_mult_28_n302, M2_mult_28_n301,
         M2_mult_28_n300, M2_mult_28_n299, M2_mult_28_n298, M2_mult_28_n297,
         M2_mult_28_n296, M2_mult_28_n295, M2_mult_28_n294, M2_mult_28_n293,
         M2_mult_28_n292, M2_mult_28_n291, M2_mult_28_n290, M2_mult_28_n289,
         M2_mult_28_n288, M2_mult_28_n287, M2_mult_28_n286, M2_mult_28_n285,
         M2_mult_28_n284, M2_mult_28_n283, M2_mult_28_n282, M2_mult_28_n281,
         M2_mult_28_n280, M2_mult_28_n279, M2_mult_28_n278, M2_mult_28_n277,
         M2_mult_28_n276, M2_mult_28_n275, M2_mult_28_n274, M2_mult_28_n273,
         M2_mult_28_n272, M2_mult_28_n271, M2_mult_28_n270, M2_mult_28_n269,
         M2_mult_28_n268, M2_mult_28_n140, M2_mult_28_n139, M2_mult_28_n138,
         M2_mult_28_n137, M2_mult_28_n136, M2_mult_28_n135, M2_mult_28_n132,
         M2_mult_28_n131, M2_mult_28_n130, M2_mult_28_n129, M2_mult_28_n128,
         M2_mult_28_n127, M2_mult_28_n126, M2_mult_28_n124, M2_mult_28_n123,
         M2_mult_28_n122, M2_mult_28_n121, M2_mult_28_n120, M2_mult_28_n119,
         M2_mult_28_n118, M2_mult_28_n117, M2_mult_28_n115, M2_mult_28_n114,
         M2_mult_28_n113, M2_mult_28_n111, M2_mult_28_n110, M2_mult_28_n109,
         M2_mult_28_n108, M2_mult_28_n106, M2_mult_28_n104, M2_mult_28_n103,
         M2_mult_28_n102, M2_mult_28_n101, M2_mult_28_n100, M2_mult_28_n99,
         M2_mult_28_n95, M2_mult_28_n94, M2_mult_28_n93, M2_mult_28_n79,
         M2_mult_28_n78, M2_mult_28_n77, M2_mult_28_n76, M2_mult_28_n75,
         M2_mult_28_n74, M2_mult_28_n73, M2_mult_28_n72, M2_mult_28_n71,
         M2_mult_28_n70, M2_mult_28_n69, M2_mult_28_n68, M2_mult_28_n67,
         M2_mult_28_n66, M2_mult_28_n65, M2_mult_28_n64, M2_mult_28_n63,
         M2_mult_28_n62, M2_mult_28_n61, M2_mult_28_n60, M2_mult_28_n59,
         M2_mult_28_n58, M2_mult_28_n57, M2_mult_28_n56, M2_mult_28_n55,
         M2_mult_28_n54, M2_mult_28_n53, M2_mult_28_n52, M2_mult_28_n51,
         M2_mult_28_n50, M2_mult_28_n49, M2_mult_28_n48, M2_mult_28_n46,
         M2_mult_28_n45, M2_mult_28_n44, M2_mult_28_n43, M2_mult_28_n42,
         M2_mult_28_n41, M2_mult_28_n40, M2_mult_28_n39, M2_mult_28_n38,
         M2_mult_28_n37, M2_mult_28_n36, M2_mult_28_n35, M2_mult_28_n34,
         M2_mult_28_n32, M2_mult_28_n31, M2_mult_28_n30, M2_mult_28_n29,
         M2_mult_28_n28, M2_mult_28_n27, M2_mult_28_n26, M2_mult_28_n25,
         M2_mult_28_n24, M2_mult_28_n22, M2_mult_28_n21, M2_mult_28_n20,
         M2_mult_28_n19, M2_mult_28_n18, M2_mult_28_n9, M2_mult_28_n8,
         M2_mult_28_n7, M2_mult_28_n6, M2_mult_28_n5, M2_mult_28_n4,
         M2_mult_28_n3, M2_mult_28_n2;
  wire   [8:0] reg_in_out;
  wire   [8:0] A1_out;
  wire   [8:0] reg_feed_out;
  wire   [8:0] M2_out;
  wire   [8:0] M1_out;
  wire   [8:0] A2_out;
  wire   [9:1] A1_add_25_carry;
  wire   [9:1] A2_add_25_carry;

  INV_X1 U2 ( .A(rst_n), .ZN(n2) );
  NAND2_X1 REG_IN_U19 ( .A1(x_n[8]), .A2(v_in), .ZN(REG_IN_n9) );
  OAI21_X1 REG_IN_U18 ( .B1(v_in), .B2(REG_IN_n18), .A(REG_IN_n9), .ZN(
        REG_IN_n27) );
  NAND2_X1 REG_IN_U17 ( .A1(x_n[7]), .A2(v_in), .ZN(REG_IN_n8) );
  OAI21_X1 REG_IN_U16 ( .B1(v_in), .B2(REG_IN_n17), .A(REG_IN_n8), .ZN(
        REG_IN_n26) );
  NAND2_X1 REG_IN_U15 ( .A1(x_n[6]), .A2(v_in), .ZN(REG_IN_n7) );
  OAI21_X1 REG_IN_U14 ( .B1(v_in), .B2(REG_IN_n16), .A(REG_IN_n7), .ZN(
        REG_IN_n25) );
  NAND2_X1 REG_IN_U13 ( .A1(x_n[5]), .A2(v_in), .ZN(REG_IN_n6) );
  OAI21_X1 REG_IN_U12 ( .B1(v_in), .B2(REG_IN_n15), .A(REG_IN_n6), .ZN(
        REG_IN_n24) );
  NAND2_X1 REG_IN_U11 ( .A1(x_n[4]), .A2(v_in), .ZN(REG_IN_n5) );
  OAI21_X1 REG_IN_U10 ( .B1(v_in), .B2(REG_IN_n14), .A(REG_IN_n5), .ZN(
        REG_IN_n23) );
  NAND2_X1 REG_IN_U9 ( .A1(x_n[3]), .A2(v_in), .ZN(REG_IN_n4) );
  OAI21_X1 REG_IN_U8 ( .B1(v_in), .B2(REG_IN_n13), .A(REG_IN_n4), .ZN(
        REG_IN_n22) );
  NAND2_X1 REG_IN_U7 ( .A1(x_n[2]), .A2(v_in), .ZN(REG_IN_n3) );
  OAI21_X1 REG_IN_U6 ( .B1(v_in), .B2(REG_IN_n12), .A(REG_IN_n3), .ZN(
        REG_IN_n21) );
  NAND2_X1 REG_IN_U5 ( .A1(x_n[1]), .A2(v_in), .ZN(REG_IN_n2) );
  OAI21_X1 REG_IN_U4 ( .B1(v_in), .B2(REG_IN_n11), .A(REG_IN_n2), .ZN(
        REG_IN_n20) );
  NAND2_X1 REG_IN_U3 ( .A1(v_in), .A2(x_n[0]), .ZN(REG_IN_n1) );
  OAI21_X1 REG_IN_U2 ( .B1(v_in), .B2(REG_IN_n10), .A(REG_IN_n1), .ZN(
        REG_IN_n19) );
  DFF_X1 REG_IN_d_out_reg_0_ ( .D(REG_IN_n19), .CK(clk), .Q(reg_in_out[0]), 
        .QN(REG_IN_n10) );
  DFF_X1 REG_IN_d_out_reg_1_ ( .D(REG_IN_n20), .CK(clk), .Q(reg_in_out[1]), 
        .QN(REG_IN_n11) );
  DFF_X1 REG_IN_d_out_reg_2_ ( .D(REG_IN_n21), .CK(clk), .Q(reg_in_out[2]), 
        .QN(REG_IN_n12) );
  DFF_X1 REG_IN_d_out_reg_3_ ( .D(REG_IN_n22), .CK(clk), .Q(reg_in_out[3]), 
        .QN(REG_IN_n13) );
  DFF_X1 REG_IN_d_out_reg_4_ ( .D(REG_IN_n23), .CK(clk), .Q(reg_in_out[4]), 
        .QN(REG_IN_n14) );
  DFF_X1 REG_IN_d_out_reg_5_ ( .D(REG_IN_n24), .CK(clk), .Q(reg_in_out[5]), 
        .QN(REG_IN_n15) );
  DFF_X1 REG_IN_d_out_reg_6_ ( .D(REG_IN_n25), .CK(clk), .Q(reg_in_out[6]), 
        .QN(REG_IN_n16) );
  DFF_X1 REG_IN_d_out_reg_7_ ( .D(REG_IN_n26), .CK(clk), .Q(reg_in_out[7]), 
        .QN(REG_IN_n17) );
  DFF_X1 REG_IN_d_out_reg_8_ ( .D(REG_IN_n27), .CK(clk), .Q(reg_in_out[8]), 
        .QN(REG_IN_n18) );
  BUF_X1 REG_FEED_U22 ( .A(reg_feed_en), .Z(REG_FEED_n29) );
  BUF_X1 REG_FEED_U21 ( .A(reg_feed_en), .Z(REG_FEED_n27) );
  NAND2_X1 REG_FEED_U20 ( .A1(REG_FEED_n29), .A2(A1_out[0]), .ZN(REG_FEED_n1)
         );
  OAI21_X1 REG_FEED_U19 ( .B1(REG_FEED_n27), .B2(REG_FEED_n10), .A(REG_FEED_n1), .ZN(REG_FEED_n19) );
  NAND2_X1 REG_FEED_U18 ( .A1(A1_out[8]), .A2(REG_FEED_n27), .ZN(REG_FEED_n9)
         );
  OAI21_X1 REG_FEED_U17 ( .B1(REG_FEED_n27), .B2(REG_FEED_n18), .A(REG_FEED_n9), .ZN(REG_FEED_n28) );
  NAND2_X1 REG_FEED_U16 ( .A1(A1_out[7]), .A2(REG_FEED_n27), .ZN(REG_FEED_n8)
         );
  OAI21_X1 REG_FEED_U15 ( .B1(REG_FEED_n27), .B2(REG_FEED_n17), .A(REG_FEED_n8), .ZN(REG_FEED_n26) );
  NAND2_X1 REG_FEED_U14 ( .A1(A1_out[6]), .A2(REG_FEED_n27), .ZN(REG_FEED_n7)
         );
  OAI21_X1 REG_FEED_U13 ( .B1(REG_FEED_n27), .B2(REG_FEED_n16), .A(REG_FEED_n7), .ZN(REG_FEED_n25) );
  NAND2_X1 REG_FEED_U12 ( .A1(A1_out[5]), .A2(REG_FEED_n27), .ZN(REG_FEED_n6)
         );
  OAI21_X1 REG_FEED_U11 ( .B1(REG_FEED_n29), .B2(REG_FEED_n15), .A(REG_FEED_n6), .ZN(REG_FEED_n24) );
  NAND2_X1 REG_FEED_U10 ( .A1(A1_out[4]), .A2(REG_FEED_n27), .ZN(REG_FEED_n5)
         );
  OAI21_X1 REG_FEED_U9 ( .B1(REG_FEED_n29), .B2(REG_FEED_n14), .A(REG_FEED_n5), 
        .ZN(REG_FEED_n23) );
  NAND2_X1 REG_FEED_U8 ( .A1(A1_out[3]), .A2(REG_FEED_n27), .ZN(REG_FEED_n4)
         );
  OAI21_X1 REG_FEED_U7 ( .B1(REG_FEED_n29), .B2(REG_FEED_n13), .A(REG_FEED_n4), 
        .ZN(REG_FEED_n22) );
  NAND2_X1 REG_FEED_U6 ( .A1(A1_out[2]), .A2(REG_FEED_n27), .ZN(REG_FEED_n3)
         );
  OAI21_X1 REG_FEED_U5 ( .B1(REG_FEED_n29), .B2(REG_FEED_n12), .A(REG_FEED_n3), 
        .ZN(REG_FEED_n21) );
  NAND2_X1 REG_FEED_U4 ( .A1(A1_out[1]), .A2(REG_FEED_n27), .ZN(REG_FEED_n2)
         );
  OAI21_X1 REG_FEED_U3 ( .B1(REG_FEED_n29), .B2(REG_FEED_n11), .A(REG_FEED_n2), 
        .ZN(REG_FEED_n20) );
  INV_X1 REG_FEED_U2 ( .A(n2), .ZN(REG_FEED_n30) );
  DFFR_X1 REG_FEED_d_out_reg_0_ ( .D(REG_FEED_n19), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[0]), .QN(REG_FEED_n10) );
  DFFR_X1 REG_FEED_d_out_reg_1_ ( .D(REG_FEED_n20), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[1]), .QN(REG_FEED_n11) );
  DFFR_X1 REG_FEED_d_out_reg_2_ ( .D(REG_FEED_n21), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[2]), .QN(REG_FEED_n12) );
  DFFR_X1 REG_FEED_d_out_reg_3_ ( .D(REG_FEED_n22), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[3]), .QN(REG_FEED_n13) );
  DFFR_X1 REG_FEED_d_out_reg_4_ ( .D(REG_FEED_n23), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[4]), .QN(REG_FEED_n14) );
  DFFR_X1 REG_FEED_d_out_reg_5_ ( .D(REG_FEED_n24), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[5]), .QN(REG_FEED_n15) );
  DFFR_X1 REG_FEED_d_out_reg_6_ ( .D(REG_FEED_n25), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[6]), .QN(REG_FEED_n16) );
  DFFR_X1 REG_FEED_d_out_reg_7_ ( .D(REG_FEED_n26), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[7]), .QN(REG_FEED_n17) );
  DFFR_X1 REG_FEED_d_out_reg_8_ ( .D(REG_FEED_n28), .CK(clk), .RN(REG_FEED_n30), .Q(reg_feed_out[8]), .QN(REG_FEED_n18) );
  BUF_X1 REG_OUT_U21 ( .A(reg_feed_en), .Z(REG_OUT_n29) );
  BUF_X1 REG_OUT_U20 ( .A(reg_feed_en), .Z(REG_OUT_n28) );
  NAND2_X1 REG_OUT_U19 ( .A1(REG_OUT_n29), .A2(M2_out[0]), .ZN(REG_OUT_n56) );
  OAI21_X1 REG_OUT_U18 ( .B1(REG_OUT_n28), .B2(REG_OUT_n47), .A(REG_OUT_n56), 
        .ZN(REG_OUT_n38) );
  NAND2_X1 REG_OUT_U17 ( .A1(M2_out[4]), .A2(REG_OUT_n28), .ZN(REG_OUT_n52) );
  OAI21_X1 REG_OUT_U16 ( .B1(REG_OUT_n29), .B2(REG_OUT_n43), .A(REG_OUT_n52), 
        .ZN(REG_OUT_n34) );
  NAND2_X1 REG_OUT_U15 ( .A1(M2_out[3]), .A2(REG_OUT_n28), .ZN(REG_OUT_n53) );
  OAI21_X1 REG_OUT_U14 ( .B1(REG_OUT_n29), .B2(REG_OUT_n44), .A(REG_OUT_n53), 
        .ZN(REG_OUT_n35) );
  NAND2_X1 REG_OUT_U13 ( .A1(M2_out[8]), .A2(REG_OUT_n28), .ZN(REG_OUT_n48) );
  OAI21_X1 REG_OUT_U12 ( .B1(REG_OUT_n28), .B2(REG_OUT_n39), .A(REG_OUT_n48), 
        .ZN(REG_OUT_n30) );
  NAND2_X1 REG_OUT_U11 ( .A1(M2_out[7]), .A2(REG_OUT_n28), .ZN(REG_OUT_n49) );
  OAI21_X1 REG_OUT_U10 ( .B1(REG_OUT_n28), .B2(REG_OUT_n40), .A(REG_OUT_n49), 
        .ZN(REG_OUT_n31) );
  NAND2_X1 REG_OUT_U9 ( .A1(M2_out[6]), .A2(REG_OUT_n28), .ZN(REG_OUT_n50) );
  OAI21_X1 REG_OUT_U8 ( .B1(REG_OUT_n28), .B2(REG_OUT_n41), .A(REG_OUT_n50), 
        .ZN(REG_OUT_n32) );
  NAND2_X1 REG_OUT_U7 ( .A1(M2_out[5]), .A2(REG_OUT_n28), .ZN(REG_OUT_n51) );
  OAI21_X1 REG_OUT_U6 ( .B1(REG_OUT_n29), .B2(REG_OUT_n42), .A(REG_OUT_n51), 
        .ZN(REG_OUT_n33) );
  NAND2_X1 REG_OUT_U5 ( .A1(M2_out[2]), .A2(REG_OUT_n28), .ZN(REG_OUT_n54) );
  OAI21_X1 REG_OUT_U4 ( .B1(REG_OUT_n29), .B2(REG_OUT_n45), .A(REG_OUT_n54), 
        .ZN(REG_OUT_n36) );
  NAND2_X1 REG_OUT_U3 ( .A1(M2_out[1]), .A2(REG_OUT_n28), .ZN(REG_OUT_n55) );
  OAI21_X1 REG_OUT_U2 ( .B1(REG_OUT_n29), .B2(REG_OUT_n46), .A(REG_OUT_n55), 
        .ZN(REG_OUT_n37) );
  DFF_X1 REG_OUT_d_out_reg_0_ ( .D(REG_OUT_n38), .CK(clk), .Q(y_n[0]), .QN(
        REG_OUT_n47) );
  DFF_X1 REG_OUT_d_out_reg_1_ ( .D(REG_OUT_n37), .CK(clk), .Q(y_n[1]), .QN(
        REG_OUT_n46) );
  DFF_X1 REG_OUT_d_out_reg_2_ ( .D(REG_OUT_n36), .CK(clk), .Q(y_n[2]), .QN(
        REG_OUT_n45) );
  DFF_X1 REG_OUT_d_out_reg_3_ ( .D(REG_OUT_n35), .CK(clk), .Q(y_n[3]), .QN(
        REG_OUT_n44) );
  DFF_X1 REG_OUT_d_out_reg_4_ ( .D(REG_OUT_n34), .CK(clk), .Q(y_n[4]), .QN(
        REG_OUT_n43) );
  DFF_X1 REG_OUT_d_out_reg_5_ ( .D(REG_OUT_n33), .CK(clk), .Q(y_n[5]), .QN(
        REG_OUT_n42) );
  DFF_X1 REG_OUT_d_out_reg_6_ ( .D(REG_OUT_n32), .CK(clk), .Q(y_n[6]), .QN(
        REG_OUT_n41) );
  DFF_X1 REG_OUT_d_out_reg_7_ ( .D(REG_OUT_n31), .CK(clk), .Q(y_n[7]), .QN(
        REG_OUT_n40) );
  DFF_X1 REG_OUT_d_out_reg_8_ ( .D(REG_OUT_n30), .CK(clk), .Q(y_n[8]), .QN(
        REG_OUT_n39) );
  INV_X1 DELAY_FEED_U3 ( .A(n2), .ZN(DELAY_FEED_n1) );
  DFFR_X1 DELAY_FEED_d_out_reg ( .D(v_in), .CK(clk), .RN(DELAY_FEED_n1), .Q(
        reg_feed_en) );
  INV_X1 DELAY_VOUT_U3 ( .A(n2), .ZN(DELAY_VOUT_n1) );
  DFFR_X1 DELAY_VOUT_d_out_reg ( .D(reg_feed_en), .CK(clk), .RN(DELAY_VOUT_n1), 
        .Q(v_out) );
  AND2_X1 A1_add_25_U1 ( .A1(M1_out[0]), .A2(reg_in_out[0]), .ZN(
        A1_add_25_carry[1]) );
  FA_X1 A1_add_25_U1_1 ( .A(reg_in_out[1]), .B(M1_out[1]), .CI(
        A1_add_25_carry[1]), .CO(A1_add_25_carry[2]), .S(A1_out[0]) );
  FA_X1 A1_add_25_U1_2 ( .A(reg_in_out[2]), .B(M1_out[2]), .CI(
        A1_add_25_carry[2]), .CO(A1_add_25_carry[3]), .S(A1_out[1]) );
  FA_X1 A1_add_25_U1_3 ( .A(reg_in_out[3]), .B(M1_out[3]), .CI(
        A1_add_25_carry[3]), .CO(A1_add_25_carry[4]), .S(A1_out[2]) );
  FA_X1 A1_add_25_U1_4 ( .A(reg_in_out[4]), .B(M1_out[4]), .CI(
        A1_add_25_carry[4]), .CO(A1_add_25_carry[5]), .S(A1_out[3]) );
  FA_X1 A1_add_25_U1_5 ( .A(reg_in_out[5]), .B(M1_out[5]), .CI(
        A1_add_25_carry[5]), .CO(A1_add_25_carry[6]), .S(A1_out[4]) );
  FA_X1 A1_add_25_U1_6 ( .A(reg_in_out[6]), .B(M1_out[6]), .CI(
        A1_add_25_carry[6]), .CO(A1_add_25_carry[7]), .S(A1_out[5]) );
  FA_X1 A1_add_25_U1_7 ( .A(reg_in_out[7]), .B(M1_out[7]), .CI(
        A1_add_25_carry[7]), .CO(A1_add_25_carry[8]), .S(A1_out[6]) );
  FA_X1 A1_add_25_U1_8 ( .A(reg_in_out[8]), .B(M1_out[8]), .CI(
        A1_add_25_carry[8]), .CO(A1_add_25_carry[9]), .S(A1_out[7]) );
  FA_X1 A1_add_25_U1_9 ( .A(reg_in_out[8]), .B(M1_out[8]), .CI(
        A1_add_25_carry[9]), .S(A1_out[8]) );
  AND2_X1 A2_add_25_U1 ( .A1(reg_feed_out[0]), .A2(A1_out[0]), .ZN(
        A2_add_25_carry[1]) );
  FA_X1 A2_add_25_U1_1 ( .A(A1_out[1]), .B(reg_feed_out[1]), .CI(
        A2_add_25_carry[1]), .CO(A2_add_25_carry[2]), .S(A2_out[0]) );
  FA_X1 A2_add_25_U1_2 ( .A(A1_out[2]), .B(reg_feed_out[2]), .CI(
        A2_add_25_carry[2]), .CO(A2_add_25_carry[3]), .S(A2_out[1]) );
  FA_X1 A2_add_25_U1_3 ( .A(A1_out[3]), .B(reg_feed_out[3]), .CI(
        A2_add_25_carry[3]), .CO(A2_add_25_carry[4]), .S(A2_out[2]) );
  FA_X1 A2_add_25_U1_4 ( .A(A1_out[4]), .B(reg_feed_out[4]), .CI(
        A2_add_25_carry[4]), .CO(A2_add_25_carry[5]), .S(A2_out[3]) );
  FA_X1 A2_add_25_U1_5 ( .A(A1_out[5]), .B(reg_feed_out[5]), .CI(
        A2_add_25_carry[5]), .CO(A2_add_25_carry[6]), .S(A2_out[4]) );
  FA_X1 A2_add_25_U1_6 ( .A(A1_out[6]), .B(reg_feed_out[6]), .CI(
        A2_add_25_carry[6]), .CO(A2_add_25_carry[7]), .S(A2_out[5]) );
  FA_X1 A2_add_25_U1_7 ( .A(A1_out[7]), .B(reg_feed_out[7]), .CI(
        A2_add_25_carry[7]), .CO(A2_add_25_carry[8]), .S(A2_out[6]) );
  FA_X1 A2_add_25_U1_8 ( .A(A1_out[8]), .B(reg_feed_out[8]), .CI(
        A2_add_25_carry[8]), .CO(A2_add_25_carry[9]), .S(A2_out[7]) );
  FA_X1 A2_add_25_U1_9 ( .A(A1_out[8]), .B(reg_feed_out[8]), .CI(
        A2_add_25_carry[9]), .S(A2_out[8]) );
  XOR2_X1 M1_mult_28_U351 ( .A(reg_feed_out[2]), .B(reg_feed_out[1]), .Z(
        M1_mult_28_n360) );
  NAND2_X1 M1_mult_28_U350 ( .A1(reg_feed_out[1]), .A2(M1_mult_28_n289), .ZN(
        M1_mult_28_n316) );
  XNOR2_X1 M1_mult_28_U349 ( .A(coeff_a[2]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n315) );
  OAI22_X1 M1_mult_28_U348 ( .A1(coeff_a[1]), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n315), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n362) );
  XNOR2_X1 M1_mult_28_U347 ( .A(M1_mult_28_n286), .B(reg_feed_out[2]), .ZN(
        M1_mult_28_n361) );
  NAND2_X1 M1_mult_28_U346 ( .A1(M1_mult_28_n287), .A2(M1_mult_28_n361), .ZN(
        M1_mult_28_n309) );
  NAND3_X1 M1_mult_28_U345 ( .A1(M1_mult_28_n360), .A2(M1_mult_28_n291), .A3(
        reg_feed_out[3]), .ZN(M1_mult_28_n359) );
  OAI21_X1 M1_mult_28_U344 ( .B1(M1_mult_28_n286), .B2(M1_mult_28_n309), .A(
        M1_mult_28_n359), .ZN(M1_mult_28_n358) );
  AOI222_X1 M1_mult_28_U343 ( .A1(M1_mult_28_n268), .A2(M1_mult_28_n79), .B1(
        M1_mult_28_n358), .B2(M1_mult_28_n268), .C1(M1_mult_28_n358), .C2(
        M1_mult_28_n79), .ZN(M1_mult_28_n357) );
  AOI222_X1 M1_mult_28_U342 ( .A1(M1_mult_28_n283), .A2(M1_mult_28_n77), .B1(
        M1_mult_28_n283), .B2(M1_mult_28_n78), .C1(M1_mult_28_n78), .C2(
        M1_mult_28_n77), .ZN(M1_mult_28_n356) );
  AOI222_X1 M1_mult_28_U341 ( .A1(M1_mult_28_n282), .A2(M1_mult_28_n73), .B1(
        M1_mult_28_n282), .B2(M1_mult_28_n76), .C1(M1_mult_28_n76), .C2(
        M1_mult_28_n73), .ZN(M1_mult_28_n355) );
  AOI222_X1 M1_mult_28_U340 ( .A1(M1_mult_28_n278), .A2(M1_mult_28_n69), .B1(
        M1_mult_28_n278), .B2(M1_mult_28_n72), .C1(M1_mult_28_n72), .C2(
        M1_mult_28_n69), .ZN(M1_mult_28_n354) );
  AOI222_X1 M1_mult_28_U339 ( .A1(M1_mult_28_n277), .A2(M1_mult_28_n63), .B1(
        M1_mult_28_n277), .B2(M1_mult_28_n68), .C1(M1_mult_28_n68), .C2(
        M1_mult_28_n63), .ZN(M1_mult_28_n353) );
  XOR2_X1 M1_mult_28_U338 ( .A(reg_feed_out[8]), .B(M1_mult_28_n276), .Z(
        M1_mult_28_n295) );
  XNOR2_X1 M1_mult_28_U337 ( .A(coeff_a[6]), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n352) );
  NOR2_X1 M1_mult_28_U336 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n352), .ZN(
        M1_mult_28_n100) );
  XNOR2_X1 M1_mult_28_U335 ( .A(coeff_a[5]), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n351) );
  NOR2_X1 M1_mult_28_U334 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n351), .ZN(
        M1_mult_28_n101) );
  XNOR2_X1 M1_mult_28_U333 ( .A(coeff_a[4]), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n350) );
  NOR2_X1 M1_mult_28_U332 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n350), .ZN(
        M1_mult_28_n102) );
  XNOR2_X1 M1_mult_28_U331 ( .A(coeff_a[3]), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n349) );
  NOR2_X1 M1_mult_28_U330 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n349), .ZN(
        M1_mult_28_n103) );
  XNOR2_X1 M1_mult_28_U329 ( .A(coeff_a[2]), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n348) );
  NOR2_X1 M1_mult_28_U328 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n348), .ZN(
        M1_mult_28_n104) );
  NOR2_X1 M1_mult_28_U327 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n291), .ZN(
        M1_mult_28_n106) );
  XNOR2_X1 M1_mult_28_U326 ( .A(coeff_a[8]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n314) );
  XNOR2_X1 M1_mult_28_U325 ( .A(M1_mult_28_n276), .B(reg_feed_out[6]), .ZN(
        M1_mult_28_n347) );
  NAND2_X1 M1_mult_28_U324 ( .A1(M1_mult_28_n302), .A2(M1_mult_28_n347), .ZN(
        M1_mult_28_n300) );
  OAI22_X1 M1_mult_28_U323 ( .A1(M1_mult_28_n314), .A2(M1_mult_28_n302), .B1(
        M1_mult_28_n300), .B2(M1_mult_28_n314), .ZN(M1_mult_28_n346) );
  XNOR2_X1 M1_mult_28_U322 ( .A(coeff_a[6]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n345) );
  XNOR2_X1 M1_mult_28_U321 ( .A(coeff_a[7]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n313) );
  OAI22_X1 M1_mult_28_U320 ( .A1(M1_mult_28_n345), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n313), .ZN(M1_mult_28_n108) );
  XNOR2_X1 M1_mult_28_U319 ( .A(coeff_a[5]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n344) );
  OAI22_X1 M1_mult_28_U318 ( .A1(M1_mult_28_n344), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n345), .ZN(M1_mult_28_n109) );
  XNOR2_X1 M1_mult_28_U317 ( .A(coeff_a[4]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n343) );
  OAI22_X1 M1_mult_28_U316 ( .A1(M1_mult_28_n343), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n344), .ZN(M1_mult_28_n110) );
  XNOR2_X1 M1_mult_28_U315 ( .A(coeff_a[3]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n307) );
  OAI22_X1 M1_mult_28_U314 ( .A1(M1_mult_28_n307), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n343), .ZN(M1_mult_28_n111) );
  XNOR2_X1 M1_mult_28_U313 ( .A(coeff_a[1]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n342) );
  XNOR2_X1 M1_mult_28_U312 ( .A(coeff_a[2]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n306) );
  OAI22_X1 M1_mult_28_U311 ( .A1(M1_mult_28_n342), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n306), .ZN(M1_mult_28_n113) );
  XNOR2_X1 M1_mult_28_U310 ( .A(coeff_a[0]), .B(reg_feed_out[7]), .ZN(
        M1_mult_28_n341) );
  OAI22_X1 M1_mult_28_U309 ( .A1(M1_mult_28_n341), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n342), .ZN(M1_mult_28_n114) );
  NOR2_X1 M1_mult_28_U308 ( .A1(M1_mult_28_n302), .A2(M1_mult_28_n291), .ZN(
        M1_mult_28_n115) );
  XNOR2_X1 M1_mult_28_U307 ( .A(coeff_a[8]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n312) );
  XNOR2_X1 M1_mult_28_U306 ( .A(M1_mult_28_n281), .B(reg_feed_out[4]), .ZN(
        M1_mult_28_n340) );
  NAND2_X1 M1_mult_28_U305 ( .A1(M1_mult_28_n299), .A2(M1_mult_28_n340), .ZN(
        M1_mult_28_n297) );
  OAI22_X1 M1_mult_28_U304 ( .A1(M1_mult_28_n312), .A2(M1_mult_28_n299), .B1(
        M1_mult_28_n297), .B2(M1_mult_28_n312), .ZN(M1_mult_28_n339) );
  XNOR2_X1 M1_mult_28_U303 ( .A(coeff_a[6]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n338) );
  XNOR2_X1 M1_mult_28_U302 ( .A(coeff_a[7]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n311) );
  OAI22_X1 M1_mult_28_U301 ( .A1(M1_mult_28_n338), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n311), .ZN(M1_mult_28_n117) );
  XNOR2_X1 M1_mult_28_U300 ( .A(coeff_a[5]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n337) );
  OAI22_X1 M1_mult_28_U299 ( .A1(M1_mult_28_n337), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n338), .ZN(M1_mult_28_n118) );
  XNOR2_X1 M1_mult_28_U298 ( .A(coeff_a[4]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n336) );
  OAI22_X1 M1_mult_28_U297 ( .A1(M1_mult_28_n336), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n337), .ZN(M1_mult_28_n119) );
  XNOR2_X1 M1_mult_28_U296 ( .A(coeff_a[3]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n335) );
  OAI22_X1 M1_mult_28_U295 ( .A1(M1_mult_28_n335), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n336), .ZN(M1_mult_28_n120) );
  XNOR2_X1 M1_mult_28_U294 ( .A(coeff_a[2]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n334) );
  OAI22_X1 M1_mult_28_U293 ( .A1(M1_mult_28_n334), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n335), .ZN(M1_mult_28_n121) );
  XNOR2_X1 M1_mult_28_U292 ( .A(coeff_a[1]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n333) );
  OAI22_X1 M1_mult_28_U291 ( .A1(M1_mult_28_n333), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n334), .ZN(M1_mult_28_n122) );
  XNOR2_X1 M1_mult_28_U290 ( .A(coeff_a[0]), .B(reg_feed_out[5]), .ZN(
        M1_mult_28_n332) );
  OAI22_X1 M1_mult_28_U289 ( .A1(M1_mult_28_n332), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n333), .ZN(M1_mult_28_n123) );
  NOR2_X1 M1_mult_28_U288 ( .A1(M1_mult_28_n299), .A2(M1_mult_28_n291), .ZN(
        M1_mult_28_n124) );
  XOR2_X1 M1_mult_28_U287 ( .A(coeff_a[8]), .B(M1_mult_28_n286), .Z(
        M1_mult_28_n310) );
  OAI22_X1 M1_mult_28_U286 ( .A1(M1_mult_28_n310), .A2(M1_mult_28_n287), .B1(
        M1_mult_28_n309), .B2(M1_mult_28_n310), .ZN(M1_mult_28_n331) );
  XNOR2_X1 M1_mult_28_U285 ( .A(coeff_a[6]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n330) );
  XNOR2_X1 M1_mult_28_U284 ( .A(coeff_a[7]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n308) );
  OAI22_X1 M1_mult_28_U283 ( .A1(M1_mult_28_n330), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n308), .ZN(M1_mult_28_n126) );
  XNOR2_X1 M1_mult_28_U282 ( .A(coeff_a[5]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n329) );
  OAI22_X1 M1_mult_28_U281 ( .A1(M1_mult_28_n329), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n330), .ZN(M1_mult_28_n127) );
  XNOR2_X1 M1_mult_28_U280 ( .A(coeff_a[4]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n328) );
  OAI22_X1 M1_mult_28_U279 ( .A1(M1_mult_28_n328), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n329), .ZN(M1_mult_28_n128) );
  XNOR2_X1 M1_mult_28_U278 ( .A(coeff_a[3]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n327) );
  OAI22_X1 M1_mult_28_U277 ( .A1(M1_mult_28_n327), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n328), .ZN(M1_mult_28_n129) );
  XNOR2_X1 M1_mult_28_U276 ( .A(coeff_a[2]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n326) );
  OAI22_X1 M1_mult_28_U275 ( .A1(M1_mult_28_n326), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n327), .ZN(M1_mult_28_n130) );
  XNOR2_X1 M1_mult_28_U274 ( .A(coeff_a[1]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n325) );
  OAI22_X1 M1_mult_28_U273 ( .A1(M1_mult_28_n325), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n326), .ZN(M1_mult_28_n131) );
  XNOR2_X1 M1_mult_28_U272 ( .A(coeff_a[0]), .B(reg_feed_out[3]), .ZN(
        M1_mult_28_n324) );
  OAI22_X1 M1_mult_28_U271 ( .A1(M1_mult_28_n324), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n325), .ZN(M1_mult_28_n132) );
  XNOR2_X1 M1_mult_28_U270 ( .A(coeff_a[8]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n322) );
  OAI22_X1 M1_mult_28_U269 ( .A1(M1_mult_28_n289), .A2(M1_mult_28_n322), .B1(
        M1_mult_28_n316), .B2(M1_mult_28_n322), .ZN(M1_mult_28_n323) );
  XNOR2_X1 M1_mult_28_U268 ( .A(coeff_a[7]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n321) );
  OAI22_X1 M1_mult_28_U267 ( .A1(M1_mult_28_n321), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n322), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n135) );
  XNOR2_X1 M1_mult_28_U266 ( .A(coeff_a[6]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n320) );
  OAI22_X1 M1_mult_28_U265 ( .A1(M1_mult_28_n320), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n321), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n136) );
  XNOR2_X1 M1_mult_28_U264 ( .A(coeff_a[5]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n319) );
  OAI22_X1 M1_mult_28_U263 ( .A1(M1_mult_28_n319), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n320), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n137) );
  XNOR2_X1 M1_mult_28_U262 ( .A(coeff_a[4]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n318) );
  OAI22_X1 M1_mult_28_U261 ( .A1(M1_mult_28_n318), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n319), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n138) );
  XNOR2_X1 M1_mult_28_U260 ( .A(coeff_a[3]), .B(reg_feed_out[1]), .ZN(
        M1_mult_28_n317) );
  OAI22_X1 M1_mult_28_U259 ( .A1(M1_mult_28_n317), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n318), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n139) );
  OAI22_X1 M1_mult_28_U258 ( .A1(M1_mult_28_n315), .A2(M1_mult_28_n316), .B1(
        M1_mult_28_n317), .B2(M1_mult_28_n289), .ZN(M1_mult_28_n140) );
  OAI22_X1 M1_mult_28_U257 ( .A1(M1_mult_28_n313), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n314), .ZN(M1_mult_28_n22) );
  OAI22_X1 M1_mult_28_U256 ( .A1(M1_mult_28_n311), .A2(M1_mult_28_n297), .B1(
        M1_mult_28_n299), .B2(M1_mult_28_n312), .ZN(M1_mult_28_n32) );
  OAI22_X1 M1_mult_28_U255 ( .A1(M1_mult_28_n308), .A2(M1_mult_28_n309), .B1(
        M1_mult_28_n287), .B2(M1_mult_28_n310), .ZN(M1_mult_28_n46) );
  OAI22_X1 M1_mult_28_U254 ( .A1(M1_mult_28_n306), .A2(M1_mult_28_n300), .B1(
        M1_mult_28_n302), .B2(M1_mult_28_n307), .ZN(M1_mult_28_n305) );
  XNOR2_X1 M1_mult_28_U253 ( .A(M1_mult_28_n290), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n304) );
  NAND2_X1 M1_mult_28_U252 ( .A1(M1_mult_28_n304), .A2(M1_mult_28_n271), .ZN(
        M1_mult_28_n303) );
  NAND2_X1 M1_mult_28_U251 ( .A1(M1_mult_28_n273), .A2(M1_mult_28_n303), .ZN(
        M1_mult_28_n54) );
  XNOR2_X1 M1_mult_28_U250 ( .A(M1_mult_28_n303), .B(M1_mult_28_n273), .ZN(
        M1_mult_28_n55) );
  AND3_X1 M1_mult_28_U249 ( .A1(reg_feed_out[8]), .A2(M1_mult_28_n291), .A3(
        M1_mult_28_n271), .ZN(M1_mult_28_n93) );
  OR3_X1 M1_mult_28_U248 ( .A1(M1_mult_28_n302), .A2(coeff_a[0]), .A3(
        M1_mult_28_n276), .ZN(M1_mult_28_n301) );
  OAI21_X1 M1_mult_28_U247 ( .B1(M1_mult_28_n276), .B2(M1_mult_28_n300), .A(
        M1_mult_28_n301), .ZN(M1_mult_28_n94) );
  OR3_X1 M1_mult_28_U246 ( .A1(M1_mult_28_n299), .A2(coeff_a[0]), .A3(
        M1_mult_28_n281), .ZN(M1_mult_28_n298) );
  OAI21_X1 M1_mult_28_U245 ( .B1(M1_mult_28_n281), .B2(M1_mult_28_n297), .A(
        M1_mult_28_n298), .ZN(M1_mult_28_n95) );
  XNOR2_X1 M1_mult_28_U244 ( .A(coeff_a[7]), .B(reg_feed_out[8]), .ZN(
        M1_mult_28_n296) );
  NOR2_X1 M1_mult_28_U243 ( .A1(M1_mult_28_n295), .A2(M1_mult_28_n296), .ZN(
        M1_mult_28_n99) );
  XOR2_X1 M1_mult_28_U242 ( .A(coeff_a[8]), .B(reg_feed_out[8]), .Z(
        M1_mult_28_n294) );
  NAND2_X1 M1_mult_28_U241 ( .A1(M1_mult_28_n294), .A2(M1_mult_28_n271), .ZN(
        M1_mult_28_n292) );
  XOR2_X1 M1_mult_28_U240 ( .A(M1_mult_28_n2), .B(M1_mult_28_n18), .Z(
        M1_mult_28_n293) );
  XOR2_X1 M1_mult_28_U239 ( .A(M1_mult_28_n292), .B(M1_mult_28_n293), .Z(
        M1_out[8]) );
  INV_X1 M1_mult_28_U238 ( .A(M1_mult_28_n346), .ZN(M1_mult_28_n275) );
  INV_X1 M1_mult_28_U237 ( .A(M1_mult_28_n22), .ZN(M1_mult_28_n274) );
  INV_X1 M1_mult_28_U236 ( .A(coeff_a[1]), .ZN(M1_mult_28_n290) );
  AND3_X1 M1_mult_28_U235 ( .A1(M1_mult_28_n362), .A2(M1_mult_28_n290), .A3(
        reg_feed_out[1]), .ZN(M1_mult_28_n270) );
  AND2_X1 M1_mult_28_U234 ( .A1(M1_mult_28_n360), .A2(M1_mult_28_n362), .ZN(
        M1_mult_28_n269) );
  MUX2_X1 M1_mult_28_U233 ( .A(M1_mult_28_n269), .B(M1_mult_28_n270), .S(
        M1_mult_28_n291), .Z(M1_mult_28_n268) );
  INV_X1 M1_mult_28_U232 ( .A(coeff_a[0]), .ZN(M1_mult_28_n291) );
  INV_X1 M1_mult_28_U231 ( .A(M1_mult_28_n323), .ZN(M1_mult_28_n288) );
  INV_X1 M1_mult_28_U230 ( .A(reg_feed_out[7]), .ZN(M1_mult_28_n276) );
  INV_X1 M1_mult_28_U229 ( .A(reg_feed_out[5]), .ZN(M1_mult_28_n281) );
  INV_X1 M1_mult_28_U228 ( .A(reg_feed_out[3]), .ZN(M1_mult_28_n286) );
  INV_X1 M1_mult_28_U227 ( .A(reg_feed_out[0]), .ZN(M1_mult_28_n289) );
  XOR2_X1 M1_mult_28_U226 ( .A(reg_feed_out[6]), .B(M1_mult_28_n281), .Z(
        M1_mult_28_n302) );
  XOR2_X1 M1_mult_28_U225 ( .A(reg_feed_out[4]), .B(M1_mult_28_n286), .Z(
        M1_mult_28_n299) );
  INV_X1 M1_mult_28_U224 ( .A(M1_mult_28_n339), .ZN(M1_mult_28_n280) );
  INV_X1 M1_mult_28_U223 ( .A(M1_mult_28_n331), .ZN(M1_mult_28_n285) );
  INV_X1 M1_mult_28_U222 ( .A(M1_mult_28_n32), .ZN(M1_mult_28_n279) );
  INV_X1 M1_mult_28_U221 ( .A(M1_mult_28_n295), .ZN(M1_mult_28_n271) );
  INV_X1 M1_mult_28_U220 ( .A(M1_mult_28_n305), .ZN(M1_mult_28_n273) );
  INV_X1 M1_mult_28_U219 ( .A(M1_mult_28_n357), .ZN(M1_mult_28_n283) );
  INV_X1 M1_mult_28_U218 ( .A(M1_mult_28_n356), .ZN(M1_mult_28_n282) );
  INV_X1 M1_mult_28_U217 ( .A(M1_mult_28_n353), .ZN(M1_mult_28_n272) );
  INV_X1 M1_mult_28_U216 ( .A(M1_mult_28_n46), .ZN(M1_mult_28_n284) );
  INV_X1 M1_mult_28_U215 ( .A(M1_mult_28_n360), .ZN(M1_mult_28_n287) );
  INV_X1 M1_mult_28_U214 ( .A(M1_mult_28_n355), .ZN(M1_mult_28_n278) );
  INV_X1 M1_mult_28_U213 ( .A(M1_mult_28_n354), .ZN(M1_mult_28_n277) );
  HA_X1 M1_mult_28_U50 ( .A(M1_mult_28_n132), .B(M1_mult_28_n140), .CO(
        M1_mult_28_n78), .S(M1_mult_28_n79) );
  FA_X1 M1_mult_28_U49 ( .A(M1_mult_28_n139), .B(M1_mult_28_n124), .CI(
        M1_mult_28_n131), .CO(M1_mult_28_n76), .S(M1_mult_28_n77) );
  HA_X1 M1_mult_28_U48 ( .A(M1_mult_28_n95), .B(M1_mult_28_n123), .CO(
        M1_mult_28_n74), .S(M1_mult_28_n75) );
  FA_X1 M1_mult_28_U47 ( .A(M1_mult_28_n130), .B(M1_mult_28_n138), .CI(
        M1_mult_28_n75), .CO(M1_mult_28_n72), .S(M1_mult_28_n73) );
  FA_X1 M1_mult_28_U46 ( .A(M1_mult_28_n137), .B(M1_mult_28_n115), .CI(
        M1_mult_28_n129), .CO(M1_mult_28_n70), .S(M1_mult_28_n71) );
  FA_X1 M1_mult_28_U45 ( .A(M1_mult_28_n74), .B(M1_mult_28_n122), .CI(
        M1_mult_28_n71), .CO(M1_mult_28_n68), .S(M1_mult_28_n69) );
  HA_X1 M1_mult_28_U44 ( .A(M1_mult_28_n94), .B(M1_mult_28_n114), .CO(
        M1_mult_28_n66), .S(M1_mult_28_n67) );
  FA_X1 M1_mult_28_U43 ( .A(M1_mult_28_n121), .B(M1_mult_28_n136), .CI(
        M1_mult_28_n128), .CO(M1_mult_28_n64), .S(M1_mult_28_n65) );
  FA_X1 M1_mult_28_U42 ( .A(M1_mult_28_n70), .B(M1_mult_28_n67), .CI(
        M1_mult_28_n65), .CO(M1_mult_28_n62), .S(M1_mult_28_n63) );
  FA_X1 M1_mult_28_U41 ( .A(M1_mult_28_n120), .B(M1_mult_28_n106), .CI(
        M1_mult_28_n135), .CO(M1_mult_28_n60), .S(M1_mult_28_n61) );
  FA_X1 M1_mult_28_U40 ( .A(M1_mult_28_n113), .B(M1_mult_28_n127), .CI(
        M1_mult_28_n66), .CO(M1_mult_28_n58), .S(M1_mult_28_n59) );
  FA_X1 M1_mult_28_U39 ( .A(M1_mult_28_n61), .B(M1_mult_28_n64), .CI(
        M1_mult_28_n59), .CO(M1_mult_28_n56), .S(M1_mult_28_n57) );
  FA_X1 M1_mult_28_U36 ( .A(M1_mult_28_n93), .B(M1_mult_28_n119), .CI(
        M1_mult_28_n288), .CO(M1_mult_28_n52), .S(M1_mult_28_n53) );
  FA_X1 M1_mult_28_U35 ( .A(M1_mult_28_n55), .B(M1_mult_28_n126), .CI(
        M1_mult_28_n60), .CO(M1_mult_28_n50), .S(M1_mult_28_n51) );
  FA_X1 M1_mult_28_U34 ( .A(M1_mult_28_n53), .B(M1_mult_28_n58), .CI(
        M1_mult_28_n51), .CO(M1_mult_28_n48), .S(M1_mult_28_n49) );
  FA_X1 M1_mult_28_U32 ( .A(M1_mult_28_n111), .B(M1_mult_28_n104), .CI(
        M1_mult_28_n118), .CO(M1_mult_28_n44), .S(M1_mult_28_n45) );
  FA_X1 M1_mult_28_U31 ( .A(M1_mult_28_n54), .B(M1_mult_28_n284), .CI(
        M1_mult_28_n52), .CO(M1_mult_28_n42), .S(M1_mult_28_n43) );
  FA_X1 M1_mult_28_U30 ( .A(M1_mult_28_n50), .B(M1_mult_28_n45), .CI(
        M1_mult_28_n43), .CO(M1_mult_28_n40), .S(M1_mult_28_n41) );
  FA_X1 M1_mult_28_U29 ( .A(M1_mult_28_n110), .B(M1_mult_28_n103), .CI(
        M1_mult_28_n285), .CO(M1_mult_28_n38), .S(M1_mult_28_n39) );
  FA_X1 M1_mult_28_U28 ( .A(M1_mult_28_n46), .B(M1_mult_28_n117), .CI(
        M1_mult_28_n44), .CO(M1_mult_28_n36), .S(M1_mult_28_n37) );
  FA_X1 M1_mult_28_U27 ( .A(M1_mult_28_n42), .B(M1_mult_28_n39), .CI(
        M1_mult_28_n37), .CO(M1_mult_28_n34), .S(M1_mult_28_n35) );
  FA_X1 M1_mult_28_U25 ( .A(M1_mult_28_n102), .B(M1_mult_28_n109), .CI(
        M1_mult_28_n279), .CO(M1_mult_28_n30), .S(M1_mult_28_n31) );
  FA_X1 M1_mult_28_U24 ( .A(M1_mult_28_n31), .B(M1_mult_28_n38), .CI(
        M1_mult_28_n36), .CO(M1_mult_28_n28), .S(M1_mult_28_n29) );
  FA_X1 M1_mult_28_U23 ( .A(M1_mult_28_n108), .B(M1_mult_28_n32), .CI(
        M1_mult_28_n280), .CO(M1_mult_28_n26), .S(M1_mult_28_n27) );
  FA_X1 M1_mult_28_U22 ( .A(M1_mult_28_n30), .B(M1_mult_28_n101), .CI(
        M1_mult_28_n27), .CO(M1_mult_28_n24), .S(M1_mult_28_n25) );
  FA_X1 M1_mult_28_U20 ( .A(M1_mult_28_n274), .B(M1_mult_28_n100), .CI(
        M1_mult_28_n26), .CO(M1_mult_28_n20), .S(M1_mult_28_n21) );
  FA_X1 M1_mult_28_U19 ( .A(M1_mult_28_n99), .B(M1_mult_28_n22), .CI(
        M1_mult_28_n275), .CO(M1_mult_28_n18), .S(M1_mult_28_n19) );
  FA_X1 M1_mult_28_U10 ( .A(M1_mult_28_n57), .B(M1_mult_28_n62), .CI(
        M1_mult_28_n272), .CO(M1_mult_28_n9), .S(M1_out[0]) );
  FA_X1 M1_mult_28_U9 ( .A(M1_mult_28_n49), .B(M1_mult_28_n56), .CI(
        M1_mult_28_n9), .CO(M1_mult_28_n8), .S(M1_out[1]) );
  FA_X1 M1_mult_28_U8 ( .A(M1_mult_28_n41), .B(M1_mult_28_n48), .CI(
        M1_mult_28_n8), .CO(M1_mult_28_n7), .S(M1_out[2]) );
  FA_X1 M1_mult_28_U7 ( .A(M1_mult_28_n35), .B(M1_mult_28_n40), .CI(
        M1_mult_28_n7), .CO(M1_mult_28_n6), .S(M1_out[3]) );
  FA_X1 M1_mult_28_U6 ( .A(M1_mult_28_n29), .B(M1_mult_28_n34), .CI(
        M1_mult_28_n6), .CO(M1_mult_28_n5), .S(M1_out[4]) );
  FA_X1 M1_mult_28_U5 ( .A(M1_mult_28_n25), .B(M1_mult_28_n28), .CI(
        M1_mult_28_n5), .CO(M1_mult_28_n4), .S(M1_out[5]) );
  FA_X1 M1_mult_28_U4 ( .A(M1_mult_28_n21), .B(M1_mult_28_n24), .CI(
        M1_mult_28_n4), .CO(M1_mult_28_n3), .S(M1_out[6]) );
  FA_X1 M1_mult_28_U3 ( .A(M1_mult_28_n20), .B(M1_mult_28_n19), .CI(
        M1_mult_28_n3), .CO(M1_mult_28_n2), .S(M1_out[7]) );
  XOR2_X1 M2_mult_28_U351 ( .A(A2_out[2]), .B(A2_out[1]), .Z(M2_mult_28_n360)
         );
  NAND2_X1 M2_mult_28_U350 ( .A1(A2_out[1]), .A2(M2_mult_28_n289), .ZN(
        M2_mult_28_n316) );
  XNOR2_X1 M2_mult_28_U349 ( .A(coeff_b[2]), .B(A2_out[1]), .ZN(
        M2_mult_28_n315) );
  OAI22_X1 M2_mult_28_U348 ( .A1(coeff_b[1]), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n315), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n362) );
  XNOR2_X1 M2_mult_28_U347 ( .A(M2_mult_28_n286), .B(A2_out[2]), .ZN(
        M2_mult_28_n361) );
  NAND2_X1 M2_mult_28_U346 ( .A1(M2_mult_28_n287), .A2(M2_mult_28_n361), .ZN(
        M2_mult_28_n309) );
  NAND3_X1 M2_mult_28_U345 ( .A1(M2_mult_28_n360), .A2(M2_mult_28_n291), .A3(
        A2_out[3]), .ZN(M2_mult_28_n359) );
  OAI21_X1 M2_mult_28_U344 ( .B1(M2_mult_28_n286), .B2(M2_mult_28_n309), .A(
        M2_mult_28_n359), .ZN(M2_mult_28_n358) );
  AOI222_X1 M2_mult_28_U343 ( .A1(M2_mult_28_n268), .A2(M2_mult_28_n79), .B1(
        M2_mult_28_n358), .B2(M2_mult_28_n268), .C1(M2_mult_28_n358), .C2(
        M2_mult_28_n79), .ZN(M2_mult_28_n357) );
  AOI222_X1 M2_mult_28_U342 ( .A1(M2_mult_28_n283), .A2(M2_mult_28_n77), .B1(
        M2_mult_28_n283), .B2(M2_mult_28_n78), .C1(M2_mult_28_n78), .C2(
        M2_mult_28_n77), .ZN(M2_mult_28_n356) );
  AOI222_X1 M2_mult_28_U341 ( .A1(M2_mult_28_n282), .A2(M2_mult_28_n73), .B1(
        M2_mult_28_n282), .B2(M2_mult_28_n76), .C1(M2_mult_28_n76), .C2(
        M2_mult_28_n73), .ZN(M2_mult_28_n355) );
  AOI222_X1 M2_mult_28_U340 ( .A1(M2_mult_28_n278), .A2(M2_mult_28_n69), .B1(
        M2_mult_28_n278), .B2(M2_mult_28_n72), .C1(M2_mult_28_n72), .C2(
        M2_mult_28_n69), .ZN(M2_mult_28_n354) );
  AOI222_X1 M2_mult_28_U339 ( .A1(M2_mult_28_n277), .A2(M2_mult_28_n63), .B1(
        M2_mult_28_n277), .B2(M2_mult_28_n68), .C1(M2_mult_28_n68), .C2(
        M2_mult_28_n63), .ZN(M2_mult_28_n353) );
  XOR2_X1 M2_mult_28_U338 ( .A(A2_out[8]), .B(M2_mult_28_n276), .Z(
        M2_mult_28_n295) );
  XNOR2_X1 M2_mult_28_U337 ( .A(coeff_b[6]), .B(A2_out[8]), .ZN(
        M2_mult_28_n352) );
  NOR2_X1 M2_mult_28_U336 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n352), .ZN(
        M2_mult_28_n100) );
  XNOR2_X1 M2_mult_28_U335 ( .A(coeff_b[5]), .B(A2_out[8]), .ZN(
        M2_mult_28_n351) );
  NOR2_X1 M2_mult_28_U334 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n351), .ZN(
        M2_mult_28_n101) );
  XNOR2_X1 M2_mult_28_U333 ( .A(coeff_b[4]), .B(A2_out[8]), .ZN(
        M2_mult_28_n350) );
  NOR2_X1 M2_mult_28_U332 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n350), .ZN(
        M2_mult_28_n102) );
  XNOR2_X1 M2_mult_28_U331 ( .A(coeff_b[3]), .B(A2_out[8]), .ZN(
        M2_mult_28_n349) );
  NOR2_X1 M2_mult_28_U330 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n349), .ZN(
        M2_mult_28_n103) );
  XNOR2_X1 M2_mult_28_U329 ( .A(coeff_b[2]), .B(A2_out[8]), .ZN(
        M2_mult_28_n348) );
  NOR2_X1 M2_mult_28_U328 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n348), .ZN(
        M2_mult_28_n104) );
  NOR2_X1 M2_mult_28_U327 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n291), .ZN(
        M2_mult_28_n106) );
  XNOR2_X1 M2_mult_28_U326 ( .A(coeff_b[8]), .B(A2_out[7]), .ZN(
        M2_mult_28_n314) );
  XNOR2_X1 M2_mult_28_U325 ( .A(M2_mult_28_n276), .B(A2_out[6]), .ZN(
        M2_mult_28_n347) );
  NAND2_X1 M2_mult_28_U324 ( .A1(M2_mult_28_n302), .A2(M2_mult_28_n347), .ZN(
        M2_mult_28_n300) );
  OAI22_X1 M2_mult_28_U323 ( .A1(M2_mult_28_n314), .A2(M2_mult_28_n302), .B1(
        M2_mult_28_n300), .B2(M2_mult_28_n314), .ZN(M2_mult_28_n346) );
  XNOR2_X1 M2_mult_28_U322 ( .A(coeff_b[6]), .B(A2_out[7]), .ZN(
        M2_mult_28_n345) );
  XNOR2_X1 M2_mult_28_U321 ( .A(coeff_b[7]), .B(A2_out[7]), .ZN(
        M2_mult_28_n313) );
  OAI22_X1 M2_mult_28_U320 ( .A1(M2_mult_28_n345), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n313), .ZN(M2_mult_28_n108) );
  XNOR2_X1 M2_mult_28_U319 ( .A(coeff_b[5]), .B(A2_out[7]), .ZN(
        M2_mult_28_n344) );
  OAI22_X1 M2_mult_28_U318 ( .A1(M2_mult_28_n344), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n345), .ZN(M2_mult_28_n109) );
  XNOR2_X1 M2_mult_28_U317 ( .A(coeff_b[4]), .B(A2_out[7]), .ZN(
        M2_mult_28_n343) );
  OAI22_X1 M2_mult_28_U316 ( .A1(M2_mult_28_n343), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n344), .ZN(M2_mult_28_n110) );
  XNOR2_X1 M2_mult_28_U315 ( .A(coeff_b[3]), .B(A2_out[7]), .ZN(
        M2_mult_28_n307) );
  OAI22_X1 M2_mult_28_U314 ( .A1(M2_mult_28_n307), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n343), .ZN(M2_mult_28_n111) );
  XNOR2_X1 M2_mult_28_U313 ( .A(coeff_b[1]), .B(A2_out[7]), .ZN(
        M2_mult_28_n342) );
  XNOR2_X1 M2_mult_28_U312 ( .A(coeff_b[2]), .B(A2_out[7]), .ZN(
        M2_mult_28_n306) );
  OAI22_X1 M2_mult_28_U311 ( .A1(M2_mult_28_n342), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n306), .ZN(M2_mult_28_n113) );
  XNOR2_X1 M2_mult_28_U310 ( .A(coeff_b[0]), .B(A2_out[7]), .ZN(
        M2_mult_28_n341) );
  OAI22_X1 M2_mult_28_U309 ( .A1(M2_mult_28_n341), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n342), .ZN(M2_mult_28_n114) );
  NOR2_X1 M2_mult_28_U308 ( .A1(M2_mult_28_n302), .A2(M2_mult_28_n291), .ZN(
        M2_mult_28_n115) );
  XNOR2_X1 M2_mult_28_U307 ( .A(coeff_b[8]), .B(A2_out[5]), .ZN(
        M2_mult_28_n312) );
  XNOR2_X1 M2_mult_28_U306 ( .A(M2_mult_28_n281), .B(A2_out[4]), .ZN(
        M2_mult_28_n340) );
  NAND2_X1 M2_mult_28_U305 ( .A1(M2_mult_28_n299), .A2(M2_mult_28_n340), .ZN(
        M2_mult_28_n297) );
  OAI22_X1 M2_mult_28_U304 ( .A1(M2_mult_28_n312), .A2(M2_mult_28_n299), .B1(
        M2_mult_28_n297), .B2(M2_mult_28_n312), .ZN(M2_mult_28_n339) );
  XNOR2_X1 M2_mult_28_U303 ( .A(coeff_b[6]), .B(A2_out[5]), .ZN(
        M2_mult_28_n338) );
  XNOR2_X1 M2_mult_28_U302 ( .A(coeff_b[7]), .B(A2_out[5]), .ZN(
        M2_mult_28_n311) );
  OAI22_X1 M2_mult_28_U301 ( .A1(M2_mult_28_n338), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n311), .ZN(M2_mult_28_n117) );
  XNOR2_X1 M2_mult_28_U300 ( .A(coeff_b[5]), .B(A2_out[5]), .ZN(
        M2_mult_28_n337) );
  OAI22_X1 M2_mult_28_U299 ( .A1(M2_mult_28_n337), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n338), .ZN(M2_mult_28_n118) );
  XNOR2_X1 M2_mult_28_U298 ( .A(coeff_b[4]), .B(A2_out[5]), .ZN(
        M2_mult_28_n336) );
  OAI22_X1 M2_mult_28_U297 ( .A1(M2_mult_28_n336), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n337), .ZN(M2_mult_28_n119) );
  XNOR2_X1 M2_mult_28_U296 ( .A(coeff_b[3]), .B(A2_out[5]), .ZN(
        M2_mult_28_n335) );
  OAI22_X1 M2_mult_28_U295 ( .A1(M2_mult_28_n335), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n336), .ZN(M2_mult_28_n120) );
  XNOR2_X1 M2_mult_28_U294 ( .A(coeff_b[2]), .B(A2_out[5]), .ZN(
        M2_mult_28_n334) );
  OAI22_X1 M2_mult_28_U293 ( .A1(M2_mult_28_n334), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n335), .ZN(M2_mult_28_n121) );
  XNOR2_X1 M2_mult_28_U292 ( .A(coeff_b[1]), .B(A2_out[5]), .ZN(
        M2_mult_28_n333) );
  OAI22_X1 M2_mult_28_U291 ( .A1(M2_mult_28_n333), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n334), .ZN(M2_mult_28_n122) );
  XNOR2_X1 M2_mult_28_U290 ( .A(coeff_b[0]), .B(A2_out[5]), .ZN(
        M2_mult_28_n332) );
  OAI22_X1 M2_mult_28_U289 ( .A1(M2_mult_28_n332), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n333), .ZN(M2_mult_28_n123) );
  NOR2_X1 M2_mult_28_U288 ( .A1(M2_mult_28_n299), .A2(M2_mult_28_n291), .ZN(
        M2_mult_28_n124) );
  XOR2_X1 M2_mult_28_U287 ( .A(coeff_b[8]), .B(M2_mult_28_n286), .Z(
        M2_mult_28_n310) );
  OAI22_X1 M2_mult_28_U286 ( .A1(M2_mult_28_n310), .A2(M2_mult_28_n287), .B1(
        M2_mult_28_n309), .B2(M2_mult_28_n310), .ZN(M2_mult_28_n331) );
  XNOR2_X1 M2_mult_28_U285 ( .A(coeff_b[6]), .B(A2_out[3]), .ZN(
        M2_mult_28_n330) );
  XNOR2_X1 M2_mult_28_U284 ( .A(coeff_b[7]), .B(A2_out[3]), .ZN(
        M2_mult_28_n308) );
  OAI22_X1 M2_mult_28_U283 ( .A1(M2_mult_28_n330), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n308), .ZN(M2_mult_28_n126) );
  XNOR2_X1 M2_mult_28_U282 ( .A(coeff_b[5]), .B(A2_out[3]), .ZN(
        M2_mult_28_n329) );
  OAI22_X1 M2_mult_28_U281 ( .A1(M2_mult_28_n329), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n330), .ZN(M2_mult_28_n127) );
  XNOR2_X1 M2_mult_28_U280 ( .A(coeff_b[4]), .B(A2_out[3]), .ZN(
        M2_mult_28_n328) );
  OAI22_X1 M2_mult_28_U279 ( .A1(M2_mult_28_n328), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n329), .ZN(M2_mult_28_n128) );
  XNOR2_X1 M2_mult_28_U278 ( .A(coeff_b[3]), .B(A2_out[3]), .ZN(
        M2_mult_28_n327) );
  OAI22_X1 M2_mult_28_U277 ( .A1(M2_mult_28_n327), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n328), .ZN(M2_mult_28_n129) );
  XNOR2_X1 M2_mult_28_U276 ( .A(coeff_b[2]), .B(A2_out[3]), .ZN(
        M2_mult_28_n326) );
  OAI22_X1 M2_mult_28_U275 ( .A1(M2_mult_28_n326), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n327), .ZN(M2_mult_28_n130) );
  XNOR2_X1 M2_mult_28_U274 ( .A(coeff_b[1]), .B(A2_out[3]), .ZN(
        M2_mult_28_n325) );
  OAI22_X1 M2_mult_28_U273 ( .A1(M2_mult_28_n325), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n326), .ZN(M2_mult_28_n131) );
  XNOR2_X1 M2_mult_28_U272 ( .A(coeff_b[0]), .B(A2_out[3]), .ZN(
        M2_mult_28_n324) );
  OAI22_X1 M2_mult_28_U271 ( .A1(M2_mult_28_n324), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n325), .ZN(M2_mult_28_n132) );
  XNOR2_X1 M2_mult_28_U270 ( .A(coeff_b[8]), .B(A2_out[1]), .ZN(
        M2_mult_28_n322) );
  OAI22_X1 M2_mult_28_U269 ( .A1(M2_mult_28_n289), .A2(M2_mult_28_n322), .B1(
        M2_mult_28_n316), .B2(M2_mult_28_n322), .ZN(M2_mult_28_n323) );
  XNOR2_X1 M2_mult_28_U268 ( .A(coeff_b[7]), .B(A2_out[1]), .ZN(
        M2_mult_28_n321) );
  OAI22_X1 M2_mult_28_U267 ( .A1(M2_mult_28_n321), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n322), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n135) );
  XNOR2_X1 M2_mult_28_U266 ( .A(coeff_b[6]), .B(A2_out[1]), .ZN(
        M2_mult_28_n320) );
  OAI22_X1 M2_mult_28_U265 ( .A1(M2_mult_28_n320), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n321), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n136) );
  XNOR2_X1 M2_mult_28_U264 ( .A(coeff_b[5]), .B(A2_out[1]), .ZN(
        M2_mult_28_n319) );
  OAI22_X1 M2_mult_28_U263 ( .A1(M2_mult_28_n319), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n320), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n137) );
  XNOR2_X1 M2_mult_28_U262 ( .A(coeff_b[4]), .B(A2_out[1]), .ZN(
        M2_mult_28_n318) );
  OAI22_X1 M2_mult_28_U261 ( .A1(M2_mult_28_n318), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n319), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n138) );
  XNOR2_X1 M2_mult_28_U260 ( .A(coeff_b[3]), .B(A2_out[1]), .ZN(
        M2_mult_28_n317) );
  OAI22_X1 M2_mult_28_U259 ( .A1(M2_mult_28_n317), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n318), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n139) );
  OAI22_X1 M2_mult_28_U258 ( .A1(M2_mult_28_n315), .A2(M2_mult_28_n316), .B1(
        M2_mult_28_n317), .B2(M2_mult_28_n289), .ZN(M2_mult_28_n140) );
  OAI22_X1 M2_mult_28_U257 ( .A1(M2_mult_28_n313), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n314), .ZN(M2_mult_28_n22) );
  OAI22_X1 M2_mult_28_U256 ( .A1(M2_mult_28_n311), .A2(M2_mult_28_n297), .B1(
        M2_mult_28_n299), .B2(M2_mult_28_n312), .ZN(M2_mult_28_n32) );
  OAI22_X1 M2_mult_28_U255 ( .A1(M2_mult_28_n308), .A2(M2_mult_28_n309), .B1(
        M2_mult_28_n287), .B2(M2_mult_28_n310), .ZN(M2_mult_28_n46) );
  OAI22_X1 M2_mult_28_U254 ( .A1(M2_mult_28_n306), .A2(M2_mult_28_n300), .B1(
        M2_mult_28_n302), .B2(M2_mult_28_n307), .ZN(M2_mult_28_n305) );
  XNOR2_X1 M2_mult_28_U253 ( .A(M2_mult_28_n290), .B(A2_out[8]), .ZN(
        M2_mult_28_n304) );
  NAND2_X1 M2_mult_28_U252 ( .A1(M2_mult_28_n304), .A2(M2_mult_28_n271), .ZN(
        M2_mult_28_n303) );
  NAND2_X1 M2_mult_28_U251 ( .A1(M2_mult_28_n273), .A2(M2_mult_28_n303), .ZN(
        M2_mult_28_n54) );
  XNOR2_X1 M2_mult_28_U250 ( .A(M2_mult_28_n303), .B(M2_mult_28_n273), .ZN(
        M2_mult_28_n55) );
  AND3_X1 M2_mult_28_U249 ( .A1(A2_out[8]), .A2(M2_mult_28_n291), .A3(
        M2_mult_28_n271), .ZN(M2_mult_28_n93) );
  OR3_X1 M2_mult_28_U248 ( .A1(M2_mult_28_n302), .A2(coeff_b[0]), .A3(
        M2_mult_28_n276), .ZN(M2_mult_28_n301) );
  OAI21_X1 M2_mult_28_U247 ( .B1(M2_mult_28_n276), .B2(M2_mult_28_n300), .A(
        M2_mult_28_n301), .ZN(M2_mult_28_n94) );
  OR3_X1 M2_mult_28_U246 ( .A1(M2_mult_28_n299), .A2(coeff_b[0]), .A3(
        M2_mult_28_n281), .ZN(M2_mult_28_n298) );
  OAI21_X1 M2_mult_28_U245 ( .B1(M2_mult_28_n281), .B2(M2_mult_28_n297), .A(
        M2_mult_28_n298), .ZN(M2_mult_28_n95) );
  XNOR2_X1 M2_mult_28_U244 ( .A(coeff_b[7]), .B(A2_out[8]), .ZN(
        M2_mult_28_n296) );
  NOR2_X1 M2_mult_28_U243 ( .A1(M2_mult_28_n295), .A2(M2_mult_28_n296), .ZN(
        M2_mult_28_n99) );
  XOR2_X1 M2_mult_28_U242 ( .A(coeff_b[8]), .B(A2_out[8]), .Z(M2_mult_28_n294)
         );
  NAND2_X1 M2_mult_28_U241 ( .A1(M2_mult_28_n294), .A2(M2_mult_28_n271), .ZN(
        M2_mult_28_n292) );
  XOR2_X1 M2_mult_28_U240 ( .A(M2_mult_28_n2), .B(M2_mult_28_n18), .Z(
        M2_mult_28_n293) );
  XOR2_X1 M2_mult_28_U239 ( .A(M2_mult_28_n292), .B(M2_mult_28_n293), .Z(
        M2_out[8]) );
  INV_X1 M2_mult_28_U238 ( .A(coeff_b[1]), .ZN(M2_mult_28_n290) );
  INV_X1 M2_mult_28_U237 ( .A(coeff_b[0]), .ZN(M2_mult_28_n291) );
  INV_X1 M2_mult_28_U236 ( .A(M2_mult_28_n22), .ZN(M2_mult_28_n274) );
  INV_X1 M2_mult_28_U235 ( .A(M2_mult_28_n346), .ZN(M2_mult_28_n275) );
  INV_X1 M2_mult_28_U234 ( .A(M2_mult_28_n305), .ZN(M2_mult_28_n273) );
  AND3_X1 M2_mult_28_U233 ( .A1(M2_mult_28_n362), .A2(M2_mult_28_n290), .A3(
        A2_out[1]), .ZN(M2_mult_28_n270) );
  AND2_X1 M2_mult_28_U232 ( .A1(M2_mult_28_n360), .A2(M2_mult_28_n362), .ZN(
        M2_mult_28_n269) );
  MUX2_X1 M2_mult_28_U231 ( .A(M2_mult_28_n269), .B(M2_mult_28_n270), .S(
        M2_mult_28_n291), .Z(M2_mult_28_n268) );
  INV_X1 M2_mult_28_U230 ( .A(M2_mult_28_n353), .ZN(M2_mult_28_n272) );
  INV_X1 M2_mult_28_U229 ( .A(M2_mult_28_n323), .ZN(M2_mult_28_n288) );
  INV_X1 M2_mult_28_U228 ( .A(M2_mult_28_n339), .ZN(M2_mult_28_n280) );
  INV_X1 M2_mult_28_U227 ( .A(M2_mult_28_n331), .ZN(M2_mult_28_n285) );
  INV_X1 M2_mult_28_U226 ( .A(M2_mult_28_n32), .ZN(M2_mult_28_n279) );
  INV_X1 M2_mult_28_U225 ( .A(A2_out[7]), .ZN(M2_mult_28_n276) );
  INV_X1 M2_mult_28_U224 ( .A(A2_out[5]), .ZN(M2_mult_28_n281) );
  INV_X1 M2_mult_28_U223 ( .A(A2_out[3]), .ZN(M2_mult_28_n286) );
  INV_X1 M2_mult_28_U222 ( .A(A2_out[0]), .ZN(M2_mult_28_n289) );
  INV_X1 M2_mult_28_U221 ( .A(M2_mult_28_n357), .ZN(M2_mult_28_n283) );
  INV_X1 M2_mult_28_U220 ( .A(M2_mult_28_n356), .ZN(M2_mult_28_n282) );
  XOR2_X1 M2_mult_28_U219 ( .A(A2_out[6]), .B(M2_mult_28_n281), .Z(
        M2_mult_28_n302) );
  XOR2_X1 M2_mult_28_U218 ( .A(A2_out[4]), .B(M2_mult_28_n286), .Z(
        M2_mult_28_n299) );
  INV_X1 M2_mult_28_U217 ( .A(M2_mult_28_n46), .ZN(M2_mult_28_n284) );
  INV_X1 M2_mult_28_U216 ( .A(M2_mult_28_n295), .ZN(M2_mult_28_n271) );
  INV_X1 M2_mult_28_U215 ( .A(M2_mult_28_n360), .ZN(M2_mult_28_n287) );
  INV_X1 M2_mult_28_U214 ( .A(M2_mult_28_n355), .ZN(M2_mult_28_n278) );
  INV_X1 M2_mult_28_U213 ( .A(M2_mult_28_n354), .ZN(M2_mult_28_n277) );
  HA_X1 M2_mult_28_U50 ( .A(M2_mult_28_n132), .B(M2_mult_28_n140), .CO(
        M2_mult_28_n78), .S(M2_mult_28_n79) );
  FA_X1 M2_mult_28_U49 ( .A(M2_mult_28_n139), .B(M2_mult_28_n124), .CI(
        M2_mult_28_n131), .CO(M2_mult_28_n76), .S(M2_mult_28_n77) );
  HA_X1 M2_mult_28_U48 ( .A(M2_mult_28_n95), .B(M2_mult_28_n123), .CO(
        M2_mult_28_n74), .S(M2_mult_28_n75) );
  FA_X1 M2_mult_28_U47 ( .A(M2_mult_28_n130), .B(M2_mult_28_n138), .CI(
        M2_mult_28_n75), .CO(M2_mult_28_n72), .S(M2_mult_28_n73) );
  FA_X1 M2_mult_28_U46 ( .A(M2_mult_28_n137), .B(M2_mult_28_n115), .CI(
        M2_mult_28_n129), .CO(M2_mult_28_n70), .S(M2_mult_28_n71) );
  FA_X1 M2_mult_28_U45 ( .A(M2_mult_28_n74), .B(M2_mult_28_n122), .CI(
        M2_mult_28_n71), .CO(M2_mult_28_n68), .S(M2_mult_28_n69) );
  HA_X1 M2_mult_28_U44 ( .A(M2_mult_28_n94), .B(M2_mult_28_n114), .CO(
        M2_mult_28_n66), .S(M2_mult_28_n67) );
  FA_X1 M2_mult_28_U43 ( .A(M2_mult_28_n121), .B(M2_mult_28_n136), .CI(
        M2_mult_28_n128), .CO(M2_mult_28_n64), .S(M2_mult_28_n65) );
  FA_X1 M2_mult_28_U42 ( .A(M2_mult_28_n70), .B(M2_mult_28_n67), .CI(
        M2_mult_28_n65), .CO(M2_mult_28_n62), .S(M2_mult_28_n63) );
  FA_X1 M2_mult_28_U41 ( .A(M2_mult_28_n120), .B(M2_mult_28_n106), .CI(
        M2_mult_28_n135), .CO(M2_mult_28_n60), .S(M2_mult_28_n61) );
  FA_X1 M2_mult_28_U40 ( .A(M2_mult_28_n113), .B(M2_mult_28_n127), .CI(
        M2_mult_28_n66), .CO(M2_mult_28_n58), .S(M2_mult_28_n59) );
  FA_X1 M2_mult_28_U39 ( .A(M2_mult_28_n61), .B(M2_mult_28_n64), .CI(
        M2_mult_28_n59), .CO(M2_mult_28_n56), .S(M2_mult_28_n57) );
  FA_X1 M2_mult_28_U36 ( .A(M2_mult_28_n93), .B(M2_mult_28_n119), .CI(
        M2_mult_28_n288), .CO(M2_mult_28_n52), .S(M2_mult_28_n53) );
  FA_X1 M2_mult_28_U35 ( .A(M2_mult_28_n55), .B(M2_mult_28_n126), .CI(
        M2_mult_28_n60), .CO(M2_mult_28_n50), .S(M2_mult_28_n51) );
  FA_X1 M2_mult_28_U34 ( .A(M2_mult_28_n53), .B(M2_mult_28_n58), .CI(
        M2_mult_28_n51), .CO(M2_mult_28_n48), .S(M2_mult_28_n49) );
  FA_X1 M2_mult_28_U32 ( .A(M2_mult_28_n111), .B(M2_mult_28_n104), .CI(
        M2_mult_28_n118), .CO(M2_mult_28_n44), .S(M2_mult_28_n45) );
  FA_X1 M2_mult_28_U31 ( .A(M2_mult_28_n54), .B(M2_mult_28_n284), .CI(
        M2_mult_28_n52), .CO(M2_mult_28_n42), .S(M2_mult_28_n43) );
  FA_X1 M2_mult_28_U30 ( .A(M2_mult_28_n50), .B(M2_mult_28_n45), .CI(
        M2_mult_28_n43), .CO(M2_mult_28_n40), .S(M2_mult_28_n41) );
  FA_X1 M2_mult_28_U29 ( .A(M2_mult_28_n110), .B(M2_mult_28_n103), .CI(
        M2_mult_28_n285), .CO(M2_mult_28_n38), .S(M2_mult_28_n39) );
  FA_X1 M2_mult_28_U28 ( .A(M2_mult_28_n46), .B(M2_mult_28_n117), .CI(
        M2_mult_28_n44), .CO(M2_mult_28_n36), .S(M2_mult_28_n37) );
  FA_X1 M2_mult_28_U27 ( .A(M2_mult_28_n42), .B(M2_mult_28_n39), .CI(
        M2_mult_28_n37), .CO(M2_mult_28_n34), .S(M2_mult_28_n35) );
  FA_X1 M2_mult_28_U25 ( .A(M2_mult_28_n102), .B(M2_mult_28_n109), .CI(
        M2_mult_28_n279), .CO(M2_mult_28_n30), .S(M2_mult_28_n31) );
  FA_X1 M2_mult_28_U24 ( .A(M2_mult_28_n31), .B(M2_mult_28_n38), .CI(
        M2_mult_28_n36), .CO(M2_mult_28_n28), .S(M2_mult_28_n29) );
  FA_X1 M2_mult_28_U23 ( .A(M2_mult_28_n108), .B(M2_mult_28_n32), .CI(
        M2_mult_28_n280), .CO(M2_mult_28_n26), .S(M2_mult_28_n27) );
  FA_X1 M2_mult_28_U22 ( .A(M2_mult_28_n30), .B(M2_mult_28_n101), .CI(
        M2_mult_28_n27), .CO(M2_mult_28_n24), .S(M2_mult_28_n25) );
  FA_X1 M2_mult_28_U20 ( .A(M2_mult_28_n274), .B(M2_mult_28_n100), .CI(
        M2_mult_28_n26), .CO(M2_mult_28_n20), .S(M2_mult_28_n21) );
  FA_X1 M2_mult_28_U19 ( .A(M2_mult_28_n99), .B(M2_mult_28_n22), .CI(
        M2_mult_28_n275), .CO(M2_mult_28_n18), .S(M2_mult_28_n19) );
  FA_X1 M2_mult_28_U10 ( .A(M2_mult_28_n57), .B(M2_mult_28_n62), .CI(
        M2_mult_28_n272), .CO(M2_mult_28_n9), .S(M2_out[0]) );
  FA_X1 M2_mult_28_U9 ( .A(M2_mult_28_n49), .B(M2_mult_28_n56), .CI(
        M2_mult_28_n9), .CO(M2_mult_28_n8), .S(M2_out[1]) );
  FA_X1 M2_mult_28_U8 ( .A(M2_mult_28_n41), .B(M2_mult_28_n48), .CI(
        M2_mult_28_n8), .CO(M2_mult_28_n7), .S(M2_out[2]) );
  FA_X1 M2_mult_28_U7 ( .A(M2_mult_28_n35), .B(M2_mult_28_n40), .CI(
        M2_mult_28_n7), .CO(M2_mult_28_n6), .S(M2_out[3]) );
  FA_X1 M2_mult_28_U6 ( .A(M2_mult_28_n29), .B(M2_mult_28_n34), .CI(
        M2_mult_28_n6), .CO(M2_mult_28_n5), .S(M2_out[4]) );
  FA_X1 M2_mult_28_U5 ( .A(M2_mult_28_n25), .B(M2_mult_28_n28), .CI(
        M2_mult_28_n5), .CO(M2_mult_28_n4), .S(M2_out[5]) );
  FA_X1 M2_mult_28_U4 ( .A(M2_mult_28_n21), .B(M2_mult_28_n24), .CI(
        M2_mult_28_n4), .CO(M2_mult_28_n3), .S(M2_out[6]) );
  FA_X1 M2_mult_28_U3 ( .A(M2_mult_28_n20), .B(M2_mult_28_n19), .CI(
        M2_mult_28_n3), .CO(M2_mult_28_n2), .S(M2_out[7]) );
endmodule

