#ifndef __FINITE_PRECISION_OPERATIONS__
#define __FINITE_PRECISION_OPERATIONS__


// Addition between two integer numbers on N bits.
// Returns the result on N bits
int finiteSum(int x, int y);

// Multiplication between two integer numbers on Nbits.
// Returns the result on N bits.
int finiteMult(int x, int y, int N);

#endif
