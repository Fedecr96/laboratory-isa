#ifndef __FILE_IO__
#define __FILE_IO__

#include <stdio.h>
#include <stdlib.h>

// Function that opens the file in the desired mode
// and check for any error during the process.
FILE *fileOpen(char *filename, char *mode);

#endif
