#include "my_IIR_filter.h"
#include "fileIO.h"

#define INPUT "../scripts/samples.txt"
#define OUTPUT "C_results.txt"

const int a1 = -41;
const int b0 = 107;

int main(int argc, char **argv){

	FILE *fp_in, *fp_out;
	int inSample, outSample;


	// Input and output files
	fp_in = fileOpen(INPUT, "r");
	fp_out = fileOpen(OUTPUT, "w");


	while(fscanf(fp_in, "%d", &inSample)!=EOF){
			
		// Filter the sample
		outSample = myIIRfilter(inSample);	

		// Write the result on the output file
		fprintf(fp_out, "%d\n", outSample);		
	}

	fclose(fp_in);
	fclose(fp_out);

	return 0;
}
