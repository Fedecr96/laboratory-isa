#include "my_IIR_filter.h"

// IIR filter on 9 bits. It works on a single sample
// and returns the value of a single output sample.
int myIIRfilter(int inSample){

	static int s;
	static int firstExe = 0;

	int fb;
	int w;
	int outSample;

	// If the filter is in its first execution
	// the internal register must be initialized
	if(firstExe == 0){
		firstExe = 1;
		s = 0;
	}

	// Feedback signal
	fb = finiteMult(a1, s, N_BITS);

	// Intermediate signal
	w = finiteSum(inSample, fb);

	// Output before the final multiplication
	outSample = finiteSum(s, w);

	// Final result
	outSample = finiteMult(outSample, b0, N_BITS);

	// Update the feedback register
	s = w;

	return outSample;
}


