#include "fileIO.h"

// Function that opens the file in the desired mode
// and check for any error during the process.
FILE *fileOpen(char *filename, char *mode){

	FILE *fp;

	fp = fopen(filename, mode);

	if(fp == NULL){
		printf("Error opening the file %s\n", filename);
		exit(-1);
	}

	return fp;
}
