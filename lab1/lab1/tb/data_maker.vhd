library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

entity data_maker is  
	port (
		-- input signals
		CLK     : in  std_logic;
		RST_n   : in  std_logic;

		-- output signals
		VOUT    : out std_logic;
		DOUT    : out signed(8 downto 0);
		a1	: out signed(8 downto 0);
		b0	: out signed(8 downto 0);
		END_SIM : out std_logic
	);
end data_maker;


architecture beh of data_maker is

	constant tco		: time := 1 ns;

	signal sEndSim		: std_logic;
	signal END_SIM_i	: std_logic_vector(0 to 2);  

begin  -- beh


   a1 <= to_signed(-41, a1'length);
   b0 <= to_signed(107, b0'length);

	-- process that generates the data to provide to the filter, together with the
	-- start signal Vin
	data_generation : process (CLK, RST_n)

		file fp_in 		: text open READ_MODE is "../scripts/samples.txt";
		variable line_in	: line;
		variable Dout_var	: integer;
		
	begin  -- process
		if RST_n = '0' -- asynchronous reset active low
		then
			DOUT <= (others => '0') after tco;      
			VOUT <= '0' after tco;
			sEndSim <= '0' after tco;

		elsif CLK'event and CLK = '1' -- rising clock edge
		then
			if not endfile(fp_in)
			then
				readline(fp_in, line_in);
				read(line_in, Dout_var);
				DOUT <= to_signed(Dout_var,DOUT'length) after tco;
				VOUT <= '1' after tco;
				sEndSim <= '0' after tco;
			else
				VOUT <= '0' after tco;        
				sEndSim <= '1' after tco;
			end if;
		end if;

	end process data_generation;



	-- process to delay the end_sim signal of a quantity sufficient to let
	-- the filter end the final computations
	end_sim_delay : process (CLK, RST_n)
	begin  -- process
		if RST_n = '0' -- asynchronous reset (active low)
		then
			END_SIM_i <= (others => '0') after tco;
		elsif CLK'event and CLK = '1' -- rising clock edge
		then
			END_SIM_i(0) <= sEndSim after tco;
			END_SIM_i(1 to 2) <= END_SIM_i(0 to 1) after tco;
		end if;
	end process end_sim_delay;

	END_SIM <= END_SIM_i(2);  

end beh;
