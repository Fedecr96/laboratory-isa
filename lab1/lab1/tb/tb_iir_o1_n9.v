//`timescale 1ns

module tb_iir_o1_n9 ();

   wire CLK_i;
   wire RST_n_i;
   wire [8:0] DIN_i;
   wire VIN_i;
   wire [8:0] b0_i;
   wire [8:0] a1_i;
   wire [8:0] DOUT_i;
   wire VOUT_i;
   wire END_SIM_i;

   clk_gen CG(.END_SIM(END_SIM_i),
  	      .CLK(CLK_i),
	      .RST_n(RST_n_i));

   data_maker SM(.CLK(CLK_i),
	         .RST_n(RST_n_i),
		 .VOUT(VIN_i),
		 .DOUT(DIN_i),
		 .b0(b0_i),
		 .a1(a1_i),
		 .END_SIM(END_SIM_i));

   iir_o1_n9 UUT(.clk(CLK_i),
	     	.rst_n(RST_n_i),
	     	.x_n(DIN_i),
        	.v_in(VIN_i),
		.coeff_b(b0_i),
		.coeff_a(a1_i),
        	.y_n(DOUT_i),
        	.v_out(VOUT_i));

   data_sink DS(.CLK(CLK_i),
		.RST_n(RST_n_i),
		.VIN(VOUT_i),
		.DIN(DOUT_i));   

endmodule

		   
