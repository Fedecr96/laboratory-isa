set SRC_DIR ../innovus
set NETLIST_DIR ../netlist
set TB_DIR ../tb

set DUT iir_o1_n9
set TB tb_iir_o1_n9

set CELL_LIB NangateOpenCellLibrary.v

# Create work library
vlib work

# Compile the verilog innovus' file
vlog $SRC_DIR/$DUT.v
vlog $NETLIST_DIR/$CELL_LIB

# Compile the testbench files
vcom -93 -work ./work $TB_DIR/data_maker.vhd
vcom -93 -work ./work $TB_DIR/data_sink.vhd
vcom -93 -work ./work $TB_DIR/clk_gen.vhd
vlog $TB_DIR/$TB.v


# Simulate in command line mode (-c) without otimizations
# (-novopt) with time resolution ns
vsim -t ns -c -novopt work.$TB

# Run the simulation
run 10 us

# Quit Modelsim
quit -f
