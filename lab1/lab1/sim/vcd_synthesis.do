set TB_DIR ../tb
set NETLIST_DIR ../netlist
set VCD_DIR ../vcd

set DUT iir_o1_n9
set TB tb_iir_o1_n9

set VCD_FILENAME synthesis.vcd

set CELL_LIB /software/dk/nangate45/verilog/msim6.2g  

# Create work library
vlib work

# Compile all the testbench files
vcom -93 -work ./work $TB_DIR/clk_gen.vhd
vcom -93 -work ./work $TB_DIR/data_maker.vhd
vcom -93 -work ./work $TB_DIR/data_sink.vhd
vlog -work ./work $TB_DIR/$TB.v

# Compile all the UUT files
vlog -work ./work $NETLIST_DIR/$DUT.v

vsim -L $CELL_LIB work.$TB


#vlog -work ./work $NETLIST_DIR/NangateOpenCellLibrary.v

# Link delay file
vsim -L $CELL_LIB -sdftyp /$TB/UUT=$NETLIST_DIR/$DUT.sdf work.$TB

# Open file to save switching info --> Look at design.vcd
vcd file $VCD_DIR/$VCD_FILENAME

# Force monitoring of all the signals
vcd add /$TB/UUT/*

# Run the simulation
run 20 us

# Quit Modelsim
quit -f
