#!/usr/bin/bash

STARTING_DIR="$1"
TARGET_DIR="$2"
EXECUTABLE_FILE="$3"
C_RESULTS="$4"


# Move to the folder in which the makefile is contained
cd $TARGET_DIR


# Remove the result from previous simulations
if [[ -f $C_RESULTS ]]
then
	rm $C_RESULTS
fi

# Create the executable starting from the various source files
make

# Add the execution permission to the generated executable only for the user
chmod 700 $EXECUTABLE_FILE

# Run the executable
./$EXECUTABLE_FILE

# Remove the object files
make clean

# Remove the executable
make cleanexe

# Return in the starting directory
cd $START_DIR
