#!/usr/bin/bash

STARTING_DIR="$1"
TARGET_DIR="$2"
TCL_SCRIPT="$3"

removeUselessFiles(){
	find . -not -name synth.tcl -not -name synthesis_power.tcl \
	-not -name report_timing.txt -not -name report_power.txt \
	-not -name report_area.txt -not -name .synopsys_dc.setup -delete
}

# Move to the directory containing the script
# to run with Modelsim
cd $TARGET_DIR

removeUselessFiles

# Set up the environment to properly run Modelsim
source /software/scripts/init_synopsys_64.18

# Execute the desired set of Modelsim commands
dc_shell-xg-t -f $TCL_SCRIPT

removeUselessFiles

# Return to the starting directory
cd $STARTING_DIR
